# Acknowledgement

This book would not have existed without the amazing support of early enthusiasts who offered to purchase the book even before it was finished! There are no words to express my gratitude for the motivation they have provided me during many long days and nights of writing. In no particular order, I'd like to thank them individually.

**Gold Supporters**

Mike Finlayson, Timothy Viola (Viola Engineering, PC), Edward Wyrwas (Prepared for Flight, LLC), Dr. Bertram Bilek (Instituto Bilek), Owen Torgerson, Matt Harding (Osprey Mapping Solutions, LLC), Issouf Ouattara (FasoDrone), Reality Scout, Nathan Ryan, Bill Fredricks (Penn’s Wall, LLC), Christoph Trockel (SMQ), Justin Cole, Tryhard (Tryhard Prospecting), Pascal Vincent (AOMS), Luca Delucchi, Matthew Cua (OneSquirrelMade), Keith Conley (Beyond Aerial, LLC), Greg Rouse (Ross County Ohio SWCD), Silva Diego Hemkemeier, Robert Nall (Bulldog Imagery, LLC), Maciej Cybulski (MC2Systems), Adam Steer (Spatialised), Pratyush Kumar Das (Asian Institute of Technology), Will Welker (Pi Farm), Shunichiro Nishino, Mapping Services Australia, Marcos Gomez-Redondo (Facultad de Ingeniería de la Universidad Nacional de Asunción), AeroSurvey New Zealand, Bryan Jackson (kaaspad), Robert Hall (Pro Aerial Digital), Jon Lee (Data Aero, LLC), Chris Mather (Bendigo Aerial Australia), Qopter360 Ltd, David Bradburn (DMB Consulting), Mauricio Gaviria (Tecnidrones S.A.S. - Colombia), Arne Wulff, Andy Lyons (University of California), Francisco Flores (FPFI Consulting Ltda., Chile), University of Copenhagen Forest and Landscape College, Masakazu Oshio (Land and House Investigator, Japan), NC State Center for Geospatial Analytics, Sasanai Chanate

**Silver Supporters**

David E. Gorla (CONICET, Argentina), Jarrett Totton (GoMapping), Japar Sidik Bujang (Universiti Putra Malaysia), Jeong Hyung-sik (Garam Forest Technology), Geco Enterprises Ltda Centro de I+D Chile, Neill Glover (Land IQ Insights), Luigi Pirelli (Freelance QGIS core developer), Jorge Lama, Michael Nielsen (Skyfair), Loren Abdulezer (Evolving Technologies Corporation), Jonathan Quiroz Valdivia, Mark W. Fink, Tomasz Nycz (GIS w Górach), Daniel Kendall (Daktech Pty Ltd), Joe Martin, Jerome Maruéjouls (Geoek), Álvaro Perdigão (TAW-S2i-Software), Kim Junseong (KMAP), Richard Marshall (Yacht Pogeyan), Randy Niedz, The New Zealand Institute for Plant & Food Research Limited, Gary Sieling (Element 84), Ryan Howell, Leon Schulpen (Kapla), Carlos Sousa, Marco Rizzetto, Sandy Thomson, Tero Keso (Häme University of Applied Sciences), Evan Watterson (Bluecoast Consulting Engineers), Alexander R. Groos (University of Bern)

\begin{center}
**Thank you!**

❤️
\end{center}
