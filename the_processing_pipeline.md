# 처리 파이프 라인
\label{the_processing_pipeline}

영상들로부터 3D 모델과 정사사진을 생성하는 과정은
일련의 세부 단계로 가장 잘 시각화되는 프로세스입니다.
각 단계는 이전 단계로부터 입력을 기반으로합니다.

![ODM’s processing pipeline\label{fig:odms_processing_pipeline}](images/figures/odms_processing_pipeline.png)

이 장에서는 파이프 라인의 각 단계에 대한 개요를 살펴 봅니다.
작업 옵션을 변경하여 각 단계의 동작을 조정하는 정도의 자세한 내용은 다루지 않습니다.
각 단계에서 작업 옵션이 내부 과정에 미치는 영향은 다음 장에서 구체적으로 설명합니다.

## 데이터셋 입력

영상이 손상되었을 수도 있습니다.
영상에 EXIF 태그로 GPS 좌표의 전체 또는 일부가 포함되기도 합니다.
이 단계에서 입력된 모든 영상에 대해 영상 개수를 세고, 영상의 크기와 GPS 정보를 추출합니다.

**입력:** 영상 + 선택적으로 지상기준점(Ground Control Point, GCP) 파일

**출력:** 영상 데이터베이스

## 모션으로부터 구조 (Structure From Motion, SfM)

SFM (Structure From Motion)은 중첩되는 영상 시퀀스 또는 모션에서
3D 객체(구조)를 추정하기위한 사진 측량 기술입니다.
개략적으로 SFM의 개념은 여러 시점에서 대상을 관찰하면
대상에 대한 많고 완전한 정보를 인식할 수 있다는 직감에서 시작됩니다.
투시(Perspective) 기하와와 광학(각각 깊이있는 책이 필요한 매혹적인 분야)을 사용하여
모든 영상에 대해 카메라의 위치와 각도를 결정할 수 있습니다.
기민한 독자라면 왜 이것이 필요한지 궁금할 것입니다.
사실 모든 사진에 이미 GPS 및 짐벌 정보가 포함되어 있지 않나요?
높은 정확도의 실시간이동측위(RTK)없이 제공되는
GPS 정보는 별로 정확하지 않고,
또한 짐벌 정보는 항상 존재하지도 않습니다.
SFM은 카메라 위치를 훨씬 더 정확하게 계산합니다.
사실 SFM은 GPS 정보 없이도 가능합니다.

SFM은 다음과 같은 몇 가지 단계를 순차적으로 수행합니다.

  - 카메라 정보가 영상의 EXIF 태그에 포함된 경우에는 이를 추출합니다.
    후속 처리 과정에서 광학 방정식에 포함된 카메라 변수(초점 길이, 센서 크기 및 기타)들에 대한 정확한 추정값이 필요합니다.
    이에 대한 가장 근사한 초기 추정치로 EXIF 태그로부터 추출한 정보를 활용하고, 이는 추후 단계에서 개선됩니다.

  - 각 영상으로부터 에지, 특이점, 및 기타 특별한 객체와 같이 쉽게 식별 가능한 요소(feature)를 찾습니다.
    이는 필수적인 단계이며 그 이유를 이해하는 것도 매우 중요합니다.
    만약 하얀 벽에 대해 두 장의 사진을 찍었다면, 두 장의 사진이 상호 어떻게 관련되어 있는지 알 수 없습니다.
    예를 들어, 카메라를 왼쪽으로 아니 오른쪽으로 움직였을까요?
    이를 식별 가능한 객체를 공유하는 두 개의 사진과 비교해봅시다.

![흰벽에 대한 두장의 사진 (위) vs. 아기 상어에 대한 두장의 사진(아래).
카메라가 왼쪽으로 이동했다고 말할 수 있나요?\label{fig:shark_wall}](images/figures/shark_wall.jpg)


두 번째 사진 세트에서 카메라가 왼쪽으로 움직였다는 것을 알 수 있습니다.
컴퓨터가 흰 벽 사진에서 어떤 움직임도 인식 할 수 없기때문에 SFM 문제를 해결할 수 없습니다.
다음에 저고도에서 촬영한 잔디밭을 재현하고 싶지 않은 이유가 궁금하다면 그 이유를 알게 될 것입니다.
충분한 식별 가능한 요소들을 없기 때문입니다.

  - 이전 단계에서 찾아놓은 영상 요소들을 이제 서로 비교합니다.
    두 영상간에 많은 요소들이 공유되면 (동일한 객체가 두 영상에 나타남) 영상이 매칭됩니다.
    최적화를 위해 GPS 정보를 사용하여 서로 멀리 떨어져있는 영상처럼 매칭이 아예 불가능한 후보를 제거하기도 합니다.

  - 한 쌍의 영상에서 시작하여 점차적으로 더 많은 영상을 추가하면서, 카메라의 위치와 각도를 추정하고
    동시에 삼각측량된 객체점의 희박한 집합(희소한 점군)을 기록하기 시작합니다.
    이것은 광학(이 경우 빛과 카메라 렌즈와의 상호 작용에 대한 물리 법칙),
    투시 기하 (2D 표면에 3D 물체를 표현하는 것에 대한 연구) 및
    카메라 정보와 위치와 자세, 결과적으로 삼각측량된 점들의 위치에 대한 최대한 일관성있는 추정값을 생성하는 근사 방법을 통해 달성됩니다.

![SFM 문제. 어떤 종류의 카메라가 사진을 찍었으며,
사진을 찍었을 때 카메라는 어디에 있었습니까?\label{fig:sfm_problem}](images/figures/sfm_problem.png)

사진측량은 새로운 분야가 아니며 그 역사는 수백 년 전으로 거슬러 올라갑니다[^photogrammetry].
단지 최근에은 컴퓨터가 정말 훌륭하고 빠르게 수행한다는 것을 발견했을 뿐입니다.
자세한 내용을 배우고자 하는 사람들을 위해 coursera.org는 이 주제에 대해 정말 좋은 강의를하고 있습니다.[^coursera_sfm].
ODM은 SFM 문제를 효율적으로 해결하기 위해 OpenSfM[^opensfm] (Open Structure From Motion)이라는 소프트웨어 패키지를 사용합니다.

**Input**: 영상 + 지상기준점(선택사항) 

**Output**: 카메라 위치/자세 + 희박한 점군 + 변환

## 멀티 뷰 스테레오(Multi-View Stereo, MVS)

SFM은 주로 카메라 위치/자세 추정에 중점을 둔 반면,
MVS는 다수의 중첩 영상에서 3D 모델을 재구성하는 데 중점을 둡니다.
카메라 정보가 이전 단계에서 계산되었으므로,
MVS 프로그램은 매우 상세한 3D 점의 집합, 즉 “고밀도 점군” 생성에 집중합니다.
ODM은 현재 MVS에 대한 두 가지 옵션을 제공합니다.

  - Multi-View Environment (MVE),  TU Darmstadt[^mve]에서 개발한 소프트웨어 제품군

  - OpenSfM, 앞에서 기술한 OpenSfM은 고밀도 점군 생성 기능도 있습니다.

![많은 갯수의 3D 점들로 구성된 고밀도 점군.\label{fig:dense_point_cloud}](images/figures/dense_point_cloud.jpg)

**입력**: 영상 + 카메라 위치/자세 + (선택적으로) 저밀도 점군

**출력**: 고밀도 점군

## Meshing

When you think of a *3D model* you most likely imagine the type of
models you see in videogames or movies. These models are more precisely
called *polygonal meshes* or *meshes* for short.

![3D mesh\label{fig:3d_mesh}](images/figures/3d_mesh.jpg)

Whenever a 3D model is scanned or derived from a photogrammetry process,
the result is typically represented with 3D points. To go from 3D points
to polygonal meshes we have to perform two steps:

1.  “Connect the dots” using many triangles to obtain a mesh. Points may
    be moved or eliminated to create a better looking mesh.

2.  Add color to the mesh (a process referred to as *texturing*).

Meshing is the process of “connecting the dots”. ODM supports two
different algorithms for meshing and uses one or the other depending on
the situation and the user settings:

1.  Screened Poisson Surface Reconstruction is a robust, memory
    efficient and battle tested algorithm for creating 3D surfaces by
    Michael Kazhdan[^poisson_recon]. It’s used for generating full 3D models with
    a high degree of accuracy.

2.  dem2mesh[^dem2mesh] is a program I developed for generating 2.5D meshes.
    2.5D meshes are meshes that look 3D, but are simple *extrusions* of
    a 2D surface. These are used for generating orthophotos, as
    orthophotos do not require full 3D models for rendering and results
    often tend to look better.

![From 3D points to mesh\label{fig:from_3d_points_to_mesh}](images/figures/from_3d_points_to_mesh.jpg)

**Input**: dense or sparse point cloud 

**Output**: 3D and 2.5D meshes

## Texturing

At this point the mesh does not have any colors associated with it. It’s
just a polygon soup. Texturing is the process of adding colors to
meshes. It does so by using specially computed images (texture images)
and by assigning each polygon to a section of the texture images. The
process of creating the texture images and creating the associated
mappings is performed by MvsTexturing[^mvs_texturing], a software also developed
at TU Darmstadt. At a very high level the program works as follows:

  - Loads camera poses and images from the SFM process.

  - Loads the mesh.

  - For each polygon in the mesh, it finds the best image to fill it.

  - It creates one or more texture images based on the information from
    the step above, also checking and attempting to remove moving
    objects (cats, cars, etc.).

  - Sections of the texture images are color adjusted to compensate for
    differences in illumination.

  - The borders (*seams*) between neighboring sections are blended to
    reduce color differences.

![Textured mesh\label{fig:textured_mesh}](images/figures/textured_mesh.jpg)

Due to some randomness in the texturing algorithm, you are not
guaranteed to get the same results if you run the process twice on the
same mesh. That’s why you might notice that the same dataset processed
twice yields slightly different looking models (and orthophotos).

**Input**: images + camera poses + meshes 

**Output**: textured meshes

## Georeferencing

Up to this point all outputs have been represented using a *local*
coordinate system (a made up coordinate system). A local coordinate
system has no correlation to real world positions. Georeferencing is the
process of converting (*transforming*) a local coordinate system into a
world coordinate system. ODM can do this only if location information
about the world is available, either via GPS coordinates embedded in the
input images or a GCP file. When GPS coordinates are available, they are
incorporated during the SFM step to align the reconstruction as to
minimize the error between all the GPS locations and the computed camera
positions. When GCPs are available, the GPS information is ignored and
GCPs are used for the alignment instead. One of the outputs of the SFM
step is a *transform* file, which allows the georeferencing step to
convert point clouds and 3D models from local to world coordinates.

Once georeferenced, the point cloud is used to generate an estimate of
the geographical boundaries of the dataset. These boundaries are used in
subsequent steps for cropping orthophotos and digital elevation models
(DEMs).

**Input**: transform + point cloud + textured meshes 

**Output**: georeferenced point cloud + textured meshes + crop boundaries

## Digital Elevation Model Processing

Point clouds are cool to look at, but much analysis is usually done
using simpler 2D DEMs, which represent XY coordinates as pixel locations
on the screen and pixel intensities (or colors) as elevation values.
During this step ODM takes the georeferenced point cloud and extracts a
surface model by using an inverse distance weighting interpolation
method. If there are any holes in the model (perhaps an area is
missing), they are filled using interpolation. Finally, the model is
smoothed using a median filter to remove noise (bad values). With
certain settings it can also attempts to classify the point cloud into
ground vs. non-ground points and generate a terrain model by first
removing all non-ground points. Finally, the results are cropped.

![Digital surface model\label{fig:digital_surface_model}](images/figures/digital_surface_model.png)

**Input**: georeferenced point cloud + crop boundaries 

**Output**: digital surface models + digital terrain models + classified
georeferenced point cloud

## Orthophoto Processing

The orthophoto is generated by taking a picture of the textured 3D mesh
from the top. A dedicated program loads the textured mesh into an
orthographic scene and saves the result to an image using the
appropriate resolution. The image is then georeferenced and converted to
a GeoTIFF using the information computed in the georeferencing step.
Finally, the result is cropped.

![Orthographic camera taking a picture of the 3D model from the top\label{fig:orthographic_camera_picture}](images/figures/orthographic_camera_picture.png)

![Orthophoto\label{fig:orthophoto}](images/figures/orthophoto.png)

**Input**: textured mesh + crop boundaries 

**Output**: orthophoto

We covered a general overview of the processing pipeline to describe how
ODM goes from images to end results. We avoided going into too much
detail about the specific implementation of each step, since any effort
to discuss implementation details would inevitably be inaccurate or even
obsolete by the time this book is completed. The beauty of open source
is that you don’t need a manual to tell you the details of the
implementation. Those interested can and are encouraged to go look for
details directly from the source code[^odm_source].

Now we turn our eyes to one of the most practical and important topics
of this book: mastering the long list of task options and understanding
what in the world each one does.

[^photogrammetry]: History of Photogrammetry: <http://wayback.archive-it.org/all/20090227061949/http://www.ferris.edu/faculty/burtchr/sure340/notes/History.pdf>
[^coursera_sfm]: Robotics: Perception: <https://www.coursera.org/learn/robotics-perception>
[^opensfm]: OpenSfM: <https://github.com/mapillary/OpenSfM/>
[^mve]: Multi-View Environment: <https://github.com/simonfuhrmann/mve>
[^poisson_recon]: Screened Poisson Surface Reconstruction: <http://www.cs.jhu.edu/~misha/Code/PoissonRecon/Version8.0/>
[^dem2mesh]: dem2mesh: <https://github.com/OpenDroneMap/dem2mesh>
[^mvs_texturing]: MvsTexturing: <https://github.com/nmoehrle/mvs-texturing>
[^odm_source]: ODM Stages Source Code: <https://github.com/OpenDroneMap/ODM/tree/master/stages>