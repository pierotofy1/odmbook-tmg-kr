# About The Author

![Piero Toffanin](images/figures/piero_toffanin.jpg)

Piero Toffanin is a software developer currently focused on geospatial
and drone software development. He has been working on open source
software for over 21 years. Since 2016 he is an OpenDroneMap core
developer and frequently speaks at conferences about OpenDroneMap,
geospatial and open source.

<https://piero.dev>

<https://masseranolabs.com>
