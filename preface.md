# Preface

“If you're wondering who's in charge of writing documentation, you are.”
 
> --Piero Toffanin

I never thought I'd eventually end up writing a book about OpenDroneMap. I made my first code contribution to the project in 2016, after buying a drone and discovering that software can automatically turn 2D images into 3D models and maps. I was intrigued by the process and OpenDroneMap was one of the few open source programs that I could manage to get up and running. At the time the program was difficult to use and worked only from the command line. So over a few days I contributed a rough user interface. That interface later evolved into the NodeODM project. People noticed, loved it and asked for more. So that was the start of the WebODM project. My involvement stepped up once I started diving into the processing engine's internals and making some major contributions there along the way. At the time the program was changing so rapidly that even writing some simple documentation seemed like an impossible task. It would be obsolete in a few months, so why bother?

Today the software is still rapidly changing, but the general structure of the program is much more defined, making an attempt at documenting it feasible. People in the meanwhile kept asking for a comprehensive guide. So, one day I decided to take up the effort and write it. Thus, OpenDroneMap: The Missing Guide was born.

I decided to offer it as a book and not as an online resource for several reasons:

 * A book has a more discursive format and allows the information to be presented in a more linear fashion.
 * The project already has an online reference documentation and I didn't want to rewrite the work others have already made. This book does not replace the online documentation, it expands it.
 * It gives people an opportunity to financially support the project.

I'm aware that for some people buying a book might not be an option. Reasons can range from financial hardship to the inability of making purchases with a credit card. To mitigate this problem, I have setup a page on the book's website at [odmbook.com](https://odmbook.com) where people can apply for a free or discounted copy. Furthermore, if you purchased this book and you know somebody who is unable to get it, please feel free to forward them your copy (just please don't share it on a public site).

As soon as a second edition of the book is written, I pledge to release this book for everyone to download freely.

I have tried my best to write in a style that a complete novice could understand, while keeping it technical enough for advanced users to gain valuable insights. I have favored simplicity over correctness when discussing concepts, knowing that scholarly readers will know how to recognize the shortcomings of my descriptions and where to lookup the more formal definitions.

I'm not a professional writer and English is not my first language, so I hope the reader will forgive me for the occasional awkwardness in sentence structures or a misspelling that might have been missed during review.

I believe that constructive criticism is a key component to learning and improving. How was the book? What could be improved for the next edition? What was not clear?
If you have feedback or any other comment in general, please feel free to drop me a note: pt@masseranolabs.com.

Enjoy the book!

-Piero