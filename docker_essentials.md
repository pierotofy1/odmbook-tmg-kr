# 도커 핵심사항 (Docker Essentials)
\label{docker_essentials}

 대부분의 오픈드론맵(OpenDroneMap) 프로젝트들은 설치와 관리 도구로서 도커(Docker)를 광범위하게 사용한다. 만약 당신이 그것을 다뤄본 적이 없다면, 아마도 혼란스럽고 이유 없이 당신의 디스크 공간만 먹고 있는 것 같이 보이며 가끔은 암호 같은 메시지와 함께 예기치 않은 오류가 발생할 것이다.

 그렇지만 좋으나 싫으나 도커는 여기 존재한다. 이번 장에서는 도커의 개념과 오픈드론맵을 보다 효율적으로 사용하는데 적용할 수 있는 몇 가지 기본명령들을 다룬다. 필자는 독자들이 이번 장을 따라가면서 직접 터미널에서 명령어를 입력하는 것을 권장한다. 이미 당신이 “명령행(The Command Line)” 챕터에서 설명된 개념에 대해 익숙하다고 가정한다.

## 도커 기초 (Docker Basics)

도커는 너무 기술적이지 않으면서 사람들이 소프트웨어와 모든 종속성을 *docker images*로 감싸는 도구이다.
이 *images* 를 프로그램으로 생각하자.
각 이미지는 하나 이상의 *containers*를 시작하는데 사용할 수 있다. 컨테이너는 소프트웨어 인스턴스를 실행하는 것으로 생각해보자. 비유하자면 메모장이 도커 이미지일 때, 메모장 프로그램을 3번 실행시키는 것은 3개의 컨테이너를 실행하는 것과 유사하다.

![Docker Example](images/figures/docker_example.png)

하지만 일반적인 프로그램들과 달리 도커 이미지는 빌드된 전체 운영체제를 가지고 있다! 이는 다른 많은 장점 중에서도 다른 운영체제에서 Linux용으로 작성된 프로그램을 실행시킬 수 있는 것이다. 물론 그것에 따른 단점(추가적인 공간, 약간의 오버헤드 등)도 존재하지만 인생은 얻는 것이 있으면 잃는 것도 있다. 그렇지 않은가?

도커 이미지는 이름과 다음의 규칙을 따른다

    사용자 명/이미지 명[:태그]

이름의 **:태그** 부분은 선택사항이며 생략하면 기본값은  **latest**이다. 예를 들어 Windows에서 ODM 프로세스를 시작하는데 일반적으로 사용하는 전체 명령은 다음과 같다.

    $ docker run -ti --rm -v //d/odmbook/project:/datasets/code opendronemap/odm --project-path /datasets

위의 명령어는 도커에게 **opendronemap/odm:latest** 이미지를 사용하여 새 컨테이너를 시작하도록 요청하고 있다.

  - **-ti** (**-t -i**의 단축) docker에게 터미널을 만들고 열어 두도록 요청. (심지어 창을 닫아도) 이것을 전달하는 것을 그냥 기억하자. 그러지 않으면 명령의 출력을 볼 수 없다.

  - **--rm** 컨테이너가 완료되면 컨테이너를 제거하도록 요청(기본적으로 컨테이너는 완료된 후 폐기되지 않고 *중지됨* 상태로 유지됨).

  - **-v** 볼륨을 맵핑(볼륨에 대한 설명은 아래에서 설명한다).

마지막으로  **--project-path** 옵션 플래그를 사용하여 컨테이너 내부의 ODM 프로세스로 전달한다.

**docker run** 명령의 문법은 다음과 같다.

    docker run [도커 옵션들] [이미지 명] [프로그램 옵션들]

## 컨테이너 관리 (Managing Containers)

앞에서 언급한 데로 컨테이너는 기본적으로 제거되지 않는다. 컨테이너는 실행 중인 상태에서 시작이 되고 완료되면 중지된 상태로 전환된다. 실행 명령에 **--rm** 플래그를 전달하는 것을 잊었을 때 어떤 일이 발생하는지 보자.

    $ docker run -ti -v //d/odmbook/project:/datasets/code opendronemap/odm --project-path /datasets

명령이 중지되면 다음을 실행하여 모든 컨테이너를 나열해 보자.

    $ docker ps -a
    
    CONTAINER ID        IMAGE                             COMMAND                  CREATED             STATUS                        PORTS               NAMES
    ab89e4b71b65        opendronemap/odm                  "python /code/run.py..."   9 minutes ago       Exited (1) 9 minutes

**opendronemap/odm** 컨테이너가 종료된 후에 제거되지 않는 것을 볼 수 있다(이것은 **--rm** 을 전달하지 않았기 때문이다). **ps** **-a** 명령은 실행 중과 중지됨에 상관없이 모든 컨테이너를 보여준다.

각 컨테이너는 다른 명령에서 해당 컨테이너를 참조하는데 사용될 수 있는 고유 식별자(또는 *해시(hash)*)가 있다.

예를 들어, 이 컨테이너의 해시는 **ab89e4b71b65** 이다.

이 컨테이너를 지우기 위해서는 단지 다음과 같이 타이프하면 된다.

    $ docker rm ab89e4b71b65

충돌하는 해시가 없는 경우 해시의 시작 문자를 하나 이상 입력하여 해시를 빠르게 입력 할 수도 있다.

    $ docker rm ab8

만약 *Error response from daemon: You cannot remove a running container* 와 같은 에러 메시지를 받았다면 그것은 오직 중단된 상태의 컨테이너만 제거할 수 있기 때문이다.

컨테이너를 중지 하는 것은 다음과 같다.

    $ docker stop ab8

컨테이너가 제거되었는지 확인할 수 있다.

    $ docker ps -a
    
    CONTAINER ID        IMAGE                             COMMAND                  CREATED             STATUS                        PORTS               NAMES

컨테이너를 생성하고 나열하고 제거하는 것이 편안해져야 한다.


**run** 명령을 위한 두 가지 중요한 플래그는 **-d** 와 **-p:** 이다.

    $ docker run -d -p 3000:3000 opendronemap/nodeodm
    
    ab89e4b71b65

**-d**  플래그는 백그라운드에서 컨테이너를 시작하는데 사용할 수 있다. 컨테이너가 백그라운드에서 시작되면 콘솔은 컨테이너와 *연결* 되지 않고 생성된 컨테이너의 해시를 즉시 돌려보낸다.
이런 방법으로 새 터미널 창을 열지 않고도 여러 컨테이너를 시작할 수 있다. **-p** 플래그는 컨테이너로부터의 네트워크 포트를 노출해서 외부로부터 그곳을 접근할 수 있도록 한다.

    -p <port of computer>:<port inside container>

상단의 예제에서 **opendronemap/nodeodm** 이미지는 3000번 포트에서 실행되도록 구성된 웹 서버가 포함되어 있다. **-p 3000:3000** 를 전달하여 docker 가 우리의 컴퓨터에서 가능하다면 3000번 포트로 웹 서버(컨테이너 **내부**에서 3000번 포트에서 실행 중)를 만들도록 요청한다.
혼란스러워 보일 수도 있지만 이렇게 다음과 같이 멋진 작업을 하도록 할 수도 있다.

    $ docker run -d -p 3000:3000 opendronemap/nodeodm
    $ docker run -d -p 3001:3000 opendronemap/nodeodm

3000번 포트와 3001번 포트에서 각자 다른 두 개의 개별 NodeODM 인스턴스가 시작된다.

## 이미지 관리 (Managing Images)

Docker 이미지는 Dockerfile[^dockerfile]로 부터 만들 수 있다. 이것은 docker에게 특정 이미지를 생성하는 방법을 알려주는 텍스트 파일이다.

반드시 여러분 자신만의 이미지를 만들 필요는 없다. 오픈드론맵 개발자는 이미 모든 사람이 사용할 수 있도록 Docker 이미지를 빌드하고 배포하였다.
이곳을 방문하여 사용 가능한 이미지를 확인 할 수 있다. [https://hub.docker.com/r/opendronemap/](https://hub.docker.com/r/opendronemap).

자신만의 이미지를 만들면 몇 가지 장점들이 있다. 소프트웨어를 수정 할 수 있으며 때에 따라서 (주로 ODM에 한하여) 약간의 속도향상을 얻을 수도 있다! 예를 들어 공개된 **opendronemap/odm** 이미지는 다양한 컴퓨터(오래된 것과 새것)를 지원하도록 구축되었으므로 최신 컴퓨터에서만 사용할 수 있는 특정 최적화가 비활성화되어있다. 만약 당신이 반짝이는 새 컴퓨터를 소유한 경우에는 고유한 이미지를 작성하여 이러한 최적화를 활용할 수 있다. ODM 이미지를 직접 만들려면 먼저 ODM의 소스 코드를 다운로드한 다음 Dockerfile이 포함된 폴더로 이동한 후 다음을 타이프한다.

    $ docker build -t 내사용자명/odm .

그것을 빌드(시간이 오래 걸림)한 후 **내사용자명/odm** 으로 당신만의 이미지를 실행할 수 있다.

    $ docker run --rm -ti [...] 내사용자명/odm [...]

**docker run**을 처음 실행한다면 Docker는 먼저 요청한 이미지가 컴퓨터에 이미 존재하는지 확인을 한다. 만약 존재한다면 Docker는 그것을 실행시키고 그렇지 않다면 docker는 [hub.docker.com](https://hub.docker.com) 에서 그것을 내려받는 것을 시도한다.
당신의 컴퓨터에 존재하는 이미지를 나열하는 것은 다음과 같다.

    $ docker images
    
    REPOSITORY                   TAG                 IMAGE ID            CREATED             SIZE
    opendronemap/odm             latest              f2275dac6ee1        22 hours ago        3.14GB

모든 이미지에는 고유 식별자(또는 *해시(hash)*)가 있다. 시간이 지남에 따라 더이상 일부 이미지는 필요하지 않을 수도 있으며 오래된 이미지를 제거하여 디스크공간을 확보할 수 있다.
예를 들어 **opendronemap/odm** 이미지를 제거하는것은 다음과 같다.

    $ docker rmi f22

[hub.docker.com](https://hub.docker.com)에서 새 이미지를 사용할 수 있는 경우(예를 들어, 새 버전의 ODM이 사용할 수 있는 경우) 당신은 **pull** 명령을 사용하여 새 버전을 내려받을 수 있도록 수동으로 지정하여야 한다.

    $ docker pull opendronemap/odm

## 볼륨 관리 (Managing Volumes)

여기 이지점까지 우리는 이 **run** 명령을 꽤 많이 사용했다.

    docker run -ti -v //d/odmbook/project:/datasets/code opendronemap/odm --project-path /datasets

그러나 **-v** //d/odmbook/project**:/datasets/code** 부분이 하는 것은 무엇일까?

그것은 맵핑된 볼륨을 생성하는 것이다

    -v <컴퓨터에서의 경로>:<컨테이너에서의 경로>

컨테이너는 격리된 환경이다. 컨테이너는 실행되는 동안 컴퓨터와는 별도로 자체의 내부 디렉터리 구조를 갖는다.
컨테이너가 컴퓨터의 일부 파일에 접근할 수 있게 하려면 컨테이너를 구체적으로 그것을 허용해야 하며 컨테이너 내부에서 우리의 파일들에 접근할 수 있는 위치를 지정해야만 합니다.

![Docker 볼륨 맵핑. **D:\textbackslash odmbook\textbackslash project** 에서의 파일들은 컨테이너의 **/datasets/code** 경로\label{fig:docker_volume_mapping}에서 사용할 수 있게 된다](images/figures/docker_volume_mapping.png)

만약 네트워크 드라이브를 설정한 경우  **G:\textbackslash folder**(심지어 **폴더**는 원격 컴퓨터에 있는 폴더가 될 수 있다) 와 같은 경로 접근할 수 있도록 네트워크 위치를 드라이브에 매핑한다.

Docker 볼륨은 네트워크 드라이브와 비슷하지만, 컴퓨터에서 컨테이너까지이다.

그래도 이해가 가지 않는다면 그냥 컨테이너 안팎으로 파일을 가져오려면 매핑된 볼륨을 설정해야 한다는 점을 보편적인 사실로 받아들이자.

다음 예시에서는 우리 컴퓨터의 **D:\textbackslash odmbook\textbackslash project** 폴더를 컨테이너의 **/datasets/code** 디렉터리에서 사용할 수 있도록 만든다.

    컴퓨터 --> 컨테이너
    D:\odmbook\project\images\1.JPG --> /datasets/code/images/1.JPG
    D:\odmbook\project\images\2.JPG --> /datasets/code/images/2.JPG
    ...

ODM 컨테이너의 경로로서 **/datasets/code** 를 선택하는 것에 대한 간단한 참고사항.
 **/datasets** 은 임의의 경로이며 ODM을 실행할 때 **--project-path /datasets** 를 통해 지정한다.

    docker run -ti -v //d/odmbook/project:/datasets/code opendronemap/odm --project-path /datasets

**code** 는 기본으로 *프로젝트명* 이다. 다른 프로젝트 이름을 명시적으로 지정하고 다음과 같이 다시 작성할 수도 있다.

    docker run -ti -v //d/odmbook/project:/datasets/example1 opendronemap/odm --project-path /datasets example1

 개발자는 게으르기(좋은 종류의 게으름) 때문에 프로젝트 이름이 생략되면 기본으로 **code**가 되고 따라서 그것은 작성하기에 짧다.

Windows 사용자들을 위한 주의사항: Docker Toolbox (Windows 7/8 및 10 Home 사용자)를 사용하여 볼륨을 매핑하려면 매핑하려는 경로를 공유해야 하거나 추가 구성을 처리하지 않으면 사용자의 홈 폴더(C:\textbackslash Users\textbackslash youruser\textbackslash )[^folder] 내에 있는 폴더만 매핑하려고 한다.

일부 독자는 볼륨 경로 앞에 하나 대신 두 개의 슬래시를 붙인 것을 알 수 있을 것이다. 이것은 Windows에서의 Git Bash의 형식으로 작성된 것이다. macOS와 Linux에서는 두 개의 슬래시를 생략할 수 있다.

## 도커-컴포즈 기본 (Docker-Compose Basics)

docker가 개별 컨테이너를 실행하는 데 사용되는 반면 docker-compose는 여러 컨테이너를 실행하는데 사용된다. docker-compose 를 사용하는 응용프로그램의 예로 WebODM을 살펴보는 것이 도움 된다. 예를 들어 WebODM은 여러 구성요소로 구성된다.

  - 웹 어플리케이션 (opendronemap/webodm\_webapp)

  - 데이터베이스 (opendronemap/webodm\_db)

  - 메시지 브로커 (library/redis)

  - 프로세스 엔진 (opendronemap/nodeodm 또는
    dronemapper/node-micmac)

WebODM 소스코드의 **docker-compose\*.yml** 파일들을 살펴보면 WebODM을 만드는 부분들을 볼 수 있다.

이 .yml (YAML) 파일들은 docker-compose의 동작을 제어한다. 이 책의 2장 (part II)에서  WebODM을 다음과 같이 시작했었다.

    ./webodm.sh start

우리가 했던 것은 docker-compose를 시작하는 것이었다. 사실 *webodm.sh*은 대부분 docker-compose의 인터페이스였다. 그것은 여러 컨테이너의 시작을 조정하고 다른 컨테이너보다 먼저 시작될 컨테이너와 저장소 구성 방법 및 기타 여러 가지 기능들을 결정하는 방법을 제공한다.

Docker-compose에 대한 완벽한 개요를 제공하는 것은 이 책의 범위를 벗어나지만, 관심 있는 독자라면 Docker 문서[^documentation]에서 완벽한 가이드를 찾아볼 수 있다. 나는 여러분이 텍스트 편집기로  *webodm.sh* 과 다양한 docker-compose\*.yml 파일들을 열어서 그것들이 어떻게 정의되었는지 보는 것을 권유합니다.

Dokcer-compose는 여러 .yml 파일들과 조합될 수 있다. 예를 들면 다음과 같다.

    $ docker-compose -f docker-compose.yml -f docker-compose.nodeodm.yml up

**docker-compose.yml** 에서 구성을 읽고 **docker-compose.nodeodm.yml**에서 구성을 적용하여 이전 구성을 재정의 하거나 확장한다.
이 경우  **docker-compose.nodeodm.yml** (NodeODM 프로세싱 노드를 사용하여 WebODM을 시작한다)은 **docker-compose.yml**(어떠한 프로세싱 노드도 없이 단지 WebODM만 실행한다)에서 확장된다.
**up** 명령은 구성에 정의된 모든 컨테이들을 생성하고 시작하도록 요청한다. 다른 유용한 명령들은 다음과 같다.
 
    컨테이너들을 중지(하지만 그것들을 지우지는 않음)
    $ docker-compose -f docker-compose.yml stop
    
    컨테이너들을 중지하고 그것들을 지움
    $ docker-compose -f docker-compose.yml down
    
    hub.docker.com으로부터 모든 이미지들을 업데이트
    $ docker-compose -f docker-compose.yml pull

## 디스크 공간 관리하기 (Managing Disk Space)

가끔 청소하지 않는다면 시간이 지남에 따라 docker는 행복하게 여러분의 디스크 공간을 전부 먹어 버릴 것이다! 이것은 컨테이너가 제거되지 않거나 많은 이미지가 내려받기 되어있거나 갈 곳을 잃은 볼륨들이 버려졌을 때 발생할 수 있다.
Docker는 여러분에게 자동으로 그것들을 제거하지 않는 호의를 베푸는 것으로 추측된다. (만약 당신이 3년 전에 만들었던 이미지가 필요한 경우라면 어떨까?). 다행히도 정리작업에 유용한 명령이 있고 그것은 다음과 같다.

    $ docker system prune

## 엔트리포인트 변경하기 (Changing Entrypoint)

컨테이너를 시작하고 중단하는 것은 시간이 걸릴 수 있다. 또한 처리에 실패한다면 그것은 무엇이 잘못되었는지 검사하기 어려울 수 있다. 저자는 docker를 사용하여 ODM 데이터 세트를 실행하는 두 단계 접근법을 제안한다.

    $ docker run -ti -v //d/odmbook/project:/datasets/example1 --entrypoint bash opendronemap/odm
    root@898747d1f3a8:/code# ./run.py --project-path /datasets example1
    root@898747d1f3a8:/code# exit
    $ 

**--entrypoint** bash 플래그를 전달하면 docker가 컨테이너의 기본 시작 명령(*run.py*)을 무시하고 bash(Linux 셸)를 대신 실행하도록 요청한다.

**\#** 은 우리가 컨테이너 안에 있고 bash 셸을 실행하고 있음을 확인한다. 이제 직접*run.py*를 호출하여 ODM 프로세스를 실행할 수 있다. 이 방법의 차이점은 문제가 발생하면 문제를 보다 쉽게 확인하고 프로세스를 다시 시작할 수 있다는 것이다. 컨테이너를 종료하기 위해서는 단순히 **exit** 를 타이프 하면 된다.

## 컨테이너에 이름 부여하기 (Assigning Names To Containers)

컨테이너가 많을 경우 해시ID를 기억하는 것보다 실망스러운 일은 없을것이다. 컨테이너를 실행할 때 이름을 지정한 다음 향후 명령에서 해당 이름을 참조할 수 있다.

    $ docker run -d -p 3000:3000 --name mynode opendronemap/nodeodm
    $ docker stop mynode
    $ docker rm mynode

## 존재하는 컨테이너로 넘어가기 (Jumping Into Existing Containers)

가끔은 컨테이너를 다시 시작하지 않고(그 상태를 날릴 수도 있음) 컨테이너 내부에서 무슨 일이 일어나고 있는지 알고 싶을 수도 있다.
예를 들면, 가끔은 WebODM이 정말로 영원히 걸릴 것만 같은 오랜 작업을 다운로드한게 맞는지 궁금하다.

    $ docker ps
    
    CONTAINER ID        IMAGE                             COMMAND                  CREATED             STATUS              PORTS                     NAMES
    aff69c390477        opendronemap/webodm_webapp   "/bin/bash -c 'chmod..."   46 hours ago        Up 3 hours          0.0.0.0:8000->8000/tcp    webapp
    
    $ docker exec -ti aff69c390477 bash
    
    root@aff69c390477:/webodm# ls app/media/project/<id>/task/<id>/assets
    all.zip    images.json    odm_orthophoto/    odm_dem/ ...

**exec** 명령은 실행 중인 컨테이너에 당신이 명령을 실행하는 것을 허용한다. 이 예시에서는 컨테이너가 실행되는 동안 컨테이너의 내용을 검사하기 위한 명령 프롬프트를 제공해주는 bash 셸을 실행하는 것을 결정하였다!

## 이미지를 재빌드 하지 않고 변경하기 (Making Changes Without Rebuilding Images)

이것은 아마도 ODM의 소스 코드에 관심 있는 사람들에게 가장 유요할 것이다. 선택한 IDE를 사용하여 ODM의 소스 코드를 수정한 다음 최신 버전의 ODM을 실행하는 컨테이너를 생성하고 다음과 같이 소스 코드 디렉터리를 컨테이너의 /code 디렉터리에 볼륨 매핑할 수 있다. (소스 코드가 D:\textbackslash ODM 에 있다고 가정)

    $ docker run -ti -v //d/ODM:/code -v //d/odmbook/project:/datasets/example1 --entrypoint bash opendronemap/odm
    root@# bash configure.sh reinstall
    # ./run.py --project-path /datasets example1

이제 D:\textbackslash ODM 에서 파일들을 편집할 수 있으며 변경사항이 컨테이너 안에서 반영된다!

Docker는 강력한 도구이지만 약간 위협적일 수도 있다. 이장이 호기심을 불러일으켜서 그 주변의 수수께끼들을 제거해 주기를 기대한다.
오픈드론맵 소프트웨어를 사용하기 위해 도커 마스터가 될 필요는 없지만 소프트웨어에 익수해지려면 확실히 도움이 될 것이다. 도커 문서 웹사이트[^website]는 지식을 넓히기를 원하는 사람들을 위한 훨씬 포괄적인 리소스이다.
 
[^dockerfile]: Dockerfile Reference: <https://docs.docker.com/engine/reference/builder/>
[^folder]: How to use a directory outside C:\textbackslash Users with Docker Toolbox/Docker for Windows
: <http://support.divio.com/local-development/docker/how-to-use-a-directory-outside-cusers-with-docker-toolboxdocker-for-windows>
[^documentation]: Docker-compose: <https://docs.docker.com/compose/>
[^website]: Docker Documentation: <https://docs.docker.com/>
