# 데이터 처리하기
\label{processing_datasets}

드론을 가지고 있다면, 직접 취득한 데이터를 처리해 보는 것을 추천한다. 다른 사람이 취득한 데이터를 취득하는 것보다 성취감이 높을 것이다. 오픈드론맵은 비행계획이나 비행조정을 위한 어플리케이션을 지원하지는 않지만,
DroneDeploy[^dronedeploy], DJI GSPro[^dji_gs_pro] or QGroundControl[^qgroundcontrol]와 같이 데이터 취득을 위한 무료 어플리케이션이 많이 있으므로 이를 이용하는 것을 추천한다. 데이터 취득 전에 *Flying Tips* 챕터의 내용을 참고하자.
취득이 여의치 않아 샘플 데이터가 필요하다면 community forum[^community_forum]을 이용하면 된다. 해당 페이지에서 다량의 데이터 셋을 무료로 제공하고 있다.

## 데이터셋 크기

독자들의 컴퓨터에 내장된 RAM 용량 때문에 많은 양의 데이터를 처리하지 못할 수도 있다(100장 미만). 현재 글을 작성하는 시점에서 독자분들에게 영상의 수와 해당 영상을 처리하기 위한 메모리 양의 관계를 정확 하게 집어주는 것은 어렵지만, 일반적으로
메모리의 크기가 클수록 처리할 수 있는 영상의 수도 증가한다. 따라서 작은 양의 영상에서부터 시작하여 점진적으로 늘려가며 확인해보는 것을 추천한다. 특정 작업 옵션들은 메모리를 추가적으로 요구할 수도 있다. 특정 작업 옵션에 대한 세부사항은 
*Task Options in Depth*을 참고하기 바란다.

## 파일 Requirements

현재 WebODM은 JPEG 파일만 지원한다. TIFF와 같은 다중밴드 파일의 경우 지원하지 않으나, 이를 위한 작업이 진행중에 있다[^multiband_files].

영상들은 서로 다른 카메라에서 다른 각도로 촬영되었을 수 있다. 대부분의 드론과 핸드폰 영상들은 EXIF(Exchangable Image file Format)로 JPEG 파일에 위치정보를 태깅한다. EXIF 태그는 영상 내 탑재된 정보 중 하나로 영상이 취득된 장소의 지리적 위치를 
포함한다. 지리적 위치정보는 지오레퍼런싱된 정사영상 또는 고도모델을 생성하기 위해 필요하나, 3차원 모델이나 포인트 클라우드를 생성하는데는 필요하지 않다. 지리적 위치정보가 없는 영상을 사용할 수는 있으나 정사영상을 생성할 수 없다는 점에 유의해야 한다.

만일 영상에 지리적 위치정보가 없는 경우, 영상에 위치정보를 추가하기 위해서 지상기준점(Ground Control Point) 파일 또는 Exiftool[^exiftool]과 같은 프로그램 사용할 수 있다. 지상기준점은 *Ground Control Points* 챕터에서 자세히 다루도록 하겠다.

GIMP[^gimp]를 이용하여 영상이 위치정보를 포함하고 있는지 확인해볼 수 있다. GIMP로 영상을 열었다면, **Image** 메뉴에서 **Metadata** - **View Metadata**로 이동한다. **EXIF** 탭에서 GPSAlititude, GPSLatitude, GPSLongitude 태그를 확인할 수 있어야 한다. 
해당 정보를 확인할 수 없다면, 해당 영상의 경우 위치정보를 포함하고 있지 않은 것이다. 해당 정보가 없는 경우, GIMP의 **Image — Metadata — Edit Metadata** 패널에서 GPS 정보를 추가하거나 수정할 수 있다. GIMP는 비교적 시간 소요량이 크므로, 영상이 많은
경우 exiftool을 사용하는 것이 좋다. exiftool을 이용하여 다량의 영상에 지오태깅하는 방법은 *The Command Line* 챕터에서 자세히 다루도록 하겠다.

## 작업 처리

**Select Images and GCP**를 누르거나 영상을 프로젝트에 드래그&드랍하는 것만으로도 작업을 처리를 수행할 수 있다. 또는 해당 버튼을 여러번 클릭하여 서로 다른 폴더에서 다른 영상들을 추가할 수도 있다.

![WebODM’s new task panel\label{fig:webodm_new_task_panel}](images/figures/webodm_new_task_panel.png)

몇 가지 설정할 수 있는 셋팅이 있으며 이는 다음과 같다.
  
  - **Name:** 작업명
  
  - **Processing Node:** **Auto** 버튼은 작업을 위한 노드를 자동으로 선택한다(적업략이 가작 적은 노드를 우선적으로 선택한다). 아니면 작업 처리를 위한 노드를 직접 선택할 수도 있다.

  - **Options:** 사전에 설정된 목록에서 선택할 수도 있다. 마우스 커서를 현재 선택된 사전 설정 위에 올려놓으면 어떠한 옵션들이 설정되어 있는지 확인할 수 있다. 몇몇 기본 사전 설정들을 사용할 수 있으나,
    먼저 설정들을 실험해보고 자체적인 사전설정을 만드는 것을 추천한다. 사전설정 리스트 옆 버튼은 옵션 패널을 편집하는 버튼이며, 그 옆의 화살표 버튼은 현재 사전설정을 저장하거나 편집, 삭제하는 버튼이다. 옵션과 관련하여
    자세한 사항은 *Task Options in Depth* 챕터에서 다루도록 하겠다. 지금은 **Default** 로 진행한다.  
  
  - **Resize Images:** 요구되는 저장공간이나 메모리 양을 줄이고 처리속도를 높이기 위해, 처리 전 영상의 사이즈를 줄일 수도 있다. 다만 결과물의 질은 다소 떨어질 수 있음에 유의한다.
    
준비가 됐다면 **Reivew**를 누르고 **Start Processing**을 누른다. 이후 다음과 같은 처리과정이 일어난다.

1. 영상들이 WebODM 내에 있는 앱/미디어 폴더로 업로드된다. WebODM을 시작할 때 **--media-dir** 변수를 입력하지 않았다면 해당 폴더에는 접근할 수 없다. 이와 관련된 사항은 *Installing The Software* 챕터의 마지막 부분에서 설명한다.
   
2. 처리 노드가 선택되고 영상들이 해당 노드로 전달된다. 해당 과정이 불필요해 보일 수도 있으나 (왜 직접 처리 노드로 업로드 하지 않는가?하는 식의 의문이 떠오를 수도 있다), 처리 노드는 다른 컴퓨터에 위치할 수도 있음을 기억하자.
   
3. 작업이 시작되고, 경과시간과 콘솔 출력과 같은 정보들이 매초 갱신된다.

일단 작업이 완료되면 결과는 해당 노드에서 WebODM으로 전달된다. 작업 결과의 사본은 일반적으로 일정 기간동안 처리 노드에 저장된다(일반적으로 2일 정도 저장한다.). 이는 WebODM이 처리 작업 중간부터 해당 작업을 재시작할 수 있도록 한다.

![WebODM download results](images/figures/webodm_download_results.png)

결과는 다음 두 인터페이스 중 한 가지를 통해 다운로드하거나 확인할 수 있다.

  - **View Map:** 정사영상이나 고도모델을 생성할 수 있는 2D 맵에서 해당 결과를 보여준다. 부피를 측정하거나, 등고선을 생성하는 기능들을 지원한다.
    
  - **View 3D Model:** View 3D Model은 포인트 클라우드를 보여주며, 사용자는 해당 포인트 클라우드에서 몇 가지 작업을 수행할 수 있다. 텍스쳐 모델이 사용 가능한 경우, 검사를 위해 토글을 할 수 있다. 측량, 고도 프로파일 생성, 포인트 클라우드
    영역 편집 등 여러가지 툴을 지원한다.
    
한 프로젝트가 다수의 작업를 포함하고 있다면, 각 프로젝트 우측 상단에서 **View Map** 버튼을 클릭하여 프로젝트의 모든 작업에 대한 정사영상과 고도모델을 동시에 표시할 수 있다.

![Map 뷰\label{fig:map_view}](images/figures/map_view.png)

![3D V뷰\label{fig:3d_view}](images/figures/3d_view.png)

## 생성 결과

**Download Assets** **—** **All Assets**을 통해 결과를 다운로들 했다면, 몇 가지 디렉토리등을 포함하고 있는 아카이브를 확인할 수 있을 것이다.
  
  - **dsm\_tiles** **dsm\_tiles**은 색상 음영 디지털 표면 모델에 대한 타일을 포함하고 있다. 해당 타일을 이용하여 Cesium이나 Leaflet과 같은 뷰어를 이용하여 웹상에서 결과를 시각화할 수 있다.
  
  - **dtm\_tiles** **dtm\_tiles** 은 위와 동일하나 디지털 지리 모델용이다.
  
  - **orthophoto\_tiles** **orthophoto\_tiles**도 위와 동일하나, 정사영상용이다.
  
  - **odm\_dem** **odm\_dem**은 DEM을 저장하고 있다.
  
  - **odm\_georeferencing**  **odm\_georeferencing**은 Dense point clouds를 저장하고 있다.
  
  - **odm\_texturing** **odm\_texturing**은 3D 텍스쳐 모델을 저장한다(지리참조된 버전과 참조되지 않은 버전 모두 포함).
  
  - **entwine\_pointcloud** **entwine\_pointcloud**는 plas.io or potree[^plasio_potree]와 같은 뷰어를 이용하여 웹에서 효율적으로 스트리밍할 수 있는 포인트 클라우드를 보여준다.
    
## 다른 사람과 공유하기

**Map View** 또는 **3D Model**에서 **Share**를 클릭하면 다른 사람과 공유할 수 있는 링크를 생성한다. 이때 해당 링크는 WebODM이 공공 IP 주소 상에 설치되었을 경우에만 유효하다. 개인 로컬컴퓨터에서 WebODM이 실행되었을 경우 
방화벽이나 로우터 등의 이유로 다른 사람과 해당 결과를 공유할 수 없을 수도 있다. 일반적으로 다른 사람과 결과를 공유하고 싶은 경우 WebODM을 공공 서버에 설치한다.

## 다른 WebODM으로 결과 추출하기

WebODM에서 생성된 결과는 다른 WebODM에서 다운로드하거나 불러올 수 있다. **Download Assets — All Assets** 을 클릭하여 작업을 다운로드한 후 **Dashboard**에서 **Import**를 눌러 해당 작업을 수행할 수 있다.

## 플러그인 관리하기

WebODM에 있는 몇 기능들은 플러그인을 통해 구현할 수 있다. 기본적으로 많은 플러그인들이 사용 가능하며 **Administration — Plugins**에 들어가서 직접 해제할 수도 있다.

## 외관 변경하기

**Administration — Theme**과 **Administration — Brand** 패널에서 색, 로고, 작업명을 변경할 수도 있다.

## 신규 사용자 생성하기

**Administration — Accounts** 패널에서 새로운 계정을 생성할 수 있다.

## 접근 관리하기

사용자에 따라 프로젝트를 생성하거나 접근할 수 있는 권한을 차등적으로 부여할 수 있으며, 해당 작업은 **Administration** — **Application** 패널에서 수행할 수 있다. **Project**를 클릭한 후, 접근권한 등을 변경하고 싶은 프로젝트를 선택한다. 
다음 화면의 우상단에서 **Object Permissions**을 클릭한다.

![객체 허가 버튼\label{fig:object_permissions_button}](images/figures/object_permissions_button.png)

기본적으로 프로젝트는 해당 프로젝트를 생성한 사용자에게 귀속되며, 프로젝트 내 모든 작업들은 접근권한을 그대로 승계한다. 해당 시스템의 관리자는 (또는 *superusers*) 모든 리소스에 접근할 수 있으나, 일반 사용자의 경우 자신이 생성한 프로젝트에만 
접근할 수 있다. 사용자 간에 프로젝트를 공유하기 위해서는 특정 **Manage User** 버튼을 누른 후 특정 사용자의 이름을 입력하면 되고, 그룹의 경우 **Manage Group** 버튼을 누른 후 그룹명을 입력해 줘야 한다.

## 어떻게 WebODM은 영상을 처리하는가?

만일 해당 작업이 첫 번째 작업이거나, 처음으로 작업했던 때를 기억하면, "어떻게 컴퓨터가 단순하 2차원 영상을 가지고 3차원 포인트 클라우드 또는 지리참조된 모자이크로 변환할 수 있지?"라고 생각하며 ***워후!*** 라고 환호성을 질렀을 수도 있다.

이것에 관해서는 다음 챕터에서 알아보도록 한다.

[^dronedeploy]: DroneDeploy: <https://dronedeploy.com>
[^dji_gs_pro]: DJI GS Pro: <https://www.dji.com/ground-station-pro>
[^qgroundcontrol]: QGroundControl: <http://qgroundcontrol.com>
[^community_forum]: OpenDroneMap Community Forum - Datasets: <https://community.opendronemap.org/c/datasets>
[^multiband_files]: Add support for GeoTIFF images: <https://github.com/OpenDroneMap/ODM/issues/865>
[^exiftool]: Exiftool: <https://www.sno.phy.queensu.ca/~phil/exiftool/>
[^gimp]: GIMP: <https://www.gimp.org>
[^plasio_potree]: Viewing Entwine Data: <https://entwine.io/quickstart.html#viewing-the-data>