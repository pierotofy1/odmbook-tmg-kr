# Glossary

**2.5D Model:** A model where elevation is simply *extruded* from the
ground plane and thus is not a true 3D model.

**Artifacts:** undesired
alterations generated as the result of a digital process. 

**API:**
Application Programming Interface. A set of functions allowing the
creation of applications that access the features or data of another
application. 

**AWS:** Amazon Web Services is a cloud service provider.

**Bundle Adjustment:** a refinement step during the Structure From
Motion process that improves the location of cameras, the 3D points of
the scene and the camera parameters. 

**CloudODM:** A command line tool
to process aerial imagery in the cloud. 

**ClusterODM:** A NodeODM API
compatible autoscalable load balancer and task tracker for connecting
multiple NodeODM nodes under a single network address. 

**CRS:**
Coordinate Reference System. A CRS is a coordinate-based system used to
locate geographical entities. 

**CSV:** Comma Separated Value is a
textual file format where fields are typically separated by commas or
some other character such as a space or a tab. 

**cURL:** a software
providing a library and command-line tool for transferring data using
many protocols. **DEM**: Digital Elevation Model (either a DSM or a
DTM). **Depthmap**: An image containing distance information for objects
in a scene relative to the camera plane. 

**Docker:** a tool used to
launch *containers*, lightweight standalone packages of software.
OpenDroneMap uses docker to run many of its software. Docker is also the
name of the company that develops the tool. **DSM**: Digital Surface
Model. A 2D representation of elevation that includes terrain,
buildings, trees and other structures. **DTM**: Digital Terrain Model. A
2D representation of elevation that includes terrain only. 

**EXIF:**
Exchangeable file format for images and their auxiliary tags. EXIF tags
are pieces of information embedded within an image. They can include
information such as camera model, GPS location of where the image was
shot, focal length information and many more. **GCP**: Ground Control
Point. A GCP is a position measurement made on the ground, often taken
at the location of a clearly identifiable marker, to increase the
georeferencing accuracy of a reconstruction. 

**GSD:** Ground Sampling
Distance. In an aerial photo, it’s the distance between pixels measured
on the ground. 

**Mesh:** A collection of vertices, edges and faces that
define the shape of a 3D model. A mesh does not include color
information such as textures. 

**MVE:** Multi-View Environment is a suite
of software packages developed to ease the work with multi-view datasets
and to support the development of algorithms based on multiple views. It
features Structure from Motion, Multi-View Stereo and Surface
Reconstruction algorithms. 

**MVS:** Multi-View Stereo is a branch of
study in computer vision that focuses on the reconstruction of 3D models
from multiple overlapping image pairs. MVS programs expect that
information about cameras has already been computed. 

**NodeODM:** A
lightweight REST API to access aerial image processing engines such as
ODM or MicMac. 

**Noise:** An unwanted interference. When applied to
point clouds, it indicates points that should not be present or that
were missed during outlier filtering. **Nadir**: The direction pointing
directly below a particular location. 

**ODM:** A command line toolkit to
generate maps, point clouds, 3D models and DEMs from drone, balloon or
kite images. 

**OpenSfM:** Open source structure from Motion library
written in Python. The library serves as a processing pipeline for
reconstructing camera poses and 3D scenes from multiple images.
**Orthophoto**: An image that has been *orthorectified*, warped in such
a way that distances and scales are uniform. **Photogrammetry**: the
process of obtaining reliable information about physical objects and the
environment through the process of using photographic images.

**Preemptive Matching:** During the Structure From Motion process, the
act of reducing the possible number of pair candidates by using the
location information stored in the EXIF tags of the images. **PyODM**: A
Python SDK for adding aerial image processing capabilities to
applications. 

**RTK:** Real Time Kinematics. A satellite navigation
technique used to enhance the precision of position data derived from
satellite-based positioning systems. 

**SDK:** Software Development Kit.
A set of libraries, tools and examples that help software developers to
build software with a particular technology. 

**SFM:** Structure From
Motion is a photogrammetry technique for estimating 3D objects
(structures) from overlapping image sequences (from motion).
**Texturing**: The act of creating 2D images suitable for use on 3D
models, also known as *texture maps* or *texture skins*. Textures give
3D models a realistic appearance. **UAV**: Unmanned Aerial Vehicle. An
aircraft piloted by remote control or on-board computers. **UI**: User
Interface. 

**UTM:** Universal Transverse Mercator is a coordinate
reference system. It ignores altitude and treats the earth as a perfect
ellipsoid. **Virtualization**: the act of creating a virtual version of
computer hardware platforms, storage devices, and computer network
resources. **VM**: Virtual Machine. A software program or operating
system that works like a separate computer. **WebODM**: User-friendly,
extendable application and API for processing aerial imagery. 

**WSL:**
Windows Subsystem for Linux. A feature of Windows 10 that allows Linux
programs to run natively on Windows.