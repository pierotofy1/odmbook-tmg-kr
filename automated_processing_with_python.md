# Automated Processing With Python
\label{automated_processing_with_python}

What’s better than aerial data processing? Automated aerial data
processing of course!

항공 데이터 처리보다 나은 것은 무엇일까? 물론 자동 항공 데이터 처리다!

In *The NodeODM API* chapter we’ve learned how to use the NodeODM API by
using cURL. Now we will learn about PyODM, a Python library for
communicating with the NodeODM API. Python is a programming language
that is used by many OpenDroneMap projects and is popular language in
the GIS community. This chapter will not try to teach the fundamentals
of Python, as there are already plenty of free online resources for that, such as <https://www.python.org/about/gettingstarted/>. Previous knowledge of Python is preferred, but not required.
Python is a very descriptive language and readers should be able to
follow the examples even without formal training in Python.

우리는 *NodeODM API* 장에서 cURL을 이용해 NodeODM API를 사용하는 방법을 배웠다. 이제 NodeODM API와 통신하기 위한 Python 라이브러리인 PyODM에 대해 알아보자. Python은 많은 OpenDroneMap 프로젝트에서 사용되는 프로그래밍 언어이며 GIS 커뮤니티에서 널리 사용되는 언어다. 이미 <https://www.python.org/about/gettingstarted/>와 같은 무료 온라인 리소스가 많이 있으므로, 이 장에서는 Python의 기본 사항에 대해 언급하지는 않을 것이다. 파이썬에 대한 사전 지식이 있으면 좋지만, 필수는 아니다. 파이썬은 매우 설명적인 언어이므로 독자들은 파이썬에 대한 정규적인 교육 없이도 예제를 따라올 수 있을 것이다.

Why would you want to use Python (instead of cURL or CloudODM)? With
Python you can leverage the image processing capabilities of ODM to
create new, custom applications that have an aerial image processing
component. For example, you could build:

cURL나 CloudODM 대신 Python을 사용하려는 이유는 무엇일까? Python을 사용하면 ODM의 이미지 처리 기능을 활용해 항공 이미지 처리 구성 요소가 있는 새로운, 사용자 지정(custom) 응용 프로그램을 만들 수 있다. 예를 들어 다음을 빌드 할 수 있다.

  - A platform for counting trees using modern computer vision
    techniques after a user uploads drone images.

  - 사용자가 드론 이미지를 업로드 한 후 최신 컴퓨터 비전 기술을 사용하여 나무를 계산하는 플랫폼.

  - An application that detects when SD cards are inserted in a computer
    and automatically processes images without user interaction.

  - SD 카드가 컴퓨터에 삽입된 시기를 감지하고 사용자와 상호작용없이 이미지를 자동으로 처리하는 응용 프로그램.

  - An application for extracting video frame segments from YouTube and
    automatically generating 3D reconstructions from scenes of your
    favorite movies.

  - YouTube에서 비디오 프레임을 추출하고 좋아하는 영화 장면에서 3D 재구성을 자동으로 생성하는 응용 프로그램.

Obviously each of these applications can be complex and requires coding
skills, but PyODM would help you with the image processing part of the
implementation.

분명히 이러한 각 응용 프로그램은 복잡할 수 있고 코딩 기술이 필요하지만, PyODM은 이미지 처리 구현에 도움을 줄 수 있다.

It should be noted that PyODM does a bit more than a simply
communicating with NodeODM, as it deals with things such as managing
parallel downloads (a sort of download accelerator which makes network
transfers faster), parallel uploads, automatic retries for fault
tolerance, as well as dealing with backward-compatibility issues between
API versions.

PyODM은 병렬 다운로드(네트워크 전송 속도를 높이는 일종의 다운로드 가속기), 병렬 업로드, 고장 허용 범위 내 자동 재시도 뿐만 아니라 API 버전 간의 호환성 문제를 처리하므로 NodeODM과 단순히 통신하는 것 이상의 역할을 한다.

## Getting Started

If you have already installed Python (see *Installing The Software*
chapter), all you need to do from a terminal is type:

Python을 이미 설치한 경우에는(*Installing The Software* 장 참조) 터미널에서 해야 할 일은 다음과 같다.

    $ pip install -U pyodm

which will install the *pyodm* package. Afterwards, make sure to start a
NodeODM instance via:

*pyodm* 패키지가 설치된다. 그런 다음 다음을 통해 NodeODM 인스턴스를 시작하면 된다.

    $ docker run -d -p 3000:3000 opendronemap/nodeodm

You can use any text editor you want to write Python code. I prefer to
use the free and open source Visual Studio Code (<https://code.visualstudio.com/>) but any text
editor will do. All examples below are also available for download from
<https://github.com/MasseranoLabs/odmbook-assets>.

어떤 텍스트 편집기를 이용해 Python 코드를 작성할 수 있다. 무료, 오픈 소스인 Visual Studio Code (<https://code.visualstudio.com/>)를 선호하지만, 모든 텍스트 편집기가 사용가능하다. 아래의 모든 예제는 <https://github.com/MasseranoLabs/odmbook-assets>에서도 다운로드 할 수 있다.

## Example 1: Hello NodeODM

Type the following program into a new file using your text editor, then
save it as **hello.py**.

텍스트 편집기를 사용하여 다음 프로그램을 새 파일로 입력 한 다음 **hello.py**로 저장한다.

```python
from pyodm import Node, exceptions

node = Node('localhost', 3000)
try:
    print(node.info())
except exceptions.NodeConnectionError as e:
    print("Cannot connect: " + str(e))
```

Then run it:

다음을 실행한다.

    $ python hello.py
    
    {'version': '1.5.2', 'task_queue_count': 0, 'total_memory': 1021136896, 'available_memory': 436518912, 'cpu_cores': 2, 'max_images': None, 'max_parallel_tasks': 2, 'engine': 'odm', 'engine_version': '0.6.0', 'odm_version': '?'}

We have successfully retrieved the NodeODM instance information. We also
check for any connection errors just in case we have forgotten to launch
the NodeODM instance and print an error message if we can’t connect to
the node.

NodeODM 인스턴스 정보를 성공적으로 검색했다. 또한 NodeODM 인스턴스 시작을 잊는 경우에 대비해 연결 오류가 있는지 확인하고, 노드에 연결할 수 없다면 오류 메시지를 출력한다.

There are a few different types of errors (*exceptions* in Python
jargon) that we can handle:

처리할 수 있는 몇 가지 다른 유형의 오류(Python jargon의 *exceptions*)가 있다.

  - **OdmError**: A generic catch-all exception related to anything
    PyODM

  - **OdmError**: PyODM과 관련된 일반적인 catch-all 예외

  - **NodeServerError**: The server replied in a manner which we did not
    expect. Usually this indicates a temporary malfunction of the node

  - **NodeServerError**: 서버가 예상하지 못한 방식으로 응답했다. 일반적으로 이는 노드의 일시적인 오작동을 나타낸다

  - **NodeConnectionError**: A connection problem (such as a timeout or
    a network error) has occurred

  - **NodeConnectionError**: 연결 문제(예: 시간 초과 또는 네트워크 오류)가 발생했다

  - **NodeResponseError**: The node responded with an error message
    indicating that the requested operation failed

  - **NodeResponseError**: 요청된 작업이 실패했음을 나타내는 오류 메시지가 노드에 응답했다.

  - **TaskFailedError**: A task did not complete successfully

  - **TaskFailedError**: 작업이 성공적으로 수행되지 않았다

## Example 2: Process Datasets

For this example, save it as **process.py** and place the file in the
same folder where some aerial images are stored (e.g.
**D:\textbackslash odmbook\textbackslash project\textbackslash images**):

이 예제에서는 다음을 **process.py**로 저장하고 파일을 항공 이미지가 저장된 동일한 폴더에 위치시킨다. (예: **D:\textbackslash odmbook\textbackslash project\textbackslash images**)

```python
import glob
from pyodm import Node, exceptions

node = Node("localhost", 3000)

try:
    # Get all JPG files in directory
    images = glob.glob("*.JPG") + glob.glob("*.jpg") + glob.glob("*.JPEG") + glob.glob("*.jpeg")

    print("Uploading images...")
    task = node.create_task(images, {'dsm': True, 'orthophoto-resolution': 2})
    print(task.info())

    try:
        def print_status(task_info):
            msecs = task_info.processing_time
            seconds = int((msecs / 1000) % 60)
            minutes = int((msecs / (1000 * 60)) % 60)
            hours = int((msecs / (1000 * 60 * 60)) % 24)
            print("Task is running: %02d:%02d:%02d" % (hours, minutes, seconds), end="\r")
        task.wait_for_completion(status_callback=print_status)

        print("Task completed, downloading results...")

        # Retrieve results
        def print_download(progress):
            print("Download: %s%%" % progress, end="\r")
        task.download_assets("./results", progress_callback=print_download)

        print("Assets saved in ./results")
    except exceptions.TaskFailedError as e:
        print("\n".join(task.output()))

except exceptions.NodeConnectionError as e:
    print("Cannot connect: %s" % e)
except exceptions.OdmError as e:
    print("Error: %s" % e)
```

Then run it via:

다음을 통해 실행한다.

    $ pwd
    /d/odmbook/project/images
    
    $ python process.py
    
    Uploading images...
    {'uuid': 'cc751818-36af-41e9-92d2-bc146cdee10c', 'name': 'Task of 2019-06-16T21:00:02.502Z', 'date_created': datetime.datetime(2019, 6, 16, 21, 0, 2), 'processing_time': 1, 'status': <TaskStatus.RUNNING: 20>, 'last_error': '', 'options': [{'name': 'orthophoto-resolution', 'value': 2}, {'name': 'dsm', 'value': True}], 'images_count': 18, 'progress': 0, 'output': []}
    Task completed, downloading results...
    Assets saved in ./results

This is a more comprehensive example. First, we create a **Node**
instance. From that instance we can create new tasks via
**create\_task()** which take as input a list of image paths (which we
generate via the **glob** function) and returns a **Task** instance. We
then wait for the results to be ready via **wait\_for\_completion()**
and we ask to be notified of status updates by displaying how long the
task has been running for. If a task fails at any point in time,
**wait\_for\_completion()** raises a **TaskFailerError.** We “catch”
that error and display the task output to the user if that happens. When
the task completes, we download the results and display download
progress information.

이는 보다 포괄적인 예제다. 먼저 **Node** 인스턴스를 만든다. 이 인스턴스에서 **create\_task()**로 새 작업을 만들 수 있다. **create\_task()**는 이미지 경로 목록(**glob** 함수를 통해 생성)을 입력으로 사용하고, **Task** 인스턴스를 반환한다. 그 다음 **wait\_for\_completion()**을 통해 결과가 준비 될 때까지 기다리고, 작업 실행 시간을 표시하여 상태 업데이트에 대한 알림을 요청한다. 어떤 시점에서 작업이 실패하면 **wait\_for\_completion()**은 **TaskFailerError**를 발생시킨다. 우리는 해당 오류를 "잡아" 그 작업이 발생하면 사용자에게 작업 출력을 표시한다. 작업이 완료되면 결과를 다운로드하고 다운로드 진행 정보를 표시한다.

## Concluding Remarks

PyODM is a simple module that makes it straightforward to leverage the
capabilities of OpenDroneMap with Python. It’s also a module used within
ODM and WebODM and will continue to be well supported in the future. If
you need to use NodeODM with a different programming language, you can
use PyODM as a reference to write a client for your language of choice.

PyODM은 Python으로 OpenDroneMap의 기능을 간단하게 활용할 수있는 간단한 모듈이다. PyODM은 ODM과 WebODM에서 사용되는 모듈이고 앞으로도 계속 지원 될 것이다. 다른 프로그래밍 언어와 함께 NodeODM을 사용해야하는 경우, PyODM을 참조해 선택한 언어로 클라이언트를 작성할 수 있다.

## API Reference

The following reference is from PyODM version 1.5.2b (the latest version
as of the writing of this book). The reference to the latest version can
be found online at <https://pyodm.readthedocs.io>

다음은 PyODM 버전 1.5.2b(이 책을 쓰는 시점의 최신 버전)에 대한 참고 문헌이다. 최신 버전에 대한 참고 문헌은 <https://pyodm.readthedocs.io>에서 온라인으로 찾을 수 있다.

### class pyodm.api.Node(host, port, token=’’, timeout=30)

A client to interact with NodeODM API.

NodeODM API와 상호작용하는 클라이언트.

Parameters:

  - **host** (*str*) – Hostname or IP address of processing node

  - **host** (*str*) – 처리 노드의 호스트 이름이나 IP 주소

  - **port** (*int*) – Port of processing node

  - **port** (*int*) – 처리 노드의 포트

  - **token** (*str*) – token to use for authentication

  - **token** (*str*) – 인증에 사용할 토큰

  - **timeout** (*int*) – timeout value in seconds for network requests

  - **timeout** (*int*) – 네트워크 요청에 대한 시간 초과 값(초)

**create\_task**(*files*, *options{}*, *name=None*,
*progress\_callback=None*, *skip\_post\_processing=False*,
*webhook=None*, *outputs=[]*, *parallel\_uploads=10*,
*max\_retries=5*, *retry\_timeout=5*)

Start processing a new task. At a minimum you need to pass a list of
image paths. All other parameters are optional.

새 작업 처리를 시작한다. 최소한 이미지 경로 목록을 전달해야 한다. 다른 매개 변수는 선택 사항이다.

Parameters:

  - **files** (*list*) – list of image paths + optional GCP file path.

  - **files** (*list*) – 이미지 경로 목록 + GCP 파일 경로(선택 사항).

  - **options** (*dict*) – options to use, for example
    {‘orthophoto-resolution’: 3, ...}

  - **options** (*dict*) – 사용할 옵션, 예: {‘orthophoto-resolution’: 3, ...}

  - **name** (*str*) – name for the task

  - **name** (*str*) – 작업 이름

  - **progress\_callback** (*function*) – callback reporting upload
    progress percentage

  - **progress\_callback** (*function*) – 업로드 진행률 콜백

  - **skip\_post\_processing** (*bool*) – When true, skips generation of
    map tiles, derivate assets, point cloud tiles.

  - **skip\_post\_processing** (*bool*) – 참이면 맵 타일, 파생 자산, 포인트 클라우드 타일을 생성하지 않는다.

  - **webhook** (*str*) – Optional URL to call when processing has ended
    (either successfully or unsuccessfully).

  - **webhook** (*str*) – 처리가 종료되었을 때 호출할 선택적 URL(성공 또는 실패)

  - **outputs** (*list*) – Optional paths relative to the project
    directory that should be included in the all.zip result file,
    overriding the default behavior.

  - **outputs** (*list*) – all.zip 결과 파일에 포함되어야 하는 프로젝트 디렉토리에 대한 선택적 경로로 기본 동작을 대체한다.

  - **parallel\_uploads** (*int*) – Number of parallel uploads.

  - **parallel\_uploads** (*int*) – parallel uploads의 수.

  - **max\_retries** (*int*) – Number of attempts to make before giving
    up on a file upload.

  - **max\_retries** (*int*) – 파일 업로드 포기 전에 시도한 횟수.

  - **retry\_timeout** (*int*) – Wait at least these many seconds before
    attempting to upload a file a second time, multiplied by the retry
    number.

  - **retry\_timeout** (*int*) – 파일을 다시 업로드하기 전에 재시도 횟수를 곱한 후 최소한 몇 초를 기다려야 한다.

Returns: **Task()**

*static* **from\_url**(*url*, *timeout=30*)

Create a Node instance from a URL.

URL에서 Node 인스턴스를 생성한다.

    >>> n = Node.from_url("http://localhost:3000?token=abc")

Parameters:

  - **url** (*str*) – URL in the format
    proto://hostname:port/?token=value

  - **url** (*str*) – proto://hostname:port/?token=value 형태의 URL

  - **timeout** (*int*) – timeout value in seconds for network requests

  - **timeout** (*int*) – 네트워크 요청에 대한 시간 초과 값(초)

Returns: **Node()**

**get\_task**(*uuid*)

Helper method to initialize a task from an existing UUID

기존 UUID에서 작업을 초기화하기 위한 helper 메소드

Parameters:

**uuid** – Unique identifier of the task

**uuid** – 작업 고유 식별자

**info**()

Retrieve information about this node

해당 노드에 관한 정보를 검색한다

Returns: **NodeInfo()**

**options**()

Retrieve the options available for creating new tasks on this node.

이 노드에서 새 작업을 생성하는데 사용 가능한 옵션을 검색한다.

Returns: [**NodeOption()**]

**url**(*url*, *query={}*)

Get a URL relative to this node.

이 노드와 관련된 URL을 가져온다.

Parameters:

  - **url** (*str*) – relative URL

  - **url** (*str*) – 관련 URL

  - **query** (*dict*) – query values to append to the URL

  - **query** (*dict*) – URL에 추가할 검색 값

Returns: Absolute URL (str)

**version\_greater\_or\_equal\_than**(*version*)

Checks whether this node version is greater than or equal than a certain
version number

이 노드 버전이 특정 버전 번호 이상인지 확인

Parameters:

**version** (*str*) – version number to compare 

**version** (*str*) – 비교할 버전

Returns: bool

### class pyodm.api.Task(node, uuid)

A task is created to process images. To create a task, use
**create\_task()**.

이미지 처리를 위한 작업이 생성된다. **create\_task()**로 작업을 생성한다.

Parameters:

  - **node** (**Node()**) – node this task belongs to

  - **node** (**Node()**) – 해당 작업이 속한 노드

  - **uuid** (*str*) – Unique identifier assigned to this task.

  - **uuid** (*str*) – 이 작업에 할당된 고유 식별자

**cancel**()

Cancel this task. 

작업 취소 

Returns:

task was canceled or not (bool)

작업 취소 혹은 유지 (bool)

**download\_assets**(*destination*, *progress\_callback=None*,
*parallel\_downloads=16*, *parallel\_chunks\_size=10*)

Download this task’s assets to a directory.

이 작업의 결과물을 디렉토리로 다운로드

Parameters:

  - **destination** (*str*) – directory where to download assets. If the
    directory does not exist, it will be created.

  - **destination** (*str*) – 결과물을 다운로드 할 디렉토리. 디렉토리가 없으면 디렉토리가 생성된다.

  - **progress\_callback** (*function*) – an optional callback with one
    parameter, the download progress percentage

  - **progress\_callback** (*function*) – 하나의 매개변수를 가진 선택적 콜백, 다운로드 진행률

  - **parallel\_downloads** (*int*) – maximum number of parallel
    downloads if the node supports http range.

  - **parallel\_downloads** (*int*) – 노드가 http 범위를 지원하는 경우의 최대 병렬 다운로드 수

  - **parallel\_chunks\_size** (*int*) – size in MB of chunks for
    parallel downloads

  - **parallel\_chunks\_size** (*int*) – 병렬 다운로드를 위한 MB 단위의 청크 크기

Returns:

path to saved assets (str)

결과물 저장할 경로 (str)

**download\_zip**(*destination*, *progress\_callback=None*,
*parallel\_downloads=16*, *parallel\_chunks\_size=10*)

Download this task’s assets archive to a directory.

이 작업의 결과물 아카이브를 디렉토리로 다운로드한다.

Parameters:

  - **destination** (*str*) – directory where to download assets
    archive. If the directory does not exist, it will be created.

  - **destination** (*str*) – 결과물 아카이브를 다운로드 할 디렉토리. 디렉토리가 없으면 디렉토리가 생성된다.

  - **progress\_callback** (*function*) – an optional callback with one
    parameter, the download progress percentage.

  - **progress\_callback** (*function*) – 하나의 매개변수를 가진 선택적 콜백, 다운로드 진행률

  - **parallel\_downloads** (*int*) – maximum number of parallel
    downloads if the node supports http range.

  - **parallel\_downloads** (*int*) – 노드가 http 범위를 지원하는 경우의 최대 병렬 다운로드 수

  - **parallel\_chunks\_size** (*int*) – size in MB of chunks for
    parallel downloads

  - **parallel\_chunks\_size** (*int*) – 병렬 다운로드를 위한 MB 단위의 청크 크기

Returns:

path to .zip archive file (str)

.zip 아카이브 파일 경로 (str)

**info**(*with\_output=None*)

Retrieves information about this task. 

이 작업에 대한 정보를 검색한다.

Returns: **TaskInfo()**

**output**(*line=0*)

Retrieve console task output.

콘솔 작업 출력을 검색한다.

Parameters:

**line** (*int*) – Optional line number that the console output should
be truncated from. For example, passing a value of 100 will retrieve the
console output starting from line 100. Negative numbers are also
allowed. For example -50 will retrieve the last 50 lines of console
output. Defaults to 0 (retrieve all console output). 

콘솔 출력이 시작되는 줄 번호. 예를 들어 100을 전달하면 100 행부터 콘솔 출력이 검색된다. 음수도 허용된다. 예를 들어, -50은 마지막 50 행의 콘솔 출력을 검색한다. 기본값은 0이다(모든 콘솔 출력 검색).

Returns: console output (one list item per row) ([str])

콘솔 출력 (행당 하나의 list item) ([str])

**remove**()

Remove this task. 

이 작업을 삭제한다.

Returns: task was removed or not (bool)

작업이 제거되거나 유지된다 (bool)

**restart**(*options=None*)

Restart this task.

이 작업을 재시작한다.

Parameters:

**options** (*dict*) – options to use, for example
{‘orthophoto-resolution’: 3, ...} 

사용할 옵션, 예를 들어 {‘orthophoto-resolution’: 3, ...} 

Returns: task was restarted or not (bool)

작업이 재시작되거나 유지된다 (bool)

**wait\_for\_completion**(*status\_callback=None*, *interval=3*,
*max\_retries=5*, *retry\_timeout=5*)

Wait for the task to complete. The call will block until the task status
has become **COMPLETED()**. If the status is set to **CANCELED()** or
**FAILED()** it raises a TaskFailedError exception. 

작업이 완료 될 때까지 기다린다. 작업 상태가 **COMPLETED()**가 될 때까지 통화가 차단된다. 상태가 **CANCELED()**나 **FAILED()**로 설정되면 TaskFailedError 예외가 발생한니다.

Parameters:

  - **status\_callback** (*function*) – optional callback that will be
    called with task info updates every interval seconds.

  - **status\_callback** (*function*) – 매 일정 간격마다 작업 정보 업데이트와 함께 호출되는 선택적 콜백.

  - **interval** (*int*) – seconds between status checks.

  - **interval** (*int*) – 상태 확인 사이의 시간(초)

  - **max\_retries** (*int*) – number of repeated attempts that should
    be made to receive a status update before giving up.

  - **max\_retries** (*int*) – 작업을 중단하기 전에 상태 업데이트를 받기 위해 반복적으로 시도해야 하는 횟수

  - **retry\_timeout** (*int*) – wait N\*retry\_timeout between
    attempts, where N is the attempt number.

  - **retry\_timeout** (*int*) – 시도 사이에 N\*retry\_timeout 만큼을 기다린다. 여기서 N은 시도 횟수이다.

### class pyodm.types.NodeInfo(json)

Information about a node

Node에 관한 정보

Parameters:

  - **version** (*str*) – Current API version

  - **version** (*str*) – 현재 API 버전

  - **task\_queue\_count** (*int*) – Number of tasks currently being
    processed or waiting to be processed

  - **task\_queue\_count** (*int*) – 처리 중이거나 처리 예정인 작업의 수

  - **total\_memory** (*int*) – Amount of total RAM in the system in
    bytes

  - **total\_memory** (*int*) – 총 RAM의 크기 (bytes 단위)

  - **available\_memory** (*int*) – Amount of RAM available in bytes

  - **available\_memory** (*int*) – 사용가능한 RAM의 양 (bytes 단위)

  - **cpu\_cores** (*int*) – Number of virtual CPU cores

  - **cpu\_cores** (*int*) – 가상 CPU 코어의 수

  - **max\_images** (*int*) – Maximum number of images allowed for new
    tasks or None if there’s no limit.

  - **max\_images** (*int*) – 새 작업에 허용된 이미지의 최대 개수, 제한이 없다면 None

  - **max\_parallel\_tasks** (*int*) – Maximum number of tasks that can
    be processed simultaneously

  - **max\_parallel\_tasks** (*int*) – 동시에 처리할 수 있는 작업의 최대 개수

  - **odm\_version** (*str*) – Current version of ODM (deprecated, use
    engine\_version instead)

  - **odm\_version** (*str*) – ODM의 현재 버전(더이상 사용되지 않음, 대신 engine\_version 사용)

  - **engine** (*str*) – Lowercase identifier of the engine (odm,
    micmac, ...)

  - **engine** (*str*) – 엔진의 소문자 식별자 (odm, micmac, ...)

  - **engine\_version** (*str*) – Current engine version

  - **engine\_version** (*str*) – 현재 엔진 버전

### class pyodm.types.NodeOption(domain, help, name, value, type)

A node option available to be passed to a node.

노드에 전달할 수 있는 옵션.

Parameters:

  - **domain** (*str*) – Valid range of values

  - **domain** (*str*) – 값의 유효범위

  - **help** (*str*) – Description of what this option does

  - **help** (*str*) – 해당 옵션의 기능 설명

  - **name** (*str*) – Option name

  - **name** (*str*) – 옵션 이름

  - **value** (*str*) – Default value for this option

  - **value** (*str*) – 해당 옵션의 기본값

  - **type** (*str*) – One of: [‘int’, ‘float’, ‘string’, ‘bool’,
    ‘enum’]

  - **type** (*str*) – [‘int’, ‘float’, ‘string’, ‘bool’, ‘enum’] 중 하나

### class pyodm.types.TaskInfo(json)

Task information

작업 정보

Parameters:

  - **uuid** (*str*) – Unique identifier

  - **uuid** (*str*) – 고유식별자

  - **name** (*str*) – Human friendly name

  - **name** (*str*) – 사람 친화적 이름

  - **date\_created** (*datetime*) – Creation date and time

  - **date\_created** (*datetime*) – 생성 날짜 및 시간

  - **processing\_time** (*int*) – Milliseconds that have elapsed since
    the start of processing, or -1 if no information is available.

  - **processing\_time** (*int*) – 처리 시작 후 경과 된 밀리 초 단위 시간, 정보가 없는 경우에는 -1

  - **status** (**pyodm.types.TaskStatus()**) – status (running, queued,
    etc.)

  - **status** (**pyodm.types.TaskStatus()**) – 상태 (running, queued, etc.)

  - **last\_error** (*str*) – if the task fails, this will be set to a
    string representing the last error that occured, otherwise it’s an
    empty string.

  - **last\_error** (*str*) – 작업이 실패하면 마지막으로 발생한 오류를 나타내는 문자열로 설정, 그렇지 않으면 빈 문자열.

  - **options** (*dict*) – options used for this task

  - **options** (*dict*) – 이 작업에 사용되는 옵션

  - **images\_count** (*int*) – Number of images (+ GCP file)

  - **images\_count** (*int*) – 이미지 개수(+ GCP 파일)

  - **progress** (*float*) – Percentage progress (estimated) of the task

  - **progress** (*float*) – 작업의 진행률(추정)

  - **output** (*[str]*) – Optional console output (one list item per
    row). This is populated only if the with\_output parameter is passed
    to info().

  - **output** (*[str]*) – 선택적 콘솔 출력 (행당 하나의 목록 항목). with\_output 매개 변수가 info()로 전달 된 경우에만 채워진다.

### class pyodm.types.TaskStatus

Task status

작업 상태

Parameters:

  - **QUEUED** – Task’s files have been uploaded and are waiting to be
    processed.

  - **QUEUED** – 작업 파일이 업로드되어 처리 대기 중이다.

  - **RUNNING** – Task is currently being processed.

  - **RUNNING** – 작업이 현재 처리 중이다.

  - **FAILED** – Task has failed for some reason (not enough images, out
    of memory, etc.

  - **FAILED** – 어떤 이유로 작업이 실패했다(이미지가 부족하거나 메모리 부족 등).

  - **COMPLETED** – Task has completed. Assets are be ready to be
    downloaded.

  - **COMPLETED** – 작업이 완료되었다. 결과물을 다운로드 할 준비가 되었다.

  - **CANCELED** – Task was manually canceled by the user.

  - **CANCELED** – 사용자가 작업을 수동으로 취소했다.