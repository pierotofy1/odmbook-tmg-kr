# 명령행(The Command Line)
\label{the_command_line}

이 책의 첫 부분에서는 ODM을 친숙한 그래픽적 인터페이스인 WebODM으로 사용하는 방법을 살펴보았다. WebODM은 ODM의 일부 복잡성을 숨기지만 그 편리함에는 비용이 따른다. WebODM에서 쉽게 할 수 없는 몇 가지 사항들은 다음과 같다.

  - 업로드 하지 않고 작업처리.

  - 중간 결과 파일 검사.

  - 파이프라인의 임의 지점에서 작업을 다시 시작 (WebODM은 작업 다시시작을 지원하지만 일부만 지원한다)

  - 시간 만료 없이 작업을 다시시작 (WebODM 은 NodeODM 설정이 변경되지 않는 한 작업 완료 후 2 일 이내에만 작업을 다시 시작할 수 있다).

이번 장에서는 사용자 인터페이스의 편안함을 떠나서 명령행을 
사용하여 ODM을 직접 작업을 처리하며 파워 유저의 영역으로 뛰어들게 된다.


 이것은 다른 운영체제를 위하면 명령행을 사용하는 법을 철저히 안내하지는 않습니다. ODM을 사용하기 위한 목적을 위해 알아야할 기본 명령에 대한 간략한 개요이다.

이미 명령행과 친숙하다면 이 장을 넘겨도 된다.

## 명령행 기초(Command Line Basics)

먼저 “명령행”은 사용자가 명령을 입력하여 상호 작용할 수 있는 응용프로그램임을 분명히 하야한다. 다양한 명령행 응용 프로그램들이 있으며 각 운영체제마다 고유한 특징들을 지니고 있다. 세 가지 주요 운영체제(Windows macOS, Linux)에 통일된 지침셋을 제공하기 위해 우리가 “명령행”이라고 말할 때에는 항상 “Bash” 또는 그것의 변형 판 가운데 하나를 지칭한다.

  - Windows: **Git Bash** 를 사용한다 (*소프트웨어 설치(Installing The Software)* 장의 지침을 따라 설치).
   명령 프롬프트 또는 파워 셸을 사용하지 **마시오**.

  - macOS: **Terminal** 앱을 사용한다.

  - Linux: 대부분의 배포판은 이미 기본적으로 **Bash** 를 사용하지만 셸이 다른 경우 터미널에서 **bash**를 입력하여 bash 셸을 시작한다.

다음은 숙지해야 할 몇 가지 명령어이다. 명령행을 열었으면 다음의 명령어들을 입력해보자:

  - **ls -al:** 파일과 디렉터리들을 나열

  - **cd \<dir\>**: 디렉터리 변경

  - **pwd**: 현재 디렉터리를 표시

  - **cat \<file\>**: 파일의 내용을 표시

  - **head -n \<lines\> \<file\>:** 파일의 첫 번째 줄을 표시

  - **tail -n \<lines\> \<file\>:** 파일의 마지막 줄을 표시

  - **find . -name \*.JPG:** : 현재 디렉터리(와 하위 디렉터리)에서 모든 JPG 파일들을 찾기

  - **whoami:** 현재 사용자의 이름을 표시

  - **chown -R $(whoami):$(whoami) \<directory\>**: 디렉터리의 소유 권한을 현재 사용자와 그룹으로 변경

  - **sudo \<command\>**: 관리자 권한으로 명령 실행 (Linux 와 Mac 한정)

위의 명령들에서  **--help** 플래그를 추가하면 사용법 정보와 함께 명령에 대한 설명이 표시된다.

    $ ls --help
    사용법: ls [<옵션>]... [<파일>]...
    List information about the FILEs (the current directory by default).
    Sort entries alphabetically if none of -cftuvSUX nor --sort is specified.
    ...

Bash 에서의 경로는 슬래시를 이용하여 구분한다 (예시, **/c/Users/myuser**). 이것은 백 슬래시를 사용하는 Windows 사용자에게는 때때로 혼동의 원인이 된다 (예시, **C:\textbackslash Users\textbackslash myuser**). 경로와 파일 이름 또한 대소문자를 구분하므로, 
**/c/file** 는 **/c/FiLe** 과 다르게 인식된다.

명령을 입력하는 동안 **TAB**키를 누르면 경로가 자동 완성될 수 있다. 예를 들어 현재 **/c** 에서 **/c/myVeryLongPathname** 이 있는 경우는 다음과 같다.

    $ pwd
    /c
    $ cd myV<TAB 키를 누른다>
    $ cd myVeryLongPathname/ <-- 자동으로 완성됨

디렉터리에서 한 수준 위로 이동하려면 특수한 
“. .” (두 개의 온점)  디렉터리를 사용할 수 있으며 다음과 같이 사용된다.

    $ pwd
    /c/dir
    $ cd ..
    $ pwd
    /c

 **cd** 와 두 개의 온점 사이에 공백이 있음을 유의하자.

## ODM을 사용하기 (Using ODM)

이제 **cd** 명령을 사용하여 디렉터리들을 탐색하는 방법을 알게 되었으니 몇 가지 이미지들을 당신이 선택한 디렉터리 (예.
**C:\textbackslash odmbook\textbackslash projects\textbackslash test\textbackslash images**) 에 위치시키고 탐색해본다.

    $ cd /c/odmbook/

윈도우에서 아래의 명령어를 실행하는 동안 에러 메시지 *the
input device is not a TTY. If you are using mintty, try prefixing the
command with ‘winpty’* 를 받았다면 다음을 입력하여 합니다.

    echo "alias docker='winpty docker'" >> ~/.bash_profile

그리고 진행하기 전에 Git Bash를 재시작 한다.

ODM으로 이미지들을 처리하기 시작하려면 다음과 같이 실행할 수 있다.

    $ docker run -ti --rm -v /$(pwd)/projects/test:/datasets/code opendronemap/odm --project-path /datasets [options]

**[options]** 의 자리에 *Task Options in Depth* 장에서 다루었던 테스크 옵션을 추가 할 수 있다. 
예를 들어 정사영상의 해상도를 변경하고 DSM을 생성하고 MVE(Multi-View Stereo) 단계로부터 작업을 다시 시작하려면 다음과 같이 실행할 수 있다.

    $ docker run -ti --rm -v /$(pwd)/projects/test:/datasets/code opendronemap/odm --project-path /datasets --orthophoto-resolution 2 --dsm --rerun-from mve

만약 사용 가능한 옵션들을 잊어버렸다면 다음과 같이 실행하여 확인할 수 있다.

    $ docker run -ti --rm opendronemap/odm --help

위의 Docker 명령어들이 불길해 보여도 걱정하지 마라. 다음 챕터에서 Docker에 대해 더욱 깊이 있게 알아보고 그것을 길들이는 방법에 대해 다루고 있다.

## 루트가 소유한 처리된 파일들(Processed Files Owned By Root)

Linux 와 Mac 에서는 이미지 처리가 완료되면 결과 파일을 변경하거나 삭제할 수 없다.
 이는 Docker 컨테이너 내에서 출력파일이 생성되는 Docker의 특성이고 
 컨테이너는 root(관리자) 사용자와 함께 실행되므로
  모든 파일들 역시 root가 소유한다. 파일들을 다시 제어하려면 다음을 실행하시오.

    $ sudo chown -R $(whoami):$(whoami) /path/to/project

다음 명령어를 통해 디렉터리의 소유자를 확인할 수 있다. 

    $ ls -al
    drwxrwxrwx  2 foo bar 4.0K Jun 10 18:02 images

위 출력에서의 **images** 디렉터리는 *foo* 사용자와 *bar* 그룹이 소유한다.


## WebODM에 새로운 프로세스 노드를 추가하기(Add New Processing Nodes to WebODM)

만약 다른 컴퓨터를 가지고 있다면 다음을 입력하여 새로운 NodeODM 노드를 실행시킬 수 있다.

    docker run --rm -it -p 3000:3000 opendronemap/nodeodm -q 1 --token secret

이 명령은 Docker에게 **opendronemap/nodeodm** 이미지(NodeODM의 최신버전)와 3000번 포트, 최대동시 작업 수는 1 그리고 미승인된 접근으로부터 보호하기 위한 암호 *secret* 을 사용하여 새로운 컨테이너를 시작하도록 요청한다.

WebODM에서 **Processing Nodes** 메뉴 아래의 **Add New** 버튼을 누를 수 있다. *호스트명/IP* 필드에 그 컴퓨터의 IP를 입력한다. *포트* 필드에 *3000*을 입력한다. *토큰* 필드에 *secret* 을 입력한다. 또한 이 노드를 위해 *라벨* 을 선택적으로 추가할 수 있다.
그리고 **저장** 버튼을 누른다.

이제 여러 장비들을 사용하여 여러 작업을 병렬로 처리 할 수 있다.

## Exiftool을 사용하여 이미지의 지오 태깅을 일괄처리하기(Batch Geotagging of Images Using Exiftool)

exiftool[^exiftool] 을 사용하여 한 번에 여러 개의 이미지에  지오로케이션 정보를 추가할 수 있습니다.
첫 단계는 LibreOffice Calc[^calc]와 같은 소프트웨어를 사용하여  다음의 열이 있는 스프레드시트를 생성한다.

    SourceFile | GPSLatitude | GPSLongitude | GPSAltitude | GPSLatitudeRef | GPSLongitudeRef | GPSAltitudeRef

그리고 태그하고자 하는 각 이미지를 다음과 같이 새로운 열을 추가한다.

    image1.JPG | 46.8425212 | -91.9942096 | 198.609 | N | W | 0
    image2.JPG | 46.8424584 | -91.9938293 | 198.609 | N | W | 0
    [...]

완료 했다면 CSV 형식으로 스프레드시트를 내보내어 다음과 같이 타이프 한다.

    $ exiftool -GPSLatitude -GPSLongitude -GPSAltitude -GPSLatitudeRef -GPSLongitudeRef -GPSAltitudeRef -csv="myfile.csv" -o geotagged_images/ input_images/

**input\_images** 디렉터리의 이미지들은 지오 태깅되어 **geotagged\_images** 디렉터리로 저장될 것이다.

## 더 많은 읽을거리(Further Readings)

오픈드론맵(OpenDroneMap)을 사용하기 위해서 필요하지는 않지만 명령행을 사용하여 기술을 확장하려는 사용자는 
[https://ryanstutorials.net/linuxtutorial/](https://ryanstutorials.net/linuxtutorial).
에서 제공되는 *Bash 명령행 알아보기* 자습서를 읽어 보십시오. 파일 조작, 편집, 파이프 및 프로세스 관리를 포함하여 포괄적인 소개가 포함되어 있습니다. [^역주]


[^exiftool]: Exiftool: <https://www.sno.phy.queensu.ca/~phil/exiftool/>
[^calc]: LibreOffice: <https://www.libreoffice.org/>
[^역주]: 한국어사용자는 한국리눅스문서프로젝트의 *고급 Bash 스크립팅 가이드* 문서가 유용합니다. [https://wiki.kldp.org/HOWTO/html/Adv-Bash-Scr-HOWTO/](https://wiki.kldp.org/HOWTO/html/Adv-Bash-Scr-HOWTO/)