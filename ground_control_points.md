# 지상기준점 (Ground Control Points)
\label{ground_control_points}

지상기준점(GCP)은 일반적으로 고밀도 GPS[^gps] 를 사용하여, 지상에서 수행된 어떤 위치에 대한 측량값이다. 
길모퉁이와 같이 식별할 수 있는 구조물이나 지형 위에 식별할 수 있는 표지를 두어 측량을 수행한다.

![지상기준점 표지. Image courtesy of Michele M. Tobias &
Alex Mandel Creative Commons Attribution-ShareAlike 4.0 International CC
BY-SA 4.0\label{fig:ground_control_point_marker}](images/figures/ground_control_point_marker.jpg)

표지가 영상에 식별가능한 경우, 영상에 나타나는 표지의 위치와 실세계에서의 위치 사이에 상응점을 생성하여 *태그* 하면, 
영상 안의 표지는 실세계 좌표값을 가지게 된다.

![Image A를 생성하기 위해 카메라 A에 의해 촬영된 GCP 마커. 두 번째 단계에서 마커의 픽셀 위치(1500,1000) 
Image A에 실제 좌표(위도)를 수동으로 태그할 수 있다 (위도 40, 경도 -85).\label{fig:gcp_marker}](images/figures/gcp_marker.png)

고밀도 GPS를 사용하여 정적(움직이지 않는) 물체의 측량값이 움직이는 UAV로부터의 측량값보다 더 신뢰도가 높기 때문에, 
지상기준점을 사용하면 reconstruction 할 때 지오레퍼런싱 정확도를 높일 수 있다.

비행할 지역을 균등하게 나누어 고려하였을 때, 이상적인 지상기준점 수는 5~8개 사이다. 
8개 이상의 지점을 취득하였다 해서 반드시 정확도를 높이는 것은 아니다.

동일한 마커가 여러 영상에서 보이는 경우, 각 영상에 대하여 태그를 여러 번 해야 한다. 
이상적으로 각 마커는 최소한 세 번 태그되어야 한다. 다시 말해서, 최소한 세 영상에 대하여 각 마커에 태그해야 하는 것을 의미한다. 
이는 마커의 위치를 계산하기 위해서는 삼각측량이 수행되어야 하기 때문이다.

지상기준점은 추가적인 텍스트 파일을 같이 업로드해주어 입력 영상과 함께 이용할 수 있다. 
다음과 같은 단순한 포맷으로 업로드해줄 수 있다.

  - 첫 번째 줄은 세계좌표 SRS(공간 기준 시스템)를 나타낸다. 
    사용할 수 있는 세계 좌표 SRS 유형에는 제한이 없다. 
    내부적으로 좌표는 가장 가까운 WGS84 UTM[^utm] 으로 투영되어 바뀐다. SRS는 3가지 다른 형식을 사용하여 지정할 수 있다.

<!-- end list -->

    1) WGS84 UTM <zone number><hemisphere>
    2) EPSG:<code>
    3) <proj4>

Format #1은 WGS84 기준 타원체 및 datum을 사용하여 UTM 투영을 명시하는, 사람이 읽을 수 있는 형식이다. 
Format #2 EPSG codes[^codes] 를 이용한다. EPSG는 많은 공통 spatial reference system을 기준하는 표준이다. 
Format #3은 spatial reference system을 명시적으로 정의하는 Proj4 문자열[^strings] 을 사용한다. 
Format #1와 #2를 #3보다 더 권장한다.

다음은 모두 GCP 파일에 대한 SRS 정의의 유효한 예들이다.

    WGS84 UTM 16N
    WGS84 UTM 32S
    EPSG:4326
    EPSG:32616
    +proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs 
    +proj=utm +zone=16 +ellps=WGS84 +datum=WGS84 +units=m +no_defs

spatial reference system에 대한 정의는 다음 링크에 상세히 서술되어 있다 <https://www.spatialreference.org>.

  - 이어서 다음은 차례로 X, Y, Z 세계 좌표값, 관련된 픽셀 좌표값, 이미지 파일 이름(대소문자 구분)을 의미한다. 
    선택적으로, 추가 필드(예: 라벨)를 다음에 지정할 수 있다. 
    탭과 스페이스는 모두 분리하는데 이용할 수 있다.

<!-- end list -->

    <spatial reference system>
    <geo_x> <geo_y> <geo_z> <im_x> <im_y> <image_name> [<extras>]
    <geo_x> <geo_y> <geo_z> <im_x> <im_y> <image_name> [<extras>]
    ...

Figure~\ref{fig:gcp_marker}은 다음과 같이 나타난다.

    EPSG:4326
    -85 40 0 1500 1000 ImageA.jpg gcp1

Figure~\ref{fig:gcp_marker}이 고도 값을 가지고 있지 않기 때문에 **geo_z** 는 0으로 설정된다. .

## POSM GCPi 를 사용하는 GCP 파일 생성하기

모든 영상에 대하여 수동으로 픽셀 좌표를 찾고 측량하는 작업과, 마커 위치를 세계좌표랑 연결하는 작업은 
지치는 작업인 동시에 실수를 초래하는 경향이 있다.

POSM GCPi(Portable OpenStreetMap Ground Control Point Interface) 는 이 과정을 좀 더 쉽게 만들 수 있는 방법을 제공한다. 
WebODM에서 기본 플러그인으로 이미 로드되었으며, **GCP Interface** 패널을 통해 접근이 가능하다.

또는 독립적으로 홈페이지[^page] 지침에 따라 다운로드하여 실행할 수도 있다. 
다음의 링크에도 설명되어 있다 <https://webodm.net/gcpi>.

### Step 1. GCP 파일 (stub) 생성하기

마커의 위치를 측정한 후에, 라벨이 붙은 좌표 리스트를 CSV로 생성할 수 있다.
이 파일을 LibreOffice Calc[^cale] 와 같은 스프레드시트 응용프로그램에서 열면 다음과 같이 확인할 수 있다.

    X,Y,Z,Label
    -91.9943320967465,46.8423713026218,0,gcp1
    -91.9938849653384,46.8423668860772,0,gcp2
    -91.9942463047423,46.8425277454029,0,gcp3

여기서 *px*, *py* 필드에 대해 두 개의 새 열을 추가하면(0으로 초기화 된다). *Label* 을 우측 열로 옮기면, 다음과 같이 확인된다.

    X,Y,Z,px,py,Label
    -91.9943320967465,46.8423713026218,0,0,0,gcp1
    -91.9938849653384,46.8423668860772,0,0,0,gcp2
    -91.9942463047423,46.8425277454029,0,0,0,gcp3

마지막으로 첫 번째 줄을 삭제하고, SRS 유로 교체한 뒤 CSV 파일을 저장한다 (이 때, 대신 
*commas* 대신 *tabs* 또는 *spaces* 를 구분문자로 사용하도록 저장해야 한다). 
다음과 같이 함에 따라 파일을 저장할 수 있다. LibreOffice Calc에서 **파일 - 다른 이름으로 저장...** 을 클릭하고, 왼쪽 하단에서
저장 창 **필터 설정 편집**을 선택한다. 마지막 결과 파일 텍스트 파일로 생성되며, 내용은 다음과 같다. 

    EPSG:4326     
    -91.9943320967465 46.8423713026218 0 0 0 gcp1
    -91.9938849653384 46.8423668860772 0 0 0 gcp2
    -91.9942463047423 46.8425277454029 0 0 0 gcp3

### Step 2: GCP 파일 (stub) 불러오기

POSM GCPi에서 **Load existing Control Point File** 버튼을 클릭한다. 1단계에서 방금 생성한 파일을 선택한다.
**Load** 를 선택한 후 오른쪽에 있는 지도는 업로드된 파일에 포함된 점의 위치를 반영하도록 업데이트된다.

![POSM GCPi로 GCP파일 로드하기\label{fig:load_gcp_stub}](images/figures/load_gcp_stub.png)

### Step 3. Import the images and start tagging

이제 **Choose images** 을 눌러 이미지들을 불러온다. 이미지를 클릭하면 좌측 패널을 볼 수 있다. 
사용 적합성 문제 때문에, 인터페이스는 삭제할 필요가 있는 새로운 지상 기준점을 추가할 것이다. 
우측 패널에서 지상 기준점을 클릭하고 삭제할 수 있다. 이제 좌측 패널에서 마커 위치에서 타겟 아이콘을 움직이기 위해 
이미지 주변을 클릭하고 확대할 수 있다. 적절한 위치에 있게 되면, 단순히 우측 패널에서 타겟 아이콘을 한 번 클릭한다. 
타겟 아이콘이 우측 패널에서 초록색으로 바뀌면 마커가 대응되었다는 것을 의미한다.

![POSM GCPi를 이용하여 점들을 태깅한 영상\label{fig:tagging_images_using_gcpi}](images/figures/tagging_images_using_gcpi.png)

이제 모든 마커와 모든 이미지에 대해 이 과정을 반복할 수 있다. 중간에 중단하고, 작업을 저장하려면 
**Export File**을 누르고 결과를 원하는 위치에 저장한다. 영상과 출력된 파일을 다시 로드하여 다시 작업을 수행할 수 있다.

왼쪽 위 모서리를 보면 녹색 점이 보인다. 적어도 한 영상과 연결된 점들의 수를 보여준다 (최소한 하나의 이미지에 연결되어 있어야 한다).
최종적으로, 적어도 5개 혹은 8개 녹색 점이 있어야 한다.

### Step 4. GCP 파일 내보내기

작업이 끝나면, **Export File** 버튼을 클릭하여 GCP 파일을 내보낼 수 있다. 이 파일로 다음 단계로 작업을 진행할 수 있다.

## GCP 파일 사용하기

WebODM과 NodeODM에 GCP파일을 사용하는 경우, 파일 업로드동안에 이미지와 함께 포함하는 것만큼 간단하다. 
ODM을 사용하면, GCP 파일이 프로젝트 폴더에 배치되고 **gcp_list.txt** 로 명명된다. 
또는 **gcp** 옵션을 사용하여 GCP에 대한 경로를 지정할 수 있다. 
예를 들어, 이미지가 **D:\textbackslash odmbook\textbackslash project\textbackslash images** 에 저장되고 
GCP 파일이 **D:\textbackslash odmbook\textbackslash project\textbackslash gcp.txt** 에 저장되는 경우, 다음과 같은 커맨드 라인을 입력할 수 있다:

    $ docker run -ti --rm -v //d/odmbook/project:/datasets/code opendronemap/odm --project-path /datasets --gcp /datasets/code/gcp.txt

위의 커맨드라인으로 ODM을 이용하는 것은 *The Command Line* 장에서 상세하게 다뤄진다.

## GCP 파일 사용 이유와 한계

최종 결과가 설정한 지상기준점과 완벽하게 일치하지 않을 수 있다. 이 때, 왜 그런지 이해하는 것이 중요하다. 
GCP 관측(observation)은 처리 파이프라인의 SFM 단계에서 사용된다. 이 단계에서 카메라 위치는 여러가지 변수가 포함된 오류 최소화 문제에 기초하여 추정된다. 
GCP 관측은 지오레퍼런싱 alignment 오류를 최소화하는 문제에 이용되지만, 
과도한 카메라 렌즈 왜곡 (**카메라 캘리브레이션** 장 참조) 혹은 최적화 되지 못한 비행경로 때문에, 
잘못된 카메라 모델 추정과 같은 요인들을 보완하지 못할 수 있다. 
영상들이 완벽하게 align되지 않았다면, 먼저 GCP 파일의 오류가 있는지 확인한다. 
없는 경우에는, 결과를 개선하기 위해 *카메라 보정* 장의 개념을 읽고 적용하는 것이 좋다.


[^gps]: I use GPS as a synonym for GNSS, since most people recognize GPS as a word.
[^utm]: Universal Transverse Mercator: <https://en.wikipedia.org/wiki/Universal_Transverse_Mercator_coordinate_system>
[^codes]: EPSG: <http://www.epsg.org/>
[^strings]: Proj Quick Start: <https://proj.org/usage/quickstart.html>
[^page]: POSM GCPi: <https://github.com/posm/posm-gcpi>
[^calc]: LibreOffice: <https://www.libreoffice.org>
