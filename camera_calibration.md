# Camera Calibration
\label{camera_calibration}

정확한 reconstruction을 위해서는 사진을 찍는 데 사용되는 카메라의 내부 세부 사항을 알아야 한다. 
이는 *카메라 내부 표정요소(파라미터)* 라고 하며, 초점 길이, 카메라 중심 및 왜곡 파라미터와 같은 값을 의미한다. 
제조사에서 제공하는 자료를 확인하거나 데이터베이스로부터의 정보는 정확한 값을 얻기에 충분하지 않다. 
제조 공정의 결함, 진동 및 기타 요인은 동일한 카메라 모델들 사이에서도 카메라 내부요소들을 달라지게 할 수 있다.

어떤 종류의 카메라 보정도 수행하지 않고, 데이터셋을 처리해도 좋은 결과를 얻을 수도 있다. 
현재 대부분의 사진측량 소프트웨어(OpenDroneMap 포함)는 self-calibration 기능이 있다. 
입력 영상에서 직접 보정하는 일종의 자가 교정인 self-calibration은 OpenDroneMap에서 
다음과 같은 경우 매우 잘 작동하는 경향이 있다.

1.  영상이 다양한 고도로 찍혔을 때

2.  영상이 다양한 각으로 찍혔을 때(수직, 경사 영상 포함).

중복도가 큰 경우, 수직 영상이 권장되지는 않다[^recommended]. 
대부분의 미션 플랜 소프트웨어가 정확히 다음과 같은 타입의 패턴을 만들어 낼 것이기 때문이다.

![미션 계획 소프트웨어의 전형적인 비행 경로. Self-calibration으로 적합하지는 않다.
\label{fig:typical_flight_path}](images/figures/typical_flight_path.png)

이것은 절대로 이런 패턴으로 비행해서는 안 된다는 것을 의미하지는 않는다. 
다만, 이 패턴으로 비행할 때, reconstruction의 정확도가 저하될 수 있다는 것을 기억해야한다. 
넓은 영역에 걸쳐, 이 오류는 누적되며 일반적으로 *doming* 효과를 낳는다.

![doming 효과가 나타난 포인트 클라우드. 지형이 직선보다는 아치형으로 보이게 된다.
\label{fig:doming}](images/figures/doming.png)

doming은 항공 사진을 취득하는 동안 다른 고도에서 비행(척도 변화 최대화) 및 
다양한 각도에서 촬영된 영상들을 기반하면 가장 잘 개선될 수 있다.

완벽한 영상을 취득하는 것이 항상 가능하지는 않다. 데이터를 이미 취득한 뒤 다시 취득할 수 없는 경우도 있고, 
또는 면적이 너무 커서 여러 번 비행으로도 커버할 수 없는 경우도 있다.

이 문제를 해결하기 위해 다음의 옵션들이 있다.

## Option 1: 기존의 카메라 모델 사용하기

이것은 바람직하게 여겨지는 방법이며, 가장 개선될 가능성이 높다.
OpenDroneMap을 사용하면, 동일한 카메라로 촬영한 적합한 데이터 세트(중첩된 로트, 다양한 고도, 다양한 각도 등 포함)를 처리하고, 
다른 데이터 세트에 다시 자가 교정을 통해 얻은 카메라 파라미터를 재사용할 수 있다.

### WebODM 지시사항

적합한 데이터셋을 처리한 후, 단순히 **Downaload Assets - Camera Parameters**를 클릭하면 된다.

![WebODM 카메라 파라미터 다운로드](images/figures/webodm_download_cameras.png)

위 과정을 통해 **cameras.json** 으로 저장 된다. 
다시 새로운 task를 만들면, **cameras.json** 파일을 선택함으로써 **cameras** 옵션을 설정할 수 있다. 
처리 데이터셋은 미리 계산된 카메라 파라미터를 이용하게 된다.

![WebODM 카메라 설정](images/figures/webodm_set_cameras.png)

### ODM 지시사항

처리가 끝난 후, **cameras.json** 파일이 항상 프로젝트의 루트 디렉터리에 저장된다. 기존 프로젝트에서 
카메라 파라미터를 다시 사용하려면, 단순히 **--cameras** 를 통해 **cameras.json** 파일로 경로를 입력한다.

예를 들면, 만일 **cameras.json** 가 **D:\textbackslash odmbook\textbackslash projects\textbackslash test** 에 저장될 경우 다음과 같은 명령어를 입력할 수 있다.

    $ docker run -ti --rm -v //d/odmbook/projects/test:/datasets/code opendronemap/odm --project-path /datasets --cameras /datasets/code/cameras.json

## Option 2: 캘리브레이션 타겟으로부터 카메라 모델 생성하기

캘리브레이션 타겟을 통해서 좋은 카메라 파라미터를 얻을 수 있다. 
하지만 타겟으로 좋은 결과를 기대하는 것보다 자가 교정 방법이 더 좋은 결과를 줄 수 있다. 
하지만, 데이터 셋이 적합하게 취득될 수 없는 경우나 자가 교정 접근방식이 양호한 결과를 생성하지 못하는 경우에는 
오히려 유용한 방법이 될 수 있다.

이 과정은 다음의 3가지 단계를 가지고 있다.

1.  캘리브레이션용 타겟의 사진을 촬영한다(보통 다른 각도로 촬영된 체스보드를 입력한다).

2.  사진으로부터 카메라 프로필을 추출한다.

3.  직접 **cameras.json** 파일을 작성한다.

## 캘리브레이션 타겟 사진 촬영하기

캘리브레이션 타겟은 단순히 인식할 수 있는 특징을 가진 객체다. 
흑백 대비가 되어 차이가 인식되기 쉽기 때문에 체스판 패턴이 자주 사용된다.

![체스보드 패턴\label{fig:chessboard_pattern}](images/figures/chessboard_pattern.png)

캘리브레이션 타겟은 소매상에서 구할 수 있다. 큰 표면에 이 패턴을 투영해야 한다. 
TV나 큰 모니터 같은 것도 사용하기에 적합하다. 이러한 패턴들은 calib.io[^io] 와 같은 웹사이트에서 얻을 수 있다.

![TV에 Chromecast를 사용하여 체스보드 캘리브레이션 타겟이 보여진 모습이다.\label{fig:calibration_target_tv}](images/figures/calibration_target_tv.png)

11x9 체스 보드 패턴을 만든 다음 모니터에 띄운다. 타겟이 제자리에 있으면 데이터 세트를 촬영하는 데 사용한 것과 
동일한 카메라를 사용하여 다른 각도에서 5~9장의 대상 사진을 찍는다. 
이 때, 카메라 포커스를 고정되어야 한다(auto focus 모드가 비활성화되어야 함).

## 카메라 프로필 추출하기

캘리브레이션 타겟을 촬영한 사진에서 카메라 프로필을 추출하는 프로그램이 많이 있다. 
여기서는 간단히 Adobe™ Lens Profile Creator를 사용한다. Adobe site[^site] 에서 Windows와 Mac 버전을 무료로 다운로드할 수 있다.

다운로드하여 설치한 뒤, 프로그램을 열고 **File - Add Images to Project (CTRL+N)** 을 클릭한다. 
원하는 영상을 선택한 뒤 **Open** 버튼을 클릭한다.

![렌즈 프로필 생성기\label{fig:lens_profile_creator}](images/figures/lens_profile_creator.png)

이제 영상이 로드되고, 화면의 우측을 확인한다.

1.  **Lens Name** 을 입력한다.

2.  알맞은 렌즈 타입을 선택한다. **Rectilinear** 렌즈는 벽지와 같은 직선 방향으로 찍힌 객체가 직선으로 나타나게 한다. 
    **Fisheye** 렌즈는 직선 방향으로 찍힌 객체가 곡선으로 보이게 나타나게 한다 (아래 예에서 확인).
    
3.  **Model** 섹션에서 **Geometric Distortion Model** 만 확인한다. 여기서 색 수차(chromatic aberration)와 비그네팅(vignetting)현상은 다루지 않는다.

4.  체크보드 타겟에 있는 행과 열의 체크 갯수를 **Checkerboard Info** 에서 입력한다.

5.  패턴의 대략적인 크기와 픽셀 하나의 실제 크기를 **Print dimension** 과 **screen dimension** 에 입력한다 (디폴트로 둘 수도 있음).

6.  **Advanced** 탭에서는, 간소화한 *perspective* 나 *fisheye* 카메라 모델을 생성하려면 
    **Rectilinear Lens Model** 과 **Fisheye Lens Model** 에서 
    **Two-Parameter Radial Distortion** 를 선택한다. 더 복잡한 *brown* 카메라 모델을 생성하려면, **Five-Parameter Radial Distortion** 을 선택한다.

![Fisheye 렌즈 (상단 이미지) vs. Rectilinear 렌즈 (하단 이미지). Image courtesy of Ashley
Pomeroy, An example of Panorama Tools, CC BY-SA 3.0\label{fig:fisheye_vs_rectilinear}](images/figures/fisheye_vs_rectilinear.jpg)

오른쪽 위에서 **Generate Profiles** 버튼을 누른다. 캘리브레이션이 끝나면, **Save a Profile** **(.lcp)** 파일을 저장하라는 메시지가 표시된다. 
폴더를 선택하고 다음 단계에서 이어가기 때문에 저장 위치를 기억한다.

오류가 발생하면 화면의 권장 사항을 따른다. 오류는 일반적으로 캘리브레이션 타겟 사진을 더 잘 촬영하거나 
**Checkerboard Info** 섹션의 값을 조정하여 해결할 수 있다. 
또한, Lens Profile Creator 사용 설명서[^guide] 를 확인하여 문제를 더 해결 할 수 있다.


## 직접 cameras.json 파일 작성하기

원하는 텍스트 편집기를 사용하여 기존 ** cameras.json** 파일과 방금 생성한 **.lcp** 파일을 모두 연다. 
**.lcp** 파일에서 캘리브레이션 값을 가져와 ** cameras.json* 파일에 적용하면 된다. 
**.lcp** 파일에서 다음의 **rdf:parseType="resource"** 항목을 찾는다.

    <stCamera:PerspectiveModel rdf:parseType="Resource">
        <stCamera:Version>2</stCamera:Version>
        <stCamera:FocalLengthX>0.581968</stCamera:FocalLengthX>
        <stCamera:FocalLengthY>0.581968</stCamera:FocalLengthY>
        <stCamera:ImageXCenter>0.500000</stCamera:ImageXCenter>
        <stCamera:ImageYCenter>0.500000</stCamera:ImageYCenter>
        <stCamera:RadialDistortParam1>0.017524</stCamera:RadialDistortParam1>
        <stCamera:RadialDistortParam2>-0.074705</stCamera:RadialDistortParam2>
        <stCamera:ResidualMeanError>0.000134</stCamera:ResidualMeanError>
        <stCamera:ResidualStandardDeviation>0.000227</stCamera:ResidualStandardDeviation>
    </stCamera:PerspectiveModel>

**cameras.json** 파일이 다음과 같은 형식으로 입력되어야 한다.

    {
        "v2 dji fc300s 4000 2250 perspective 0.5555": {
            "focal_prior": 0.5555555, 
            "width": 4000, 
            "k1": 0.0, 
            "k2": 0.0, 
            "k1_prior": 0.0, 
            "k2_prior": 0.0, 
            "projection_type": "perspective", 
            "focal": 0.555555, 
            "height": 2250
        }
    }
    
    (예시 파일이므로 값은 다를 수 있음.)

**.lcp** 파일에서 초점 거리와 왜곡 계수를 이용함으로써 다음과 같이 **cameras.json** 파일을 다시 쓸 수 있다.

    {
        "v2 dji fc300s 4000 2250 perspective 0.5555": {
            "focal_prior": 0.581968, 
            "width": 4000, 
            "k1": 0.017524, 
            "k2": -0.074705, 
            "k1_prior": 0.017524, 
            "k2_prior": -0.074705, 
            "projection_type": "perspective", 
            "focal": 0.581968,
            "height": 2250
        }
    }

값 이름은 다음과 같이 변한다.

    .lcp —> cameras.json
    
    FocalLengthX,FocalLengthY —> focal,focal_x,focal_y
    RadialDistortParam1 —> k1
    RadialDistortParam2 —> k2
    RadialDistortParam3 —> k3
    TangentialDistortParam1 —> p1
    TangentialDistortParam1 —> p2
    ImageXCenter,ImageYCenter —> c_x,c_y

카메라의 종류와 계산된 렌즈 모델에 따라 일부 값이 존재하지 않을 수 있다
(예를 들어 단순한 **perspective** 카메라 모델에는 접선 파라미터가 없지만 **brown** 모델에는 있다).

**projection\_type** 필드는 항상 **perspective**, **brown**(Duane C. Brown과 Alexander E. Conrady의 이름을 딴 보다 복잡한 원근법 모델)
중 하나이다. **prior** 필드는 우선순위가 아닌 상대 필드와 동일한 값으로 채워야 한다.

일단 변경하면, 단순히 새 **cameras.json** 파일을 저장하고, 이제 사용할 수 있다.

## Bonus: 기하학적 왜곡을 수동으로 제거하여 LCP 파일 확인하기

데이터 세트를 처리하기 전에 LCP 파일이 올바르게 계산되었는지 확인하는 한 가지 방법은 
이미지에서 렌즈 왜곡을 제거하는 데 계산 값을 사용하고 왜곡이 사라졌는지 확인하는 것이다.

There are several programs that can read **.lcp** camera profiles. We’ll
use Rawtherapee, because it’s a stable, free and open source program
available on all major platforms. Installers can be downloaded directly
from Rawtherapee’s website[^website]. Once installed, open it. Rawtherapee
has lots of features and the user interface might look a bit
intimidating at first. Use the reference image below to help navigate
the next steps.
**.lcp** 카메라 프로파일을 읽을 수 있는 프로그램이 여러 개 있다. 
여기서는 모든 주요 플랫폼에서 사용할 수 있는 안정적이고 무료 오픈 소스 프로그램인 Rawtherape를 사용한다. 
Rawtherape의 웹사이트에서 직접 다운로드 받을 수 있다. 
Rawtherape는 많은 기능을 가지고 있고 사용자 인터페이스는 처음에는 복잡하게 보일 수도 있다. 
다음 아래 이미지는 다음 단계들을 보여주고 있다.

![Rawtherapee의 참조 영상\label{fig:rawtherapee_reference}](images/figures/rawtherapee_reference.png)

1.  **File Browser** 탭을 열고 영상이 저장되어 있는 드라이브로 이동한다.

2.  영상이 저장되어 있는 폴더로 이동한다.    

3.  **CTRL+A** 키 (우측 버튼을 누르고 모두 선택을 누름)와 함께 모든 영상을 선택한다.

4.  **Batch Edit** 탭을 열어, 좌측 상단의 세번 째 아이콘을 다음의 순으로 클릭한다 **Lens / Geometry**.

5.  **Profiled Lens Correction** 을 연다.

6.  폴더 아이콘을 눌러 이전에 내보낸 .lcp 카메라 프로파일을 선택한다. 이 때, **Distortion Correction**이 꼭 체크되어 있도록 한다.

**Distortion Correction** 체크박스를 켜면, 중앙 패널 (3)에서 영상이 바뀌는 것이 보여야 한다. 
영상이 바뀌는게 보이지 않는다면 위의 단계를 순서대로 수행했는지 세 번 정도 다시 확인한다.
이미지가 왜곡되지 않으면 변경 사항이 보이지 않을 수 있다. 
이미지를 다시 내보낼 준비가 되면 중앙 패널(3)을 **마우스 오른쪽 버튼 클릭** 하고 **put to queue** 를 누르고 **Queue** 탭(1)을 연다.

Queue 탭을 열고 다음의 순서로 연결 수행한다.

![Rawtherappe Queue 탭](images/figures/rawtherapee_queue.png)

1.  JPEG quality는 **100** 으로 설정하고 **Best Quality** 로 subsampling 하도록 한다 
    (이 때, 영상 quality 는 100외에 합리적인 값으로 결정할 수 있다.).

2.  **Off** 버튼을 클릭하여 영상 처리를 시작한다.

기하적으로 왜곡되지 않은 영상들은 입력 영상 디렉토리 안에 **converted** 폴더에 저장된다.

![원본 영상 (상단) vs. 왜곡되지 않은 영상 (하단)\label{fig:original_vs_undistorted}](images/figures/original_vs_undistorted.jpg)

**Congratulations!** 위의 절차에 따라 카메라 파라미터를 생성하고 수동으로 중단하지 않는 이미지 처리와 
기존 카메라 파라미터를 사용하는 프로세스에 익숙해져야 한다. 
나중에 사용할 수 있도록 생성한 카메라 파일을 폴더에 보관해야 한다. 
진동과 환경이 카메라 매개변수를 변화시킬 수 있다는 것을 기억해야 한다. 
그래서 가끔 새롭게 파라미터를 결정하는 것이 좋다.

카메라 캘리브레이션이 더 나은 reconstruction을 얻기 위한 완벽한 방법은 아니다. 
좋은 데이터 취득을 대신할 수는 없다. 그러나 *doming* 현상이 나타날 때 여러 시나리오에서 확실히 도움이 될 수 있는 단계다.

[^recommended]: Camera Calibration Considerations for UAV Photogrammetry: <https://www.isprs.org/tc2-symposium2018/images/ISPRS-Invited-Fraser.pdf>
[^io]: Calib.io calibration pattern generator: <https://calib.io/pages/camera-calibration-pattern-generator>
[^site]: Adobe™ Lens Profile Creator download page: <https://helpx.adobe.com/photoshop/digital-negative.html#resources>
[^guide]: Adobe™ Lens Profile Creator User Guide: <https://wwwimages2.adobe.com/content/dam/acom/en/products/photoshop/pdfs/lensprofile_creator_userguide.pdf>
[^website]: Rawtherapee’s download page: <https://rawtherapee.com/downloads>
