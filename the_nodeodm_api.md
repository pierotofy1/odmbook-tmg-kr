# NodeODM API (The NodeODM API)
\label{the_nodeodm_api}

ODM은 데이터를 처리하는 엔진이고 WebODM은 ODM을 사용하기 위한 인터페이스이다.
NodeODM[^nodeodm]은 네트워크를 통해 WebODM이 ODM과 소통하기 위해 만들어졌다.
지금은 NodeODM은 그 역할을 확장하여 오픈드론맵 프로젝트의 전체 제품군과 연결하는 매개체가 되었다. 각각의 프로젝트는
NodeAPI에서 정의한 API를 이해하고 있다. 우리가 *NodeODM*을 말할 때는 그것은 <https://github.com/OpenDroneMap/NodeODM>에서 사용 가능한 NodeODM API의 레퍼런스 구현을 지칭하는 것이다.

NodeAPI의 핵심은 쉽게 새로운 테스크를 만들고, 취소, 삭제, 재시작과 같은 테스크를 관리하고, 정보의 상태를 관리하고, 처리결과를 다운로드하는 방법을 정의한 것이다.

![OpenDroneMap projects use the NodeODM API to communicate with each
other\label{fig:nodeodm_ecosystem}](images/figures/nodeodm_ecosystem.png)

흥미로운 점은 NodeAPI는 OpenDroneMap 생태계 외부 프로젝트에서도 채택되었다는 것이다.
예를 들어 NodeMICMAC 프로젝트는 [^project] ODM 대신 MicMac 사진측량 엔진을 이용해서 항공영상처리를 성공적으로 수행하였다.
이와 같은 방법은 기존에 이미 만들어진 다른 툴을 OpenDroneMap 생태계 내에서 재사용할 수 있도록 해주는 효과가 있다.

이번장에서는 NodeODM 인스턴스를 수동으로 실행하는 방법을 배울 것이다. 
그리고 웹 인터페이스를 살펴보고 약간의 cURL을 이용해서 수동적인 상호작용을 해볼 것이다. 
정통 NodeODM API의 사용은 사용자가 프로젝트들 사이에 일어나는 네트워크의 상호작용에 대해 보다 쉽게 이해할 수 있도록 해준다.  

API는 다른 버전들이 있고, 새로온 버전이 출시될 때마다 이전 버전과 구버전은 호환되게 하려고 노력했다.
대부분의 최신 사양은 온라인 [^online] 에서 이용할 수 있다. 이 글을 쓸 당시의 최신 사양에 대해서는 이책의 마지막 장에 참고문헌으로 기술되어 있다.

## NodeODM 인스턴스 실행 (Launching a NodeODM Instance)

아래와 같이 간단히게 NodeODM을 실행할 수 있다:

    $ docker run -d -p 3000:3000 opendronemap/nodeodm

웹브라우저에서 <http://localhost:3000>를 열면 NodeODM의 인터페이스를 볼 수 있다. 인터페이스는 WebODM의 기능성과 대치되지 않도록 최소화하였다.

![NodeODM’s web interface\label{fig:nodeodm_web_interface}](images/figures/nodeodm_web_interface.png)

웹 인터페이스에서 이미지를 로드하고, 테스크에 대한 옵션 설정, 모니터링, 관리가 가능하고, 콘솔을 통해 결과를 알수 있고 다운로드 받을 수 있다.
## NodeODM 환경설정 (NodeODM Configuration)

You can pass several options while launching NodeODM. The full list is
available by running:
NodeODM이 실행되는 동안 다양한 옵션들이 지나갈 것이다. 전체 리스트는 아래와 같은 명령으로 확인할 수 있다.

    $ docker run --rm -ti opendronemap/nodeodm --help
    
    Usage: node index.js [options]
    
    Options:
            --config <path> 설정파일의 경로 (default: config-default.json)
            -p, --port <number>     서버 바인딩 포트 (default: 3000)
            --odm_path <path>       OpenDroneMap의 코드 경로 (default: /code)
            --log_level <logLevel>  로그 수준 셋팅(default: info)
            -d, --deamonize         데몬으로 실행하기 위한 프로세스 셋팅
            -q, --parallel_queue_processing <number> 동시에 수행할 수 있는 프로세스의 숫자(default: 2)
            --cleanup_tasks_after <number> 테스크를 지우거나, 취소하기 전 대기하는 시간(분) (default: 2880)
            --cleanup_uploads_after <number> 끝나지 않은 업르도를 지우기전에 대기하는 시간, 이 값은 최대 업로드 시간에 맞춰서 설정할 것(default: 2880)
            --test Enable test mode. 테스트모드에서, OpendDroneMap에 명령을 보내지 않는다. 개발이나 테스트하는 하는 동안 유요하게 사용하는 모드이다. (default: false)
            --test_skip_orthophotos 이 모드가 활성화되면, 정사영상 만드는 공정을 건너 뛴다. (default: false)
            --test_skip_dems        이 모드가 활성화되면, DEM을 만들지 않는다. (default: false)
            --test_drop_uploads     이 모드가 활성화되면, 이 폴더 /task/new/upload 를 드롭한다. 50% 확율로(default: false)
            --test_fail_tasks       이 모드가 활성화되면, 테스크가 실패한 것으로 기록한다. (default: false)
            --test_seconds  이 모드가 활성화되면, 테스트 테스크를 끝내기전에 수초 동안 쉬게 한다. (default: 0)
            --powercycle    전원이 공급되면 즉시 어플리케이션이 빠져나간다.실행과 편집 테스트에 유용하다.
            --token <token> 모든 요청을 지나쳐야하는 토큰을 설정할 때 사용. 토큰 홀더들만 노드에 접근하도록 접근을 제한하는데 사용한다. (default: none)
            --max_images <number>   노드가 철히ㅏㄹ 수 있는 최대의 이미지를 명시한다.(default: unlimited)
            --webhook <url> 테스크를 완료했을 때 호출할 수 있는 POST URL의 endpoint를 명시한다. (default: none)
            --s3_endpoint <url>     완료된 테스크의 결과를 업로드하기 위한 S3의 endpoint를 명시한다.(예를 들면 nyc3.digitaloceanspaces.com)(default: do not upload to S3)
            --s3_bucket <bucket>    완료된 테스크의 결과를 업로드하기 위한 s3의 bucket 명칭을 명시한다.(default: none)
            --s3_access_key <key>   s3의 endpoint가 설정 되었을 때 필요한 S3의 접근키 (default: none)
            --s3_secret_key <secret>        s3의 endpoint가 설정 되었을 때 필요한 S3의 비밀키 secret key, required if --s3_endpoint is set. (default: none)
            --s3_signature_version <version>        S3의 시그니처 버전. (default: 4)
            --s3_upload_everything  모든 결과를 s3에 업로드. (default: .zip 파일과 정사영상만 업로드하기)
            --max_concurrency   <number>   각 테스크에서 사용할 수 있는 최대 동시성 옵션에 표시를 한다. (default: no limit)
    Log Levels:
    error | debug | info | verbose | debug | silly

가장 중요한 요소는 아래와 같다.

  - **-q** 병렬로 처리할 수 있는 테스크의 양.

  - **--max\_images** 노드가 소화할 수 있는 최대 이미지수를 제한.

  - **--token** 권한있는 사용자에게 접근키를 부여함. NodeODM을 불특정다수에게 공개할 경우, 접근 권한을 제한할 때 유용하게 사용된다.

  - **--cleanup\_tasks\_after** 처리된 데이터를 디스크에서  자동으로 지우는 시간을 늘이거나 줄이는 방법, 디스크 공간이 없을 때 사용

## cURL으로 API 사용하기 (Using the API with cURL)


아래 예제에서 직접 API와 상호작용하기 위해서 curl program을 이용할 것이다. 이미지를 업로드하고, 테스크 상태를 확인하고, 결과를 다운로드 받을 것이다. Curl은 아래에서 다운 받을 수 있다.
<https://curl.haxx.se/download.html>.

문제해결과 한번 경험해 본다는 것 외에 NodeODM과 상호작용을 하기 위해 cURL을 사용해야할 필요는 없다. 
만약 커멘드라인에서 NodeODM과 상호작용하기를 원한다면 CloudODM[^cloudodm]을 사용하는 편이 더 편리할 것이다.
하지만 배운다는 의미에서 cURL을 사용할 것이다.

### 새 작업 생성 (Create a New Task)

다음 폴더에 이미지가 있다면 아래과 같이 수행 **D:\textbackslash odmbook\textbackslash project\textbackslash images** :

    $ pwd 
    /d/odmbook/project
    
    $ curl -F images=@images/DJI_0018.JPG -F images=@images/DJI_0019.JPG -F name=Test -X POST http://localhost:3000/task/new
    
    {"uuid":"c99f32a8-d3b2-48d2-adfb-6c8e14e405e3"}

더 많은 **-F images** 파라메타를 이용하면 더 많은 이미지를 사용할 수 있다. 

이 명령으로 *Test*라는 이름의 새로운 테스크를 생성했다. *name*는 FormData 파라메타를 통해 전달되었다.
몇몇 API 기능들은 쿼리에 의한 파라메타와 Body 파라메타를 받을 수 있다. 우리는 두가지 모두의 경우를 살펴볼 것이다.
API는 JSON(**J**ava**S**cript **O**bject **N**otation)이라는 포맷으로 통신을 한다.
JSON은 사람이 읽은 수 있는 아주 단순한 포맷이다. 예를 들어 에러가 발생하면 프로그램은 아래와 같이 출력할 것이다.:

 :
    {"error": "오류의 설명"}

**/task/new**의 성공적인 호출로 우리는 테스크 ID를 받았다.

### 작업 정보 쿼리 (Query Task Information)
테스크를 만들고나서, **/task/\<uuid\>/info**와 같이 ID를 이용해서 테스크의 상태를 호출할 수 있다.:

    $ curl http://localhost:3000/task/c99f32a8-d3b2-48d2-adfb-6c8e14e405e3/info
    
    {"uuid":"131ee2a5-9757-46e9-8491-81d2d4558680","name":"Test","dateCreated":1560702697082,"processingTime":17655,"status":{"code":30,"errorMessage":"Process exited with code 1"},"options":[],"imagesCount":2,"progress":100}

또한 아래와 같은 호출로 전체 테스크의 출력을 디스플레이할 수 있다.

    $ curl http://localhost:3000/task/c99f32a8-d3b2-48d2-adfb-6c8e14e405e3/output
    
    ["[INFO]    Initializing OpenDroneMap app - Sun Jun 16 16:28:16  2019","[DEBUG]   ==============","[DEBUG]   build_overviews: False","[DEBUG]   crop: 3","[DEBUG]   dem_decimation: 1","[DEBUG]   dem_euclidean_map: False","[DEBUG]   dem_gapfill_steps: 3","[DEBUG]   dem_resolution: 5","[DEBUG]   depthmap_resolution: 640", ...

혹은 **?line=** 옵션을 이용해서 마지막 2개의 문장을 불러올 수 있다:

    $ curl http://localhost:3000/task/c99f32a8-d3b2-48d2-adfb-6c8e14e405e3/output?line=-2
    
    ["if bbox is None: raise Exception(\"Cannot compute bounds for %s (bbox key missing)\" % input_point_cloud)","Exception: Cannot compute bounds for /var/www/data/c99f32a8-d3b2-48d2-adfb-6c8e14e405e3/odm_filterpoints/point_cloud.ply (bbox key missing)"]

쿼리 파라메타는 URLs를 통해 직접 전달된다.

지금까지 쿼리와 FormData 파라메타를 전달하는 예제를 살펴보았다. 다음에는 Body 파라메타를 전달하는 것을 살펴볼 것이다.

## 작업 제거 (Remove a Task)

테스크를 지우기 위해서 **/task/delete**를 호출할 수 있다:

    $ curl -d uuid=c99f32a8-d3b2-48d2-adfb-6c8e14e405e3 http://localhost:3000/task/remove
    
    {"success":true}

테스크 ID는 *uuid* Body 파라메타를 이용해 전달된다.

## API 사양서 (API Specification)

Version: 1.5.3

### POST /task/new/init

#### 설명 (Description):

새로운 테스크의 업로드를 초기화한다. 만약 성공적으로 초기화가 되면 사용자는 /task/new/upload를 통해 파일 업로드를 시작한다. 테스크는 /task/new/commit 호출될 때까지 시작되지 않는다.
#### 매개변수 (Parameters)

|   이름   |  위치  |    설명   |  필수  |   스키마   |
| ---- | ---------- | ----------- | -------- | ---- |
| name | formData | An optional name to be associated with the task | No | string |
| options | formData | Serialized JSON string of the options to use for processing, as an array of the format: [{name: option1, value: value1}, {name: option2, value: value2}, ...]. For example, [{"name":"cmvs-maxImages","value":"500"},{"name":"time","value":true}]. For a list of all options, call /options | No | string |
| skipPostProcessing | formData | When set, skips generation of map tiles, derivate assets, point cloud tiles. | No | boolean |
| webhook | formData | Optional URL to call when processing has ended (either successfully or unsuccessfully). | No | string |
| outputs | formData | An optional serialized JSON string of paths relative to the project directory that should be included in the all.zip result file, overriding the default behavior. | No | string |
| dateCreated | formData | An optional timestamp overriding the default creation date of the task. | No | integer |
| token | query | Token required for authentication (when authentication is required). | No | string |
| set-uuid | header | An optional UUID string that will be used as UUID for this task instead of generating a random one. | No | string |

#### 응답(Response)

| 코드 | 설명 | 스키마 |
| ---- | ----------- | ------ |
| 200 | Success | object |
| default | Error | [Error](#error) |

### POST /task/new/upload/{uuid}

#### 설명 (Description):

/task/new/init를 통해 만들어진 테스크에 파일을 추가한다. 이는 테스크 시작하게 하지는 않는다. 테스크를 시작하기 위해서는 /task/new/commit를 호출해야 한다.

#### 매개변수 (Parameters)

|   이름   |  위치  |    설명   |  필수  |   스키마   |
| ---- | ---------- | ----------- | -------- | ---- |
| uuid | path | 작업의 UUID | Yes | string |
| images | formData | Images to process, plus an optional GCP file (*.txt) and/or an optional seed file (seed.zip). If included, the GCP file should have .txt extension. If included, the seed archive pre-polulates the task directory with its contents. | Yes | file |
| token | query | Token required for authentication (when authentication is required). | No | string |

#### 응답(Response)

| 코드 | 설명 | 스키마 |
| ---- | ----------- | ------ |
| 200 | File Received | [Response](#response) |
| default | Error | [Error](#error) |

### POST /task/new/commit/{uuid}

#### 설명 (Description):

/task/new/upload를 통해 업로드된 이미지를 위해서 새로운 테스크를 만든다.

#### 매개변수 (Parameters)

|   이름   |  위치  |    설명   |  필수  |   스키마   |
| ---- | ---------- | ----------- | -------- | ---- |
| uuid | path | 작업의 UUID | Yes | string |
| token | query | Token required for authentication (when authentication is required). | No | string |

#### 응답(Response)

| 코드 | 설명 | 스키마 |
| ---- | ----------- | ------ |
| 200 | Success | object |
| default | Error | [Error](#error) |

### POST /task/new

#### 설명 (Description):

새로운 테스크를 만들고 그것을 프로세스 큐의 마지막에 위치시킨다. 아주 큰 테스크를 업로드하기 위해서는 /task/new/init를 사용한다. 

#### 매개변수 (Parameters)

|   이름   |  위치  |    설명   |  필수  |   스키마   |
| ---- | ---------- | ----------- | -------- | ---- |
| images | formData | Images to process, plus an optional GCP file (*.txt) and/or an optional seed file (seed.zip). If included, the GCP file should have .txt extension. If included, the seed archive pre-polulates the task directory with its contents. | No | file |
| zipurl | formData | URL of the zip file containing the images to process, plus an optional GCP file. If included, the GCP file should have .txt extension | No | string |
| name | formData | An optional name to be associated with the task | No | string |
| options | formData | Serialized JSON string of the options to use for processing, as an array of the format: [{name: option1, value: value1}, {name: option2, value: value2}, ...]. For example, [{"name":"cmvs-maxImages","value":"500"},{"name":"time","value":true}]. For a list of all options, call /options | No | string |
| skipPostProcessing | formData | When set, skips generation of map tiles, derivate assets, point cloud tiles. | No | boolean |
| webhook | formData | Optional URL to call when processing has ended (either successfully or unsuccessfully). | No | string |
| outputs | formData | An optional serialized JSON string of paths relative to the project directory that should be included in the all.zip result file, overriding the default behavior. | No | string |
| dateCreated | formData | An optional timestamp overriding the default creation date of the task. | No | integer |
| token | query | Token required for authentication (when authentication is required). | No | string |
| set-uuid | header | An optional UUID string that will be used as UUID for this task instead of generating a random one. | No | string |

#### 응답(Response)

| 코드 | 설명 | 스키마 |
| ---- | ----------- | ------ |
| 200 | Success | object |
| default | Error | [Error](#error) |

### GET /task/{uuid}/info

#### 설명 (Description):

이 테스크에 대한 이름, 만들어진 시간, 처리시간, 상태, 코맨드라인 옵션과 처리되고 있는 이미지의 수와 같은 정보를 얻는다. 전체 목록을 보기 위해서는 정의된 스키마를 참조하시오.

#### 매개변수 (Parameters)

|   이름   |  위치  |    설명   |  필수  |   스키마   |
| ---- | ---------- | ----------- | -------- | ---- |
| uuid | path | 작업의 UUID | Yes | string |
| token | query | Token required for authentication (when authentication is required). | No | string |
| with_output | query | Optionally retrieve the console output for this task. The parameter specifies the line number that the console output should be truncated from. For example, passing a value of 100 will retrieve the console output starting from line 100. By default no console output is added to the response. | No | integer |

#### 응답(Response)

| 코드 | 설명 | 스키마 |
| ---- | ----------- | ------ |
| 200 | Task Information | object |
| default | Error | [Error](#error) |

### GET /task/{uuid}/output

#### 설명 (Description):

OpenDroneMap의 출력 콘솔을 검색한다. 실행된 사항을 모니터링하는데 유용하고, 사용자에게 상태를 업데이트 해준다.

#### 매개변수 (Parameters)

|   이름   |  위치  |    설명   |  필수  |   스키마   |
| ---- | ---------- | ----------- | -------- | ---- |
| uuid | path | 작업의 UUID | Yes | string |
| line | query | Optional line number that the console output should be truncated from. For example, passing a value of 100 will retrieve the console output starting from line 100. Defaults to 0 (retrieve all console output). | No | integer |
| token | query | Token required for authentication (when authentication is required). | No | string |

#### 응답(Response)

| 코드 | 설명 | 스키마 |
| ---- | ----------- | ------ |
| 200 | Console Output | string |
| default | Error | [Error](#error) |

### GET /task/{uuid}/download/{asset}

#### 설명 (Description):

테스크를 통해 만들어진 결과물을 검색한다

#### 매개변수 (Parameters)

|   이름   |  위치  |    설명   |  필수  |   스키마   |
| ---- | ---------- | ----------- | -------- | ---- |
| uuid | path | 작업의 UUID | Yes | string |
| asset | path | Type of asset to download. Use "all.zip" for zip file containing all assets. | Yes | string |
| token | query | Token required for authentication (when authentication is required). | No | string |

#### 응답(Response)

| 코드 | 설명 | 스키마 |
| ---- | ----------- | ------ |
| 200 | Asset File | file |
| default | Error message | [Error](#error) |

### POST /task/cancel

#### 설명 (Description):

Cancels a task (stops its execution, or prevents it from being executed)
테스크를 취소한다(실행을 멈추거나 실행되는지 않도록 함).

#### 매개변수 (Parameters)

|   이름   |  위치  |    설명   |  필수  |   스키마   |
| ---- | ---------- | ----------- | -------- | ---- |
| uuid | body | 작업의 UUID | Yes | string |
| token | query | Token required for authentication (when authentication is required). | No | string |

#### 응답(Response)

| 코드 | 설명 | 스키마 |
| ---- | ----------- | ------ |
| 200 | Command Received | [Response](#response) |

### POST /task/remove

#### 설명 (Description):

테스크를 제거하고 모든 성과물을 지운다.

#### 매개변수 (Parameters)

|   이름   |  위치  |    설명   |  필수  |   스키마   |
| ---- | ---------- | ----------- | -------- | ---- |
| uuid | body | 작업의 UUID | Yes | string |
| token | query | Token required for authentication (when authentication is required). | No | string |

#### 응답(Response)

| 코드 | 설명 | 스키마 |
| ---- | ----------- | ------ |
| 200 | Command Received | [Response](#response) |

### POST /task/restart

#### 설명 (Description):

이전에 취소되거나 실패하거나 성공적으로 수행한 테스크를 재시작한다.

#### 매개변수 (Parameters)

|   이름   |  위치  |    설명   |  필수  |   스키마   |
| ---- | ---------- | ----------- | -------- | ---- |
| uuid | body | 작업의 UUID | Yes | string |
| options | body | Serialized JSON string of the options to use for processing, as an array of the format: [{name: option1, value: value1}, {name: option2, value: value2}, ...]. For example, [{"name":"cmvs-maxImages","value":"500"},{"name":"time","value":true}]. For a list of all options, call /options. Overrides the previous options set for this task. | No | string |
| token | query | Token required for authentication (when authentication is required). | No | string |

#### 응답(Response)

| 코드 | 설명 | 스키마 |
| ---- | ----------- | ------ |
| 200 | Command Received | [Response](#response) |

### GET /options

#### 설명 (Description):

테스크에 전달된 커멘드라인 옵션을 검색한다.

#### 매개변수 (Parameters)

|   이름   |  위치  |    설명   |  필수  |   스키마   |
| ---- | ---------- | ----------- | -------- | ---- |
| token | query | Token required for authentication (when authentication is required). | No | string |

#### 응답(Response)

| 코드 | 설명 | 스키마 |
| ---- | ----------- | ------ |
| 200 | Options | [ object ] |

### GET /info

#### 설명 (Description):

이 노드에 대한 정보를 검색한다.

#### 매개변수 (Parameters)

|   이름   |  위치  |    설명   |  필수  |   스키마   |
| ---- | ---------- | ----------- | -------- | ---- |
| token | query | Token required for authentication (when authentication is required). | No | string |

#### 응답(Response)

| 코드 | 설명 | 스키마 |
| ---- | ----------- | ------ |
| 200 | Info | object |

### GET /auth/info

#### 설명 (Description):

로그인정보를 검색한다.

#### 응답(Response)

| 코드 | 설명 | 스키마 |
| ---- | ----------- | ------ |
| 200 | LoginInformation | object |

### POST /auth/login

#### 설명 (Description):

사용자이름/비밀번호의 쌍으로 생성된 토큰을 검색한다.

#### 매개변수 (Parameters)

|   이름   |  위치  |    설명   |  필수  |   스키마   |
| ---- | ---------- | ----------- | -------- | ---- |
| username | body | Username | Yes | string |
| password | body | Password | Yes | string |

#### 응답(Response)

| 코드 | 설명 | 스키마 |
| ---- | ----------- | ------ |
| 200 | 로그인 성공 | object |
| default | 오류 | [Error](#error) |

### POST /auth/register

#### 설명 (Description):

새로운 사용자명/비밀번호를 등록한다.

#### 매개변수 (Parameters)

|   이름   |  위치  |    설명   |  필수  |   스키마   |
| ---- | ---------- | ----------- | -------- | ---- |
| username | body | 사용자명 | Yes | string |
| password | body | 비밀번호 | Yes | string |

#### 응답(Response)

| 코드 | 설명 | 스키마 |
| ---- | ----------- | ------ |
| 200 | Response | [Response](#response) |

### 모델 (Models)

### 오류 (Error)

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| error | string | 오류의 설명 | Yes |

### 응답(Response)

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| success | boolean | true if the command succeeded, false otherwise | Yes |
| error | string | Error message if an error occured | No |

## 연습(Exercises)

이제 여러분은 새로운 지식을 사용할 준비가 되었다. NodeODM에 대한 문서를 읽고 아래와 같은 테스크를 수행해보기 바란다:
	
  - API는 두가지 방법으로 새로운 테스크를 정의할 수 있다. 우리는 이미 **/task/new**를 사용했고,
    이것은 한번에 모든 이미지를 업로드하는 단순화된 방법이다.
    NodeODM은 또한 이미지를 병렬로 한꺼번에 업로드할 수 있는 API를 제공한다.
	**/task/new/init**, **/task/new/upload** and
    **/task/new/commit**를 이용해서 테스크를 만들 수 있나요?

  - JSON은 테스크의 옵션을 위해 사용된다. 
    예를 들어 **orthophoto-resolution** 처리 옵션을 설정하기 위해서 먼저 이것을 
	JSON으로 [{“name”:“orthophoto-resolution”,“value”:“2”}] 엔코딩을 한다.
    그리고 이것을 **/task/new** FormData 파라메타 *options* 에 전달한다.
    구글에서 JSON을 어떻게 사용하는지 검색할 수 있다.
	그리고 나서 **/task/new**에 복수의 옵션을 전달해보자

  - 인증토큰을 추가하기 위해서 **token** 파라메타로 NodeODM을 재시작해 보자. 
    그리고 URLS에서 쿼리 파라메타 **?token=**을 이용해서 API 기능을 호출해보자.
	URLs에 토큰을 전달하지 않은면 어떤 일이 발생하는지 알아보자.

문제가 발생한다면 오픈드론맵 포럼[^forum]에서 도움을 요청하시오.

[^nodeodm]: Node is a reference to Node.js, the language NodeODM is written in.
[^project]: NodeMICMAC: <https://github.com/dronemapper-io/NodeMICMAC/>
[^online]: NodeODM 사양서: <https://github.com/OpenDroneMap/NodeODM/blob/master/docs/index.adoc>
[^cloudodm]: CloudODM: <https://github.com/OpenDroneMap/CloudODM/>
[^forum]: 오픈드론맵 포럼: <https://community.opendronemap.org>
