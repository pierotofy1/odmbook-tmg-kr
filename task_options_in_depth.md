# 작업 옵션 심화 학습
\label{task_options_in_depth}

데이터를 처리하는 파이프라인에 포함된 몇 가지 옵션들이 있으며, 이중 몇 가지는 직접 조절 가능할 뿐 아니라 결과에 상당한 영향을 미칠만한 것들이다.  The software exposes a subset of these available knobs through various options.(?) 작업을 생성할 때,
사용자는 한 가지 또는 그 이상의 옵션들을 조작하여 파이프라인의 데이터 처리 방식에 변화를 줄 수 있다.

![작업 생성시 보이는 옵션들\label{fig:options_as_shown_in_webodm}](images/figures/options_as_shown_in_webodm.png)

위의 리스트가 많아보일 수도 있으나, 파이프라인을 구성하는 다양한 구성단계에 사용할 수 있는 모든 옵션의 일부에 불과하다. 숨겨진 처리 기능이나 옵션들이 오픈드론맵의 소스코드에 숨겨진 형태로 숨겨져 있다. 오픈드론맵은 결과 에 가시적인 영향을 미치는 몇 옵션들만을 노출시키거나, 
직접 조작이 필요한 몇 가지 옵션만을 노출시키고 있다. 훨씬 많은 옵션들이 노출되어 있지 않은 형태로 남아 있다는 점을 기억하자.

옵션을 튜닝하는 작업에는 수많은 시행착오가 필요하다. 가장 좋은 결과를 얻기 위한 절대적 가이드라인은 없다. 특정 데이터셋에 최적인 옵션이 다른 데이터셋에도 마찬가지일 것이라는 보장을 할 수 없기 때문이다. 매우 다양한 카메라의 종류 및 촬영 각도, 비행계획
등을 모두 고려했을때, 이와 관련한 철저한 가이드라인을 제공하는 것은 현실적으로 어렵다. 게다가 장비들 또한 급속히 변화하고 있기 때문에 가이드라인을 작성하는 그 시점에서도 해당 가이드라인이 유효할 것이라고 장담할 수 없다. 그렇다고 망설이거나 두려워할 필요도 없다.

이번 챕터에서는 각 옵션들이 어떠한 기능을 갖고 있는지 설명할 것이다. 해당 챕터의 마지막 즈음에서는 독자분들의 결과를 향상시킬 뿐 아니라 특정 결과가 도출된 이유를 설명할 수 있을 것이고, 또한 어떤 옵션을 조작해야 독자분들이 원하는 결과를 도출할 수 있을지
알게 될 것이다.

몇 가지 옵션은 그래픽 인터페이스에서 누락되었을 수 있으며, 명령창으로만 실행가능하 것들도 있다. 이는 인터페이스로 제공하기에 적절하지 않거나 단순히 지원되지 않는 경우일수도 있기 때문이다.

이번 챕터를 건너 뛰거나 참고용으로만 사용해도 무리는 없다. 오픈드론맵 버전이 향상됨에 따라, 특정 옵션을 사라지거나 추가될 수도 있다. 아래의 옵션 목차는 2019년 6월 26일 버전이다. 현재 오픈드론맵을 지속적으로 개발하기 위하여 노력중이며, 그에 따라 본 책의 개정판도 계획하고
있다. 옵션에 대한 설명은 알파벳 순서로 나열되어 있다.

## build-overviews

해당 옵션을 설정하면, 정사영상에 오버뷰 기능이 추가될 뿐, DEM 같은 다른 2차원 생성물에 영향을 미치는 옵션은 아니다. (Overviews are an optimization available for GeoTIFFs that reduces the time it takes to open them, for the tradeoff of a larger filesize and some computational time.) \
오버뷰 기능은 정사영상 자체에 내장된 축소 사본이라고 생각하면 된다. 오버뷰는 QGIS[^qgis]처럼 해당 파일을 지원하는GIS 프로그램에서 정사영상을 열어보고자 할 때 유용하다. 오버뷰 기능을 사용할 수 있다면, 해당 뷰어프로그램은 전체 파일을 불러오는 것이 아니라 현재 확대 단계에 가장 적절한 
수준으로 오버뷰를 로드한다. 400MB 정도되는 정사영상을 GIS상에 불러오는데 시간이 많이 소요된다면, 아마도 오버뷰 기능이 없기 때문일 것이다. 오픈드론맵은 원래 영상의 1/2, 1/4, 1/8, 1/16 수준의 해상도 를 갖는 오버뷰 영상을 지원한다. 이때 평균 보간법을 사용한다. 정사영상이 1000x1000 해상도라면,
**build-overviews**는 500, 250, 125, 62 픽셀 해상도를 갖는 정사영상을 사본으로 저장한다.

## cameras

기본적으로 SFM 단계에서, 입력된 영상을 기준으로 카메라 변수를 추출한다. 해당 옵션을 사용하면 **cameras.json** 파일의 경로 또는 **cameras.json** 파일 자체를 이용하여 사전에 계산된 카메라 변수를 선택할 수도 있다.  **cameras.json** 파일은 데이터 
처리시에 항상 계산되므로, 특정 데이터 셋에서 계산된 카메라 변수를 다른 데이터 셋에 활용할 수도 있다. **cameras.json** 파일은 특정 캘리브레이션을 위한 물체(Calibration target)나 캘리브레이션 소프트웨어를 이용하여 생성할 수 있다. 사전에 미리 계산된
카메라 내부변수를 지정해주면 생성된 결과물의 정확도를 높일 수 있으며, 특히 돔효과(Doming effects, 평탄한 구간에서 포인트 클라우드가 완만한 곡면을 이루는 현상)를 방지하는데도 유용하다. 위와 관련한 내용은 *Camera Calibration* 챕터에서 더욱 자세히 다루도록 하겠다.

## crop

일반적으로 정사영상의 가장자리는 고르지 않다. 이는 텍스쳐 프로그램이 데이터가 없거나 부족한 부분을 인위적으로 채우려고 하기 때문에 발생하는 결과다. 해당 옵션은 이러한 부분을 제거하여 좀더 부드럽고 깔끔한 형태의 정사영상을 제공한다. 먼저
정사영상의 가장자리 점군(Point cloud)를 바탕으로 추정한다. 해당 포인트 클라우드 중 아웃라이어로 판단된 부분을 제거하며, 이후 해당 결과를 포함하는 폴리곤이 *odm\_georeferencing/odm\_georeferenced\_model.bounds.gpkg*에 저장된다. 해당 폴리곤은 **crop** 옵션에
의해 가장자리가 기존 보다 축소된 상태다. 이후 최종 폴리곤이 정사영상을 편집하기 위해 사용된다.

![포인트 클라우드(좌)와 Crop 영역에 의해 축소된 가장자리
(우)\label{fig:point_cloud_and_shrinked_bounds}](images/figures/point_cloud_and_shrinked_bounds.png)

가장자리의 위치를 추정하는 테크닉이 완벽하지는 않다. 때로는 정사영상의 가장자리 부분 중 양호한 부분이 해당 옵션으로 인하여 제거될 수도 있다. 이 옵션은 0으로 설정하여 생략할 있으며, 큰 데이터셋의 경우 해당 기능을 생략하는 것이 소요시간을 줄이는데 상당한
도움을 줄 수 있다.

## debug

디버그 메세지를 콘솔에 출력할 수도 있다. 개발 목적이라면 해당 기능이 유용할 것이다.

## dem-decimation

All DEMs are computed from the point cloud output. The larger the point
cloud, the longer it takes to compute a DEM. To speed things up, at the
trade-off of possible accuracy loss, this option reduces (decimates) the
number of points used to compute DEMs. The value specifies how many
points to “skip” during the decimation. Let’s look at an example by
setting this option to 3. By taking all the points in the point cloud
and placing them in a straight line, the program will reduce the number
of points by keeping one every three.
일반적으로 DEM은 점군를 이용하여 생성하는데, 점군의 크기가 클수록 DEM을 생성하는데 소요되는 시간도 늘어나게 된다. 이는 포인트 클라우드의 수가 많으면 DEM 생성을 위해 사용하는 연산량이 증가하기 때문이다. 따라서 DEM 생성 속도를 개선 하기 위해서는
포인트 클라우드의 수를 줄여야 하며, 이는 해당 옵션을 통해 가능하다. 해당 옵션의 값은 during the decimation에 고려하지 않을 포인트 클라우드의 수를 의미한다. 예시로 해당 옵션을 3으로 설정해보자. 이 경우 모든 포인트 클라우드를 일렬로 나열한 후, 
3번째에 위치한 포인트 클라우드만 선택하는 방식으로 DEM 생성시 필요한 연산에 포함되는 점군의 수를 줄인다.

![r검정색 점만 포함되며, 회색점은 DEM 생성시 고려하지 않는다.\label{fig:dem_decimation}](images/figures/dem_decimation.png)

이 방법으로는 약 \~33%의 포인트만이 포함되며, 나머지 약 \~66는 연산에서 제외된다. 옵션의 값을 1로 설정하면 모든 포인트가 포함될 것이며, 50으로 설정하면 약 \~2%의 포인트만이 유지되고, \~98%는 제외된다. 해당 비율은 다음과 같이 계산할 수 있다.

    (1 / decimation) * 100

다만, 위의 방식에서도 짐작할 수 있듯이 이 옵션은 포인트 클라우드의 위치 데이터를 고려하지 않는다는 점에 유의해야 한다. 또한, 해당 옵션은 DEM 연산시에 일정 비율의 포인트 클라우드를 고려하지 않을 뿐 실제 포인트 클라우드 객체에 영향을 주는 것은 아니다.

## dem-euclidean-map -> 이부분 이해가 잘 안됨

An euclidean map is a georeferenced image derived from DEMs (before any
holes are filled) where each pixel represents the geometric distance of
each pixel to the nearest *void*, *null* or *NODATA* pixel. It’s an
indicator (map) of how far a value in the DEM is to an area where there
are no values. This can be useful in cases when a person wishes to know
which areas of a DEM were derived from actual point cloud values and
which ones were filled with interpolation. Looking at the euclidean map,
every pixel that has a value of zero indicates that the corresponding
location on the DEM was filled with interpolation (because the distance
of a *NODATA* pixel to itself is zero)
유클리디언 맵은 데이터가 없는 각 픽셀 간 기하학적 거리를 표현한 DEM으로부터 추출한
지오레퍼런스 된 이미지다.(?) 

![DEM before hole filling (left) and corresponding euclidean map (right)\label{fig:dem_euclidean_map}](images/figures/dem_euclidean_map.png)

Euclidean map results are stored in the *odm\_dem* directory.

## dem-gapfill-steps

포인트 클라우드에서 DEM으로 가는 과정은 보기보다 복잡하다. DEM은 *레스터* 영상이며, 레스터 영상의 각 픽셀은 반드시 값을 가지고 있어야 한다. 레스터 영상의 해상도를 고려할 때, 특정 부류의 픽셀은 어떤 점군의 객체와도 매칭이 되지 않거나, 
한 개 또는 그 이상의 값과 매칭될 수도 있다.그러나 어떤 값과도 매칭되지 않는 경우라고 하더라도 반드시 값을 부여해야하며, 그렇지 않을 경우 공란(gap)이 생성될 수밖에 없다. 이를 해결하기 위한 방법으로 각 픽섹 주변으로 특정 크기의 반지름을 갖는 원을
생성한다. 각 픽셀의 원에 포함되는 모든 포인트는 해당 픽셀의 소속된 것으로 간주한다.

![픽셀과 포인트 (좌), 라디우스 0.5 (중) and 라디우스 1
(우)\label{fig:dem_gapfill_steps}](images/figures/dem_gapfill_steps.png)

But how big should the radius be? If too small, as in the 0.5 radius
example above, some cells might remain empty. If too big, there will be
too much smoothing and accuracy will suffer. Since different point
clouds have varying degrees of density, one solution is to compute
multiple DEMs with different radiuses and stack them.
그렇다면 어느 정도로 반지름을 설정해야하는가? 위와 같이 반지름이 너무 작은 경우, 여전히 어떠한 점군 객체도 포함하지 못하는 픽셀이 존재한다. 반면, 반지름이 너무 큰 경우 스무딩 효과가 발생할 것이며, 정확도(?) 또한 떨어질 수밖에 없다. 포인트 클라우드
객체마다 서로 다른 밀집도를 가지고 있기 때문에, 다양한 반지름을 직접 시도해보며 DEM 생성 결과를 파악하는 것이 가장 적합할 것이다.

![Gap fill interpolation with 2 DEM layers\label{fig:dem_gapfill_interpolation}](images/figures/dem_gapfill_interpolation.png)

Results with smaller radiuses (more accuracy, more gaps) are placed at
the top, while results with bigger radiuses (less accuracy, less gaps)
are placed at the bottom. If there are still gaps at the end of this
process, any remaining gaps are filled using a less accurate smoothed
nearest neighbor interpolation.
반지름이 작은 경우 정확도(?)는 높으나 더 많은 공란을 생성하게 될 것이며, [첫 번째 문장 의역필요].
공란이 지속적으로 발생할 경우, 최근접 보간법을 이용하여 채워지게 된다.
How many layers should there be? However many this option says there
should be. The initial value for the radius for the first layer is set
to half of the raster resolution. Subsequent layers have twice the
radius of their predecessors.
첫번째 레이어의 반지름 초깃값은 레스터 해상도의 절반으로 설정된다. 이후 레이어는 이전 레이어의
두 배로 반지름을 설정한다.

local gridding 방법은 open source Points2Grid 프로젝트에서 사용한 방법과 동일하다.

## dem-resolution

해당 옵션은 픽셀 당 cm 단위로 DEM의 해상도를 결정한다.

![Each square represents a pixel in a raster DEM\label{fig:dem_resolution}](images/figures/dem_resolution.png)

예를 들어, 포인트 클라우드로 덮인 어떤 공간의 면적이 100x50 평방미터이고, dem 해상도가 10cm/픽셀이라면,
최종 DEM 영상의 크기는 다음과 같이 계산된다.

    (100 meters * 100 cm/meter ) / 10 cm/pixel
    = 1000 pixels
    (50 meters * 100 cm/meter ) / 10 cm/pixel
    = 500 pixels

결과적으로 생성된 DEM은 10000x500 픽셀 사이즈의 영상이 된다. 100cm가 1m라는 점을 고려하여 소괄호 내에 100을 곱했다.

## depthmap-resolution

Depthmap은 영상 내 물체의 거리와 관련된 정보를 포함하고 있는 영상이다.

![Image and corresponding depthmap. Darker areas are closer to the
camera. Image courtesy of Dominicus, Cubic Structure, CC BY-SA 3.0\label{fig:depthmap_resolution}](images/figures/depthmap_resolution.png)

Depthmap 영상은 밀집 점군을 계산하는 사진측량학적 처리과정에서 생성된다. Depthmap의 해상도가 높을수록, 포인트 간 삼각측량을 수행하는 시간 또한 많이 소요된다. 고해상도의 Depthmap은 포인트 수를 증가시키나, 노이즈도 증가하는 문제가 있다. 
Depthamp의 해상도가 2배 높아지게 되면 처리시간은 약 4배 증가한다. 만일 포인트 클라우드의 밀집도를 변경하고 싶다면, 옵션을 변경하면 된다. 포인트 클라우드는 3차원 모델의 기반 데이터이며, 결과적으로 정사영상을 생성에도 사용된다. 도시를 맵핑했을 때, 
건물에 구멍이나 웨이브가 생성되는 경우가 있는데, 이 경우 포인트 클라우드의 밀집도를 높이는 것이 도움이 된다. 그러나 밀집도를 과하게 높이게 되면 노이즈 또한 함께 증가하므로 적절하게 조절하는 것이 필요하다.

## dsm

이 옵션은 디지털 표면 모델(DSM)을 생성한다. DSM은 지표나 건물, 나무 등과 같은 것을 포함하여 점군 중에서 가장 높은 고도를 가지고 있는 점들만 취하여 생성한다. 즉, 두 개의 점이 동일한 픽셀에 포함될 경우 가장 높은 고도값을 가지는 값만을 사용하는 것이다.
포인트 클라우드에 존재하는 공란의 경우 **dem-gapfill-steps**에서 설명한 방법을 이용하여 보간한다. 결과는 *odm\_dem\dsm.tif*에 저장된다.


## dtm

이 옵션은 디지털 지형 모델(DTM)을 생성한다. DTM은 간단한 모폴로지 필터[^filter] (SMRF).를 이용하여 포인트 클라우드를 분류함으로써 생성할 수 있다. 이 옵션을 설정하면 **pc-classify** 옵션이 적용되며, 해당 옵션은 포인트 클라우드를 지면과 비지면으로 
분류한다. 비지면 포인트로 분류된 객체들은 DTM 계산에 적용되지 않는다. 공란은 **dem-gapfill-steps**에서 설명한 방법을 이용하여 보간한다. SMRF 분류 알고리즘을 조절하는 것과 관련한 정보는 **pc-classify**에서 확인할 수 있다. 결과는 *odm\_dem/dtm.tif*에 
저장된다.

![DSM (좌) vs. DTM (우)\label{fig:dsm_vs_dtm}](images/figures/dsm_vs_dtm.png)

## end-with

해당 프로세스의 파이프라인은 여러 단계로 구성되어 있으며, 각 단계는 순차적으로 실행된다. 생성된 결과는 예상과는 다를 수 있으며, 다른 옵션을 사용한 결과들을 비교해보고 싶을 때도 있을 것이다. 옵션을 조작하는 것은 파이프라인의 특정 단계에만 영향을 미치기 때문에,
모든 단계를 끝까지 실행해볼 필요는 없다. 이 옵션을 사용하면, 설정한 단계에서 해당 프로세스를 멈출 수 있다. 가능한 옵션값들은 다음과 같다.

  - **dataset**

  - **split**

  - **merge**

  - **opensfm** (Structure From Motion)

  - **mve** (Multi-View Stereo)

  - **odm\_filterpoints**

  - **odm\_meshing**

  - **odm\_25dmeshing**

  - **mvs\_texturing**

  - **odm\_georeferencing**

  - **odm\_dem**

  - **odm\_orthophoto**

이 옵션은 **rerun-from**와 주로 함께 사용한다.

## fast-orthophoto

최소한의 데이터를 이용하여 정사영상을 빠르게 생성하고 싶은 경우가 있다. 이전에 다뤘던 파이프라인에서 정사영상을 생성하는 일반적인 정사영상 생성 파이프라인을 다시 살펴보자.

![데이터 처리 파이프라인 (normal)\label{fig:processing_pipeline_normal}](images/figures/processing_pipeline_normal.png)

**fast-orthophoto** 옵션을 사용하면, 일반적인 정사영상 생성 파이프라인에서 MVS 단계를 생략한다.

![Processing Pipeline (fast-orthophoto)\label{fig:processing_pipeline_fastortho}](images/figures/processing_pipeline_fastortho.png)

MVS는 매우 밀집한 포인트 클라우들 생성하는 반면 SFM은 희소한 포인트 클라우드를 생성하는 것을 상기해보자.

![희소 점군 (상) vs. 밀집 점군 (하) 결과\label{fig:sparse_vs_dense_point_cloud}](images/figures/sparse_vs_dense_point_cloud.png)

밀집 점군와 희소 점군 모두 메쉬를 생성할 수 있다. 그러나, 밀집 점군을 이용하는 것이 좀 더 양질의 메쉬를 생성할 수 있다. 위의 밀집 점군 영상에서, 가운데이 위치한 건물은 잘 생성된 반면, 희소 점군에서는 거의 사라진 것을 확인할 수 있다. 
특히 건물의 경우 밀집 점군 없이는 모델링하는 것이 매우 어렵기 때문에, 이 옵션은 도시 지역에 적용하기에는 적합하지 않다. 반면, 농지와 같이 평탄한 지역의 경우, 건물과 같은 구조물이 거의 없기 때문에, 희소 점군만 사용하더라도 양질의 결과를
생성할 수 있다. 이 옵션은 경우 또한 고도가 매우 높은 영상이나 중첩률이 50퍼센트 미만인 영상을 이용할 경우 매우 잘 적용된다.

![일반적인 정사영상 (상) vs. 고속 정사영상 (하)\label{fig:normal_vs_fastortho}](images/figures/normal_vs_fastortho.png)

위의 두 영상을 비교해보면, 위에 있는 영상 내 건물이 아래에 있는 영상 내 건물 보다 더 잘 생성된 것을 볼 수 있으나, 지면의 경우 두 영상 모두 비슷한 것을 볼 수 있다.

이 옵션은 아주 큰 데이터 셋을 이용하는 경우 소요시간에 있어 매우 큰 차이를 보일 수 있다.

## gcp

기본적으로 오픈드론맵은 프로젝트 경로에서 *gcp_list.txt* 파일을 찾는다. gcp_list.txt
파일이 있으면 지오레퍼런싱 정확도를 높이기 위해 gcp_list.txt에 저장된 지상기준점을 사용한다.
gcp 옵션을 사용하면 GCP 파일이 있는 경로를 사용자가 직접 정할 수 있다. 지상기준점과 GCP
파일에 대해서는 *지상기준점* 장에서 자세히 설명한다.

WebODM은 gcp 옵션을 표시하지 않으며, GCP 파일이 함께 업로드되면 자동으로 설정된다.

## help

사용 가능한 모든 옵션을 표시한 후 종료한다.

## ignore-gsd

오픈드론맵은 처리 속도를 좋게 하기 위해 여러 가지 최적화 기법에 의존한다. 오픈드론맵이 사용하는
최적화 기법 중 하나는 모든 영상에서 평균 지상표본거리(ground sampling distance, GSD)를
계산하는 것이다. 평균 GSD를 이용하는 이유는 다음 두 가지와 같다.

1\) 정사영상 및 DEM 해상도 제한. 2) Compute a
good target size for the images when texturing 3D models.

평균 GSD를 이용해서 해상도에 제한을 두는 이유는 간단하다. 오픈드론맵은 *orthophoto-resolution*,
*dem-resolution* 옵션을 이용해서 출력 해상도를 결정할 수 있지만 최대 해상도가 얼마인지 계산하는
것은 귀찮은 일이다. 예를 들어 영상을 400ft(약 122m) 고도에서 촬영했을 때 공간해상도를 화소당 0.1cm로
설정하는 것은 말이 안 된다. 이 경우 0.1cm급 해상도를 제공하기에는 영상에 담긴 정보가 충분하지 않기
때문이다. 그러므로 최적화 기법을 통해 해상도를 낮춰 더 합리적인 값을 도출해야 한다.

이와 유사하게, 만약 오픈드론맵이 정사영상의 공간해상도가 화소당 5cm인 것을 알고 있다면 3차원 모델의
텍스처를 만드는 과정에서 최대 해상도 영상을 사용하는 것은 낭비라는 것을 알 수 있다. 따라서 오픈드론맵은
정사영상의 해상도를 고려하여 영상 크기를 텍스처링 전에 미리 줄인다. 이렇게 하면 처리 속도가 빨라지고
메모리 사용량이 줄어든다.

이러한 접근에는 다음과 같은 몇가지 주의 사항이 있다. 1) GSD 값은 전체 영상의 GSD 값을 평균하는
방식으로 계산한다. 더 나아가서 비행 고도는 sparse point cloud에서 산출된 평균고도면을 이용해서 추정한다.

![Average plane height (dotted gray line) and terrain (black line)\label{fig:average_plane_height}](images/figures/average_plane_height.png)

이는 곧 언덕이나 산처럼 고도 변화가 심한 곳은 낮은 GSD 값으로 표현될 수 있는 것을 의미한다.
이러한 상황에서는 해상도가 이상적인 값도가 더 낮게 제한될 수 있다.

2\) 메시에 색을 입히려면 텍스처링 프로그램이 영상의 가장 “좋은” 부분을 선택해야 한다. 영상에서 가장
“좋은” 부분이 어디인지 결정하기 위해 알고리즘은 몇가지 요인을 고려한다. **texturing-data-term** 이
기본값인 gmi로 설정되어 있으면 몇가지 요인 중 하나는 흐린 영상을 택하지 않기 위해 *선명도(sharpness)* 가
된다. 텍스처링을 하기 전에 영상 크기를 줄이면 *선명도* 가 전체적으로 감소하는 부작용이 생기며 이는 곧
텍스처링 알고리즘이 고려하는 요인 중 선명도의 가중치를 낮춘다. 따라서 텍스처링 결과를 볼 때 더 흐린 모습이
나타날 수 있다.

정사영상이 기대했던 것보다 흐리게 생성되거나 출력 데이터의 해상도가 기대했던 것보다 낮을 때는 **ignore-gsd**
옵션을 켜는 것이 좋다. 그러나 이 옵션을 켰을 때 처리 시간이 길어지고 메모리 사용량이 늘어난다.

## matcher-distance

SFM은 영상 쌍을 매칭하는 과정, 즉 서로 특징을 공유하는(같은 물체가 나타난) 영상을 찾는 과정을 포함한다.
brute force는 영상 매칭을 수행하는 간단한(naïve) 접근법이다. 이 방법은 결과가 정확하지만 탐색 시간이
오래 걸린다. 100개 영상 데이터셋을 전부 비교하고 매칭하는 과정에는 많은 시간이 걸린다. brute force
비교를 얼마나 해야 하는지 알고싶다면 전체 영상 개수와 전체 영상 개수에서 1을 뺀 값을 간단히 곱하면 된다.

    100 * (100 - 1)
    = 9900 comparisons

이 숫자는 데이터셋의 규모가 커질수록 빠르게 증가한다. 오픈드론맵은 속도를 빠르게 하기 위해 오픈드론맵은
일정한 패턴으로 취득된 항공 영상 데이터셋은 영상끼리 서로 인접해 있다는 점을 이용한다. 각 영상은 GPS
위치 정보를 포함하고 있으므로 오픈드론맵은 매칭에 사용할 영상을 탐색하는 거리에 대한 임계값을 설정할 수
있다. 이 방법을 선점 매칭(preemptive matching)이라 한다.

![Dots represent approximate image locations, extracted from EXIF tags.
The md value represents the maximum distance that the image p1 will
search for other images. Images outside of the circle will not be
considered for matching\label{fig:matcher_distance}](images/figures/matcher_distance.png)

거리는 미터(m) 단위로 표현한다. 0으로 설정하면 선점 매칭을 하지 않는다. EXIF 태그에 위치 정보가 없다면
이 옵션은 해제된다. 이 옵션은 **matcher-neighbors** 옵션과 함께 작동한다.

## matcher-neighbors

**matcher-distance** 와 비슷하게 이 옵션은 영상끼리의 위치를 고려해서 매칭하는 선점 매칭을 수행한다.
아래 그림은 이 옵션을 8로 설정했을 때 어떤 결과가 나오는지를 보여준다.
데이터셋의 중복도(overlap)가 높다면 이 값을 늘려주는 것이 좋다. 좋은 매칭 쌍이 계산되지 않을 수가 있고,
이는 reconstruction 정확도를 낮출 수 있기 때문이다. 이 옵션을 0으로 설정하면 해제된다. EXIF 태그에
위치 정보가 없다면 이 옵션은 해제된다. 이 옵션은 matcher-distance 옵션과 함께 작동한다.


![Dots represent approximate image locations, extracted from EXIF tags.
When the matcher-neighbors is set to 8, only the 8 nearest neighbors
(highlighted in gray) are considered for matching with image p1\label{fig:matcher_neighbors}](images/figures/matcher_neighbors.png)

데이터셋의 중복도(overlap)가 높다면 이 값을 늘려주는 것이 좋다. 좋은 매칭 쌍이 계산되지 않을 수가 있고,
이는 reconstruction 정확도를 낮출 수 있기 때문이다. 이 옵션을 0으로 설정하면 해제된다. EXIF 태그에
위치 정보가 없다면 이 옵션은 해제된다. 이 옵션은 **matcher-distance** 옵션과 함께 작동한다.

## max-concurrency

기본적으로 오픈드론맵은 가용한 모든 CPU 자원을 사용한다. 그러나 공유 서버나 처리 중 다른 작업을 하고 싶을
때 등 모든 CPU를 사용하기를 원하지 않는 경우가 있다. 이 옵션을 시용하면 오픈드론맵이 사용하는 최대 CPU
코어 수를 제한할 수 있다(항상 제한하는 것은 아니다).

## merge

이 옵션은 split-merge 파이프라인에서 어떤 asset을 병합해야 할 지 결정한다. 기본적으로 모든 가능한
asset이 병합되지만, 이 옵션을 이용해서 원하는 것만 고를 수 있다. 이 옵션에 대해서는 *대용량 데이터 처리*
장에서 자세히 다룬다.

## mesh-octree-depth

이 옵션은 3차원 모델을 만들 때 가장 중요하다. 이 옵션으로 포인트 클라우드에서 메시를만드는 알고리즘인
Screened Poisson Reconstruction의 중요한 변수를 정하기 때문이다. Screened Poisson Reconstruction
알고리즘은 흥미진진하지만 이 장에서 다룰 수 있는 범위를 넘으므로 궁금한 독자는 다음 페이지에서
*Surface Reconstruction* 절을 읽어보길 권한다. [https://en.wikipedia.org/wiki/Poisson%27s\_equation](https://en.wikipedia.org/wiki/Poisson's_equation)

mesh-octree-depth 옵션이 결과물에 어떤 영향을 주는지 이해하려면 octree에 대한 개념을 시각적으로
이해하는 것이 좋다. 첫째, octree는 *8개 트리* 를 의미한다(그리스어로 okta는 숫자 *8* 이다).
왜 8개인가? 트리의 각 단계(*깊이*)에 속하는 상자(*노드, 가지*)는 8개 부분으로 나뉘기 때문이다.
1단계에서는 1개 가지만 있다. 2단계에서는 8개 가지가 있다. 3단계에서는 64개 가지가 있고, 단계가 높아지면서
이런 식으로 가지가 늘어난다.

![An octree with depth 1, 2 and 3\label{fig:octree_depths}](images/figures/octree_depths.png)

깊이가 깊어질수록 더 자세한 디테일을 표현한다.

![Points and resulting octree. Image from
http://www.cs.jhu.edu/\~misha/Code/\label{fig:points_and_octree}](images/figures/points_and_octree.jpg)

이 옵션의 실질적인 면을 살펴보면 값이 높을수록 메시 생성 결과는 더 자세해진다는 것을 알 수 있다.
그러나 값이 높을수록 처리 시간이 길어지고 메모리 사용량이 늘어난다. 기본값인 9를 사용했을 때 많은 경우에서
잘 동작한다. 평지일수록 값이 낮으면(6~8) 좋고, 도심에서는 값이 높으면(10~12) 좋다. mesh-octree-depth
값을 높였을 때 **mesh-size** 도 함께 높여야 한다. 더 자세한 메시는 더 많은 삼각형을 요구하기 때문이다.

![mesh-octree-depth 6 and mesh-size 10000 (top) vs. mesh-octree-depth 11
and mesh-size 1000000 (bottom)\label{fig:mesh_octree_depth_comp}](images/figures/mesh_octree_depth_comp.png)

## mesh-point-weight

이 옵션은 **mesh-octree-depth** 처럼 포인트 클라우드에서 메시를 생성하는 과정인 Screened Poisson
Reconstruction 에서 중요한 역할을 한다. 이 옵션은 점의 위치에 더 많은 중요도를 부여하는 방식으로 메시
생성에 영향을 끼친다. In practical terms, higher values can help create higher fidelity
models, but can also lead to the generation of artifacts (undesired alterations).
In general the default value works fairly well.

![mesh-point-weight set to 0 (top) and 20 (bottom). Notice the lack of
edges in the top image and the excessive bumps in the bottom image\label{fig:mesh_point_weight}](images/figures/mesh_point_weight.png)

## mesh-samples

이 옵션은 **mesh-octree-depth** 처럼 포인트 클라우드에서 메시를 생성하는 과정인 Screened Poisson
Reconstruction 에서 중요한 역할을 한다. 이 옵션은 octree에서 하나의 노드에 몇 개의 점이 들어가야
하는지를 정한다. 현실적인 면을 살펴보면 모델의 부드러운 정도를 향상시키기 위해 이 값을 1~20 사이에서
조절해야 한다. 만약 포인트 클라우드에 노이즈가 있으면 이 값은 증가해야 한다. 포인트 클라우드에 노이즈가
없거나 적으면 기본값인 1로 설정해야 한다.

![mesh-samples set to 1 (top) 20 (bottom). Ideal values for this option
are between 1 and 20\label{fig:mesh_samples}](images/figures/mesh_samples.png)

## mesh-size

메모리 사용량과 런타임을 정상 수준으로 유지하기 위해 오픈드론맵은 메시가 포함할 수 있는 삼각형의 최대 개수
를 제한한다. 삼각형이 많을수록 처리가 오래 걸리고 후속 과정에서의 처리 시간도 더 늘어나기 떄문이다. 삼각형
수가 적으면 메시의 품질이 낮아질 수 있다. 만약 3차원 모델의 디테일이 사라져 보이거나 날카로운 삼각형이 보이면
이 옵션 값을 증가시켜 해결할 수 있다. 이렇게 하면 특히 건물이 많은 도심 지역 영상 처리에 좋다.

![Effects of reducing the number of triangles in a mesh. Image courtesy
of Trevorgoodchild, Quadric error metric simplification applied to the
Stanford bunny, CC BY-SA 3.0\label{fig:bunny_simplification}](images/figures/bunny_simplification.png)

## min-num-features

SFM 과정에서 영상은 영상 쌍으로 짝지어져야(pairing) 한다. 영상 쌍을 찾는 과정은 영상에 포함된 특징점을
매칭하는 방법으로 이루어진다. 특징점의 대표적인 예는 건물 꼭짓점이나 차량 모서리 등이 있다. 서로 다른 두
영상에서 같은 특징점을 찾으면 이는 매칭 가능한 영상의 증거로서 사용된다. 그러나 특징점은 모호할 수 있다.
두 영상에 나타난 건물의 완벽해 보이는 건물 꼭짓점은 우연히 건축 양식이 유사한 서로 다른 두 건물의
꼭짓점에서 온 것일 수도 있다. 그러므로 오픈드론맵은 각 영상에서 최대한 많은 특징점을 찾고, 모든 특징점을
가능한 영상 쌍을 찾는 과정에서 사용한다. 이 옵션은 영상 사이에서 가능한 쌍을 모두 찾을 가능성을 높이기
위해 각 영상에서 오픈드론맵이 추출하는 특징점의 최소 개수를 정한다. It does so by progressively lowering certain threshold values, which lead to more, but less ideal features.

![Features (red points) and matches between overlapping images (white
lines). min-num-features controls the target number of red points in
each image\label{fig:image_features}](images/figures/image_features.png)

이 옵션은 숲처럼 식별 가능한 특징점이 적은 곳을 매핑할 때 증가시켜야 한다.

![Can you easily find many good features / reference points in the image
above? Neither can a computer. But increasing the number of features can
increase the chances of a match between images\label{fig:forest_features}](images/figures/forest_features.jpg)

이 옵션의 값을 증가시키면 처리 시간이 길어진다. 그러나 reconstruction이 정상적으로
될 가능성을 높여준다. 어떤 경우에는 오픈드론맵이 부분적으로만 reconstruction을 할
수도 있다. 그런 경우에는 최종 결과물에서 일부 영역이 보이지 않는다는 것을 확인할
수 있을 것이다(정사영상의 일부 영역이 사라진 것처럼 보인다). 이 옵션 값을 증가시키면
매칭 쌍을 찾는데 도움을 주고 더 완전한 reconstruction 결과가 나오게 된다.

이 옵션 값을 증가했을 때 흥미로운 효과 중 하나는 sparse point cloud의 개수가 늘어
난다는 점이다. 그러므로 **fast-orthophoto** 옵션을 함께 사용했을 때 좀 더 좋은 결과가
나올 것이다.

## mve-confidence

MVE를 이용해서 dense point cloud를 생성할 때 각 점에은 0에서 1 사이의 신뢰도 값이
부여된다. 어떤 점의 신뢰도가 0이면 노이즈일 가능성이 큰 것을, 1이면 좋은 점일 가능성이
큰 것을 의미한다. 신뢰도가 특정한 임계값 이하인 점은 제외할 수 있다. 사용자는 이 옵션
값을 증가시켜 노이즈를 제거하거나(정상적인 점도 제거될 수 있다) 값을 감소시켜 더 많은
점을 획득할 수 있다(노이즈가 많아질 수 있다).

![Confidence set to 0.6 (top) and 0 (bottom). Notices some areas are
missing in the top image, but much more noise is present in the bottom
image\label{fig:mve_confidence}](images/figures/mve_confidence.png)

## opensfm-depthmap-method

**use-opensfm-dense** 가 설정되어 있을 때, 이 옵션으로 깊이 맵을 만드는데 사용하는 방법
을 정할 수 있다(깊이 맵에 대한 간단한 논의를 보려면 **depthmap-resolution** 을
참고하라). 3가지 가능한 옵션은 다음과 같다.

  - **PATCH\_MATCH** (기본값)

  - **PATCH\_MATCH\_SAMPLE**

  - **BRUTE\_FORCE**

**PATCH\_MATCH** 와 **PATCH\_MATCH\_SAMPLE** 는 비교적 빠르다. 그러나 정상적인 점을
놓칠 수도 있어서 생성된 포인트 클라우드에 빈 공간이 생길 수도 있다. **BRUTE\_FORCE** 는
비교적 느리다. 그러나 더 철저한 과정을 수행하기 때문에 더 완전한 결과를 얻을 수 있다.
기본값은 대체로 잘 동작하며 포인트 클라우드에 빈 공간이 많을 때만 **BRUTE\_FORCE** 를
사용하면 된다. **PATCH\_MATCH** 는 Accurate Multiple View 3D Reconstruction Using
Patch-Based Stereo for Large-Scale Scenes[^scenes] 논문을 기반으로 구현하였다.
**PATCH\_MATCH** 는 **PATCH\_MATCH\_SAMPLE** 에 비해 약간 느리지만 조금 더 밀집된
포인트 클라우드를 생성한다.

## opensfm-depthmap-min-patch-sd

**use-opensfm-dense** 옵션이 켜져 있고 **opensfm-depthmap-method** 옵션이
**PATCH\_MATCH** 또는 **PATCH\_MATCH\_SAMPLE** 로 설정되어 있을 때 이 옵션은 깊이
맵 생성 시 실행 시간을 줄이기 위해 건너뛰어야 하는 영역을 설정한다. 패치(patch)는
간단히 말해 영상의 작은 부분을 의미한다.

![Patch in an image\label{fig:image_patch}](images/figures/image_patch.png)

계산 중에 입력된 영상은 패치로 나뉜다. 각 패치에는 패치에 포함된 픽셀로 계산한 분산
값이 배정된다. 여기서 중요한 점은 하늘 처럼 균일한 영상(픽셀 값이 서로 비슷한 영상)에는
삼각측량을 하기에 충분한 정보가 담겨있지 않다는 것이다.

![Patch with high variance (left) vs. patch with low variance (right)\label{fig:patch_variance_comp}](images/figures/patch_variance_comp.png)

분산은 표준편차를 제곱한 값이다. 그러므로 이 옵션은 OpenSfM의 깊이 맵 계산 과정에
포함되기 위해 필요한 최소 표준편차를 정의한다. 이 옵션을 너무 작게 설정하면 처리 시간이
길어지고, 너무 높게 설정하면 무늬가 균일한 영역이 무시될 가능성이 크다. 아래 스크린샷은
이러한 현상을 보여준다.

![opensfm-depthmap-min-patch-sd set to 5 (top) and 2.5 (bottom). Notice
the lack of roads in the image on the left. Roads have uniform colors
(low variance). Setting a high value caused the roads to disappear!\label{fig:osfm_patch_sd_comp}](images/figures/osfm_patch_sd_comp.png)

## orthophoto-bigtiff

BigTIFF는 TIFF 형식의 확장으로 4GB 이상의 파일 크기를 지원한다. 이 옵션에서 가능한 값은
다음과 같다.

  - **YES**: 정사영상 파일 출력이 항상 BigTIFF 형식이 되도록 강제

  - **NO**: 정사영상 파일 출력이 항상 전통적인 TIFF 형식이 되도록 강제

  - **IF\_NEEDED**: 필요할 때 BigTIFF 사용 (영상 파일 크기가 4GB 이상이고 압축을
    사용하지 않을 때. **orthophoto-compression** 참조)

  - **IF\_SAFER** (기본값): 결과 파일이 4GB 이상일 때 BigTIFF 사용. 일종의 어림법
  (heuristics)이고, 압축 여부에 따라 항상 동장하지 않을 수 있음

BigTIFF는 전통적인 TIFF 형식의 하위 호환성이 보장되지 않는다. 즉 TIFF를 지원하는
프로그램이 항상 BigTIFF를 지원하는 것은 아니다. 뷰어가 명시적으로 BigTIFF를 지원한다고
되어 있어야 한다. 다행스럽게도 대부분의 GIS 프로그램은 BigTIFF를 지원한다. 일부 레거시
프로그램들은 그렇지 않다. 만약 레거시 프로그램을 사용한다면 이 옵션을 **NO** 로 설정
하는 것이 좋다. 만약 *TIFFAppendToStrip:Maximum TIFF file size exceeded* 오류가
발생한다면 **IF\_SAFER** 에서 사용하는 어림법이 제대로 작동하지 않은 것이다. 이런 경우
에는 **YES** 로 옵션을 변경하면 오류를 해결할 수 있다.

## orthophoto-compression

압축은 실행 시간이 길어지는 대신 공간을 절약할 수 있는 방법이다. 이 옵션에서 가능한 값은
다음과 같다.

  - **JPEG:** 품질 값이 75인 JPEG 압축을 사용한다. JPEG은 손실 압축 방법이므로 압축
  과정에서 영상 품질이 손실된다.

  - **LZW:**  Lempel–Ziv–Welch 압축을 사용한다. 이 방법은 비손실 압축 방법이며, 압축
  과정에서 영상 품질이 손실되지 않는다.

  - **PACKBITS:** LZW와 마찬가지로 비손실 압축 방법이다. 거의 대부분 PACKBITS 형식을
  지원하지만 LZW에 비해 압축률이 낮다.

  - **DEFLATE** (기본값): ZIP 압축으로도 불리는 비손실 압축 방법이다. LZW에 비해 파일
  크기가 약간 작다.

  - **LZMA:** 또다른 비손실 압축 방법이다.

  - **NONE:** 압축을 하지 않는다. 정사영상이 더 빨리 생성되지만 파일 크기가 커진다.

## orthophoto-cutline

이 옵션을 켜면 오픈드론맵은 절단선(cutline)을 생성한다. 절단선은 영상에 나타나는 객체
(feature)의 경계를 따라 생성되는 폴리곤이다.

![Cutline\label{fig:cutline}](images/figures/cutline.png)

![Cutline going around the edges of a car\label{fig:cutline_car}](images/figures/cutline_car.png)

절단선은 서로 중첩된 여러 정사영상을 병합할 때 경계선에서 생상 차이를 보정하기 위해
사용한다. 이 옵션은 split-merge 파이프라인을 이용해서 대용량 데이터셋을 처리할 때
사용한다(*대용량 데이터 처리* 장을 참고하라).

절단선은 *odm\_orthophoto/cutline.gpkg* 에 저장된다.

## orthophoto-no-tiled

오픈드론맵은 기본적으로 타일 크기가 256x256 픽셀인 타일 처리된 TIFF를 생성한다.
여기서 타일링은 웹 뷰어에서 타일을 만드는 것과는 달리 파일 내에서 데이터의 구성과 관련이
있다. 데이터는 타일 구조나 띠 구조로 구성할 수 있다. 데이터 전체를 읽거나 표시할 때 타일
구조와 띠 구조는 동일한 성능을 발휘한다. 반면 줌 기능 등을 이용해서 데이터의 일부에
접근할 때는 타일 구조의 성능이 더 좋다. 이런 경우 더 적은 데이터만 읽으면 되기 때문이다.

![Tiles (left) vs. stripes (right). The red rectangle is the area being
accessed. The highlighted area shows the amount of data that needs to be
read from the file. The smaller the highlighted area, the quicker it is
to access the file\label{fig:tiles_vs_stripes}](images/figures/tiles_vs_stripes.png)

파일 생성시 타일 처리는 기본값이다. 타일 처리된 정사영상은 띠 구조에 비해 생성 시간이
오래 걸린다. 띠 구조를 사용하려면 이 옵션을 끄면 된다.

## orthophoto-resolution

**dem-resolution** 옵션과 같다. 그러나 DEM 대신 정사영상에 적용된다.

## pc-classify

포인트 클라우드에 포함된 포인트는 지면, 건물, 식생, 물 등 여러 가지 클래스 중 하나로
분류할 수 있다. pc-classify 설정을 켜지 않았을 때 모든 포인트는 ‘미분류(unclassified)’
로 지정되며 오픈드론맵은 각 포인트가 어떤 클래스에 속하는지 자동으로 분류하지 않는다.
그러나 pc-classify 옵션을 켜면 simple morphological filter (SMRF) 알고리즘에 의해
지면에 속하는 포인트를 자동으로 분류하여 전체 포인트 클라우드를 지면점과 비지면점으로
지정한다. 지면점에 속하는 포인트 클라우드는 수지지형모델(digital terrain model, DTM)
을 생성할 때 사용할 수 있다.

![Point cloud (top) and classification results with SMRF (bottom)\label{fig:point_cloud_classification}](images/figures/point_cloud_classification.png)

SMRF 알고리즘은 다음과 같이 4가지 매개변수로 조절할 수 있다. 오픈드론맵에 저장된
기본값을 사용하되, 결과를 확인해보고 조금씩 조절해볼 필요가 있다.

  - **smrf-scalar** 는 경사도(slope)에 의존적인 매개변수에 임계값을 설정하는데 사용한다.
    좋은 결과를 얻으려면 **smrf-threshold** 가 증가했을 때 **smrf-scalar** 를 감소시키고,
    **smrf-threshold** 가 감소했을 때 **smrf-scalar** 를 증가시켜야 한다.

  - **smrf-slope** 는 “가장 보편적인 지형 경사도”, 즉 수평 거리에 따른 지형 고도 변화
    비율로 설정해야 한다. 예를 들어 고도가 수평 거리 10미터마다 1.5미터씩 변한다면
    1.5 / 10 = 0.15를 입력하면 된다. 언덕이나 산 등 지형 고도 변화가 심한 곳은
    **smrf-slope** 를 증가시켜야 하며, 평지에서는 감소시키면 된다. 좋은 결과를 얻으려면
    0.1보다는 커야 하며 1.2보다는 작아야 한다.

  - **smrf-threshold** 는 비지면 객체의 최소 고도(단위: m)로 설정한다. 예를 들어 5m로
    설정하면 건물을 구분하기에는 충분하지만 자동차를 구분하기에는 충분하지 않다. 자동차까지
    구분하기 위해서는 2m 또는 1.5m까지 값을 줄여야 한다. 이 매개변수는 결과에 가장 많은 영향을 끼친다.

  - **smrf-window** 는 비지면 객체 중 가장 큰 객체의 크기(단위: m)로 설정해야 한다.
    예를 들어 영상이 나무와 같이 작은 물체로 가득 차 있는 경우 **smrf-window** 를 감소시키고,
    건물과 같이 큰 물체가 포함된 경우 **smrf-window** 를 증가시키면 된다. 좋은 결과를 얻으려면
    10m으로 고정하는 것이 좋다.

SMRF를 잘 활용하려면 SMRF의 한계를 잘 이해해야 한다. 특히 SMRF는 건물이나 나무에 속하는
포인트를 지면점으로 구분하는 제 2종 오류를 일으킬 수 있다.

![Input surface model\label{fig:input_surface_model}](images/figures/input_surface_model.png)

![Terrain model obtained with default SMRF options. Note some houses were
mistakenly included and artifacts are lingering around the edges of
removed objects\label{fig:smrf_model_default}](images/figures/smrf_model_default.png)

![A much improved terrain model obtained by setting smrf-threshold 0.3
(decreased), smrf-scalar 1.3 (increased), smrf-slope 0.05 (decreased)
and smrf-window 24 (increased)\label{fig:smrf_model_improved}](images/figures/smrf_model_improved.png)

포인트 클라우드를 자동으로 분류할 수 있는 방법을 개발하는 것은 활발히 연구중인 주제다.
SMRF는 대체로 잘 동작하지만 고품질 수치지형모델을 생성하려면 SMRF와 관련된 설정을
조정하면서 결과를 유심히 살펴봐야 한다. 그리고 지도학습(supervised or trained)
알고리즘으로 구현된 분류 방법을 적용한 결과와 SMRF를 적용한 결과를 비교해 보는 것도
좋다. CloudCompare는 SMRF 이외의 포인트 클라우드 분류 방법을 구현한 무료 오픈소스
소프트웨어다.

## pc-csv

기본적으로 포인트 클라우드는 압축된 LAZ 형식으로 저장된다. LAZ는 사람이 읽을 수 있는
(human readable) 형식이 아니며 단순한 텍스트 에디터로 열 수 없다. 이 옵션을 켜면
CSV(Comma Separated Value) 형식으로 포인트 클라우드 복사본을 내보낼 수 있다. CSV 파일
형식은 포인트 클라우드를 다루기에 좋은 파일 형식은 아니다. 그러나 포인트 클라우드에
들어있는 값을 디버깅하거나 LAS/LAZ를 열 수 없는 프로그램을 활용할 때 필요하다. 결과
포인트 클라우드는 *odm\_georereferencing/odm\_georeferenced\_model.csv* 에 저장된다.

## pc-ept

이 옵션을 켜면 포인트 클라우드를 네트워크 상에서 효율적으로 스트리밍 할 수 있는 EPT
(Entwine Point Tile) 형식[^format]으로 내보낼 수 있다. EPT 데이터셋은 효율적인
분석, 검색이 가능하며 PDAL[^pdal] 같은 도구를 이용해서 변환할 수 있다. 결과 포인트
클라우드는 *entwine\_pointcloud* 에 저장된다.

## pc-filter

포인트 클라우드의 노이즈는 통계적 필터를 이용해서 제거할 수 있다. 이 옵션은 통계적
필터에서 사용하는 표준편차 값을 설정한다. 표준편차는 점들이 주변 점들이 비해 상대적으로
얼마나 퍼져 있는지를 나타내는 지표다. 통계적 필터는 가까운 16개 점을 확인하고, 해당 점의
표준편차를 계산한다. 즉 각 점이 점들 사이의 평균 거리에 비해 얼마나 떨어저 있는지 확인
한다. 만약 어떤 점이 주변 점들에 비해 너무 떨어져 있다고 판단하면, 즉 임계값에 비해
높은 표준편차 값을 가지고 있으면 이 점은 이상치(outlier)로 지정된다.

![The gray point has a high standard deviation, so it’s labeled as an
outlier\label{fig:pc_filter}](images/figures/pc_filter.png)

이 값을 너무 높게 설정하면 노이즈가 많이 나타난다. 너무 작게 설정하면 정상적인 점도
삭제된다. 이 옵션을 0으로 설정하면 통계적 필터링을 하지 않는다.

## pc-las

기본적으로 포인트 클라우드는 압축된 LAZ 형식으로 저장된다. 그러나 모든 프로그램이 LAZ
형식을 지원하는건 아니다. 이 옵션을 켜면 포인트 클라우드를 비압축 LAS 형식으로 저장
한다. 결과 포인트 클라우드는 *odm\_georereferencing/odm\_georeferenced\_model.las*
에 저장된다.

## rerun

Shorthand for **rerun-from \<step\>** **end-with \<step\>**.

## rerun-all

Shorthand for **rerun-from dataset**, while also removing all output
folders before starting the process.

## rerun-from

Same as **end-with**, except that it instructs the program to resume
execution from a specific point in the pipeline, skipping previous
steps.

When using WebODM the rerun-from option is automatically set when using
the **Restart** button dropdown.

![Restart drop-down in WebODM\label{fig:restart_dropdown_webodm}](images/figures/restart_dropdown_webodm.png)

It’s not always possible to restart a task from a certain step in
WebODM. The processing node has to support task restarts and the task
intermediate results need to have been kept on disk (by default they are
kept only for 2 days). See *The NodeODM API* chapter for information on
how to change the number of days results are kept.

## resize-to

SFM 과정에서 오픈드론맵은 각 영상에서 특징점을 추출한다. 오픈드론맵은 특징점을 빠르게
추출하기 위해 특징점 추출 전에 모든 영상의 크기를 줄인다. 이 옵션은 영상의 가로/세로 중
긴 쪽의 크기를 어떻게 줄일지를 결정한다. 중요한 점은 이 옵션은 입력은 물론 모든 처리 과정에서
영상에 어떠한 영향도 미치지 않는다는 것이다. 이 옵션을 바꾼다고 해서 정사영상이나 3차원
모델의 품질이 낮아지지 않는다. 이 옵션은 자동차, 건물 등 많은 객체가 영상에 나타날 때
줄일 수 있으며 숲이나 사막 등 식별 가능한 객체가 적을 때 높여야 한다.

## skip-3dmodel


Sometimes all that a user wants is an orthophoto. In that case, it’s not
necessary to generate a full 3D model. This option saves some time by
skipping the commands that produce a 3D model. A 2.5D model (a 3D model
where elevation is simply *extruded* from the ground plane) is still
generated. 2.5D models are not *true* 3D models as they cannot represent
the true shape of objects such as overhangs, but work well for the
purpose of rendering orthophotos.

![3D model (top) vs. 2.5D model (bottom). Note the absence of overhangs
on the bottom\label{fig:3d_vs_25d_model}](images/figures/3d_vs_25d_model.png)

By default both models are created. See also **use-3dmesh**.

## sm-cluster

Cluster ODM instance로 URL을 지정한다. **split** 옵션과 결합하면, 다중 처리 노드를 사용하여 
대규모 데이터셋을 병렬로 처리하도록 하면서 분산된 split-merge 파이프라인을 가능하게 한다. 
이 옵션은 *대규모 데이터셋 처리* 장에서 자세히 다룬다. 

## smrf-scalar

SMRF에 대한 스칼라 변수를 제어한다. (**pc-classify** 참조).

## smrf-slope

smrf-slope 변수를 제어한다. (**pc-classify** 참조).

## smrf-threshold

smrf-threshold 변수를 제어한다. (**pc-classify** 참조).


## smrf-window

smrf-window 변수를 제어한다. (**pc-classify** 참조).

## split

입력 영상 수보다 작은 숫자로 설정하면 split-merge 파이프라인이 활성화된다. 
이 옵션은 *대규모 데이터셋 처리* 장에서 자세히 다룬다.

## split-overlap

split-merge 파이프라인 활성 중에 하위 모델(submodel)이 포함해야 하는 중복도(미터 단위) 크기를 지정한다. 
이 옵션은 *대규모 데이터셋 처리* 장에서 자세히 다룬다.


## texturing-data-term

mesh의 각 부분이 겹쳐서 여러 개의 이미지에 의해 촬영되었을 가능성이 높기 때문에, 
mesh의 각 부분에 대해 어떻게 최적의 이미지를 선택할 것인지에 대한 옵션이다. 
이 프로세스는 *view selection* 으로 알려져 있으며 *data term* 또는 *cost function* 에 따라 처리된다.

![view selection 프로세스는 이미지 하나를 mesh의 영역에 할당한다. 위의 스크린샷에서 각기 다른 색상은 
영역에 할당된 다른 이미지를 나타낸다.\label{fig:view_selection_mesh}](images/figures/view_selection_mesh.png)

두 데이터 용어는 다음과 같다:

1.  gmi (default)

2.  area

GMI는 *Gradient Magnitude Image* 의 약자다. 그래디언트(gradient)는 *값의 변화* 로 종종 이해하기 쉽도록 그래픽으로 표현된다. 
magnitude은 그래디언트가 얼마나 *변화* 하는지 측정한다. 그래디언트가 점진적으로 변화하면 그래디언트가 갑자기 변하는 경우보다 더 낮은 크기를 갖게 된다.

![그래디언트(gradient) (black to white)\label{fig:magnitude_gradient}](images/figures/magnitude_gradient.jpg)

그래디언트의 목적은 초점이 맞춰진 영상의 영역을 우선순위로 지정하는 것이다 (이 영역들은 더 높은 그라데이션 크기를 보이게 된다). 
그래디언트 이미지는 *Sobel* 엣지 검출기를 실행하고 결과에 대한 그래디언트를 계산한다.

![이미지(위쪽) vs. 흑백 구배(하단)에 매핑된 Sobel 엣지 검출기. 물은 그래디언트 변화가 적은 반면 해안선은 그래디언트 변화가 더 크다.
\label{fig:image_sobel}](images/figures/image_sobel.png)

데이터 용어 *면적* 은 다르게 적용된다. 단순히 메쉬의 특정 부분에 가장 큰 영역을 제공하는 이미지를 우선시한다.

![textured mesh로 결정된 삼각형\label{fig:triangle_in_mesh}](images/figures/triangle_in_mesh.png)

![같은 삼각형이 두 개의 이미지에 투영되었다. 위쪽 이미지의 삼각형 영역이 더 크기 때문에 위쪽 이미지는 삼각형을 색칠하기 위해 선택된다.
\label{fig:triangle_in_two_images}](images/figures/triangle_in_two_images.png)

![데이터 용어 *gmi* (상단) vs. *area* (하단). 위에서 도랑과 파란 차의 선예도(sharpness)가 느껴지지만, 도로에 나무가 있는 것을 주목해야 한다.
\label{fig:data_term_gmi_vs_area}](images/figures/data_term_gmi_vs_area.png)

상단 이미지를 보면 나무가 위쪽에 잘못 배치되었다. 나무는 높은 그래디언트를 가지고 있는 반면, 도로는 그렇지 않다.

![비교를 위해 UAV로 촬영된 실제 이미지\label{fig:actual_image_from_uav}](images/figures/actual_image_from_uav.png)

## texturing-keep-unseen-faces

파이프라인의 texturing 부분에 대한 입력은 mesh, 카메라 및 이미지로 구성된다. 
입력 mesh는 삼각형으로 되어 있다. 
texturing 중 영상에 대하여 어떤 삼각형을 볼 수 있는지 점검하고, 기본적으로 모든 영상에서 삼각형이 보이지 않으면 출력되지 않는다.

![보이지 않는 면은 textured mesh에서 제거됨(위) vs. 면들이 색이 없는 상태로 됨(아래)
\label{fig:texturing_keep_unseen_faces}](images/figures/texturing_keep_unseen_faces.png)

이 옵션은 영상에 보이든 안 보이든 상관없이 모든 삼각형을 유지하도록 한다.

## texturing-nadir-weight

texturing 중 mesh의 각 섹션에 가장 적합한 이미지를 선택해야 한다. 
data term 사용(**texturing-data-term** 참조) 외에, 2.5D mesh를 사용하여 정사영상 생성에 사용할 경우, 
이 프로그램은 nadir라는 특수 모드를 활성화한다. 
nadir 모드에서, 수직 또는 거의 수직인 mesh의 일부에 따라 다르게 동작한다 (건물 벽을 고려한다).

1.  수직 부분에 대한 가시성 테스트(visibility test)는 비활성화된다(가시성 테스트에 대한 설명은 **texturing-skip-visibility-test* 참조).

2.  *gmi* 또는 *area* data term을 통해 얻은 품질 점수는 영상의 *nadir-ness* 에 비례하는 값으로 바뀌며 (얼마나 이미지가 수직으로 찍혔는가에 따른 것을 의미한다), 가중치를 곱한다. 
    아래로 바로 찍힌 영상은 비스듬히 찍힌 영상보다 우선 순위가 더 높다.

이 옵션은 nadir factor의 가중치를 제어한다. 이 옵션이 높을수록 mesh의 수직 영역을 texturing하는데 더 많은 nadir 영상을 선택한다. 값이 낮으면 더 적은 nadir 영상을 선택한다.

![카메라 A는 건물 옆면을 더 잘 볼 수 있고 카메라 B는 옆면이 가려져 있다. 그러나 가시성 테스트는 생략하고 카메라 B는 더 nadir하므로, 
nadir 모드 카메라에서는 B가 선택된다.\label{fig:nadir_mode_cameras}](images/figures/nadir_mode_cameras.png)

nadir 모드는 특히 건물 모서리에 있는 정사영상의 품질을 실질적으로 향상시킬 수 있다.

![texturing-nadir-weight이 0(위) 및 32(아래)로 설정되었다. 건물 가장자리 근처에서 품질이 개선된 것을 볼 수 있다.
\label{fig:texturing_nadir_weight_comp}](images/figures/texturing_nadir_weight_comp.png)

이 옵션은 2.5D textured mesh(3D textured mesh가 아니다.)에만 영향을 미친다. 기본값인 16은 대부분의 데이터 세트에 적합하다. 
값이 높을수록 건물의 품질이 높아지지만, 정사영상 다른 영역(area)에서는 디테일하지 않을 수 있다.

## texturing-outlier-removal-type

항공사진이 항상 정적인 것은 아니다. 움직이는 물체(자동차, 자전거, 고양이 등)가 여러 장의 사진 사이에 보일 수 있다. 
그러나 물체가 이동하기 때문에 texturing 중에 textured mesh의 여러 부분에 나타날 수 있다.

![두 사진 사이에 보이는 움직이는 객체\label{fig:texturing_outlier_removal_comp}](images/figures/texturing_outlier_removal_comp.png)

이러한 현상을 방지하기 위해 두 개 이상의 이미지 간의 일관성을 검사하는 기능이 있다. 
불일치가 발견되면 이상치로 분류하고 제거한다. 이를 해결하는 다음의 두 가지 방법이 있다.

1.  gauss_clamping (default)

2.  gauss_damping

두 방법 모두 통계적 방법을 사용하여 이상치를 볼 수 있는 각 사진을 평가하는 데 기반한다. gauss_clamping은 강하게 적용하는 방법이다. 
이상치가 발생할 가능성이 높은 경우, 해당 이상치가 포함된 사진을 texturing 영역에 사용할 수 없도록 거부한다. 
반면, gauss_damping은 문제가 포함되는 사진의 선택 우선순위를 점진적으로 낮추는 완화한 접근법이므로 더 매끄러운 결과를 가져올 수 있다.

![Gauss Damping (위) vs. Gauss Clamping (아래). 카트의 일부가 상단 이미지에서는 누락되었다.
\label{fig:gauss_damping_vs_clamping}](images/figures/gauss_damping_vs_clamping.png)

## texturing-skip-global-seam-leveling

texturing 동안, 프로그램은 다른 빛 강도 등과 같은 다른 특성을 가지고 있는 이미지를 합쳐야 한다.

![빛 강도(상단) 및 전체 seam 레벨링 적용(하단)으로 인해 textured model의 seam
\label{fig:texturing_skip_global_seam}](images/figures/texturing_skip_global_seam.png)

이러한 종류의 seam blending은 모든 텍스처 패치에 대해 평가되기 때문에 *글로벌* 하다. 
패치 간의 차이를 최소화하여 전체적으로 일관된 휘도(luminosity)를 달성하는 것을 목적으로 둔다.

## texturing-skip-hole-filling

텍스처링 과정에서 mesh의 일부에 텍스처를 부여할 수 없다. 
입력 이미지 중 하나에 mesh의 특정 면을 할당할 수 있는 정보가 충분하지 않기 때문에 이러한 현상이 발생할 수 있다. 
이것을 완화시키기 위해, mesh의 작은 구멍은 근처 면의 texture로 보간함으로써 채워진다.

![작은 구멍(왼쪽)이 보간을 통해 채워졌다(오른쪽). 매끄러운 블러처리로 나타나는데, 작은 영역에는 눈에 잘 띄지 않는다.
\label{fig:texturing_skip_hole_filling}](images/figures/texturing_skip_hole_filling.png)

이 옵션은 구멍 채우기 기능(hole filling feature)을 비활성화한다(권장하지 않음).

## texturing-skip-local-seam-leveling

texturing할 때 다른 특성을 가진 영상(예: 다른 광도)을 병합해야 한다. 
global seam leveling 적용(**texting-skip-global-seam-leveling**에서 논의)은 텍스처 패치에서 
눈에 보이는 모든 seam을 제거하기 위해 필요하지만 종종 불충분한 단계다.

이러한 문제 때문에 모든 텍스처 패치(texture patch)에 국소화된 포아송 편집(두 개의 이미지를 혼합하는 방법)을 적용한다. 
이 방법은 런타임을 제어하기 위해 텍스처 패치 경계 주변의 로컬 버퍼에만 영향을 미치기 때문에 *지역적* 특성을 가지고 있다. 
이에 대한 더 자세한 논의는 이 장의 범위를 벗어나지만, Let There Be Color! Large-Scale Texturing of 3D Reconstructions[^reconstructions]
라는 논문의 *Poisson Editing* 섹션 안에 명확하게 설명되어 있다. 

![local seam leveling이 아닌 global seam leveling의 결과(위쪽)와 local seam leveling인 결과(아래쪽)\label{fig:texturing_skip_local_seam}](images/figures/texturing_skip_local_seam.png)

이 옵션은 local seam leveling을 비활성화한다(권장하지 않음).

## texturing-skip-visibility-test

texturing 과정에서 mesh 면의 가시성을 점검한다. 면과 카메라 사이에 장애물이 있는 경우(예: 건물) 해당 면은 특정 카메라에서 나오는 이미지 정보를 무시한다. 
일관성을 보장하는 좋은 방법으로 볼 수 있다.

![카메라 광선이 면에 투사된다(빨간 점). 건물에 의해 카메라가 인식할 수 없다. 가시성 테스트가 없다면, 건물 옥상으로부터 찍힌 이미지가 지상에 나타날 것이다.
\label{fig:texturing_skip_visibility_test}](images/figures/texturing_skip_visibility_test.png)

## texturing-tone-mapping

이 옵션은 때때로 textured mesh의 품질과 이에 따라 정사영상의 품질을 향상시키는 데 도움이 될 수 있다. 
이 옵션을 **gamma**로 설정할 때 프로그램은 local 및 global seam leveling을 적용하기 전에 감마 보정을 textured map에 적용한다. 
감마보정은 영상에서 휘도 값을 매핑하기 위한 방법(자세한 내용은 <https://www.cambridgeincolour.com/tutorials/gamma-correction.htm>
에서 확인할 수 있다.)이다. texturing의 맥락에서 감마 보정을 사용하면 보다 생생한 결과를 얻을 수 있다.

![원본 입력 영상 (좌측), 감마 보정된 영상 (우측)\label{fig:texturing_tone_mapping}](images/figures/texturing_tone_mapping.jpg)

## time

파이프라인의 각 단계를 처리하는데 걸린 시간을 보여주는 프로젝트 디렉토리에 저장된 *benchmark.txt* 을 생성한다.
성능 측정에 유용하다. 디폴트로 벤치마크 파일은 생성되지 않는다.

## use-3dmesh

디폴트로 2.5D textured mesh를 사용하여 정사영상을 렌더링한다. 2.5D mesh는 대부분의 항공 데이터셋에서 잘 동작하는 경향이 있지만, 
특히 지상과 거의 수직으로 촬영된 nadir 영상이 없는 경우, 보통 수준 이하의 결과를 얻을 수 있다. 
그 이유는 texturing 단계가 3D mesh와 2.5D mesh 사이에서 다르게 수행되기 때문이다. 
2.5D mesh는 nadir 영상에 우선순위를 부여하며, 만약 이것이 누락된다면 texturing은 3D mesh에 비해 품질이 떨어질 수 있다. 
경사 이미지로 근거리의 단일 건물을 선회한 영상의 경우, 2.5D mesh도 성능이 떨어진다. 
이 옵션은 프로그램에 전체 3D 모델을 사용하여 정사영상을 생성하고, 2.5D 모델 생성을 건너뛰도록 지시한다 (**skip-3dmodel** 참조).

## use-exif

프로젝트 디렉터리(ODM)에 만약 *gcp\_list.txt* 파일이 존재하거나 GCP파일이 데이터셋 (WebODM)과 함께 업로드 된다면, 
Ground Control Pint 파일을 사용한다. 
이 옵션을 키면 프로그램은 GCP파일을 이용하지 않고, 영상의 EXIF 태그의 위치 정보를 이용한다.

## use-fixed-camera-params

SFM 프로세스 동안 카메라의 내부 파라미터를 추정 및 개선해야 한다. 때때로, 이미지 수집 단계가 잘 수행되지 않거나 혹은 과도한 렌즈 왜곡 때문에
카메라 파라미터가 잘못 추정되고 *doming* 현상이 나타나는 reconstruction 결과를 얻을 수 있다. 
doming 현상에 대한 해결책은 *카메라 캘리브레이션* 장과 **카메라** 옵션을 통해서도 설명된다. 
또한 이 옵션을 켜서 소프트웨어에 카메라 파라미터를 전혀 최적화하지 않도록 지시할 수 있다(파라미터 고정). 
영상의 기하학적 왜곡이 거의 또는 전혀 없으며 이미지의 EXIF 태그에 포함된 초점 길이 값이 정확하다면, 결과가 개선될 수도 있다.

## use-hybrid-bundle-adjustment

번들 조정(bundle adjustment; BA)은 카메라, 3D 포인트 및 카메라 파라미터의 위치를 개선하는 SFM 프로세스 중의 정교화 단계다. 
이 프로세스는 reconstruction 과정에서 오류가 누적되는 것을 피하기 위해 정기적으로 실시해야 하지만 계산 비용이 크다. 
지역적인 것과 전역적인 것의 두 가지 종류로 나뉜다. 로컬 번들 조정은 reconstruction 일부만 개선하고, 글로벌 번들 조정은 전체를 개선한다. 
글로벌 번들 조정을 수행하려면 전체 장면을 재평가해야 하므로 속도가 느리다. 디폴트로는 전체 영상의 20%가 증가하여 새로운 삼각 측량 후에만 
글로벌 BA를 수행한다. 또 3단계(영상 30장) 이내의 카메라를 비교해 새로운 카메라가 추가될 때마다 지역적 번들 조정을 수행한다.

![Local Bundle Adjustment with 3 (좌측) and 1 (우측) levels of
connectivity. 회색 카메라는 (30까지) 로컬 BA 계산에 사용된다. 
Camera icon by FontAwesome CC BY 4.0\label{fig:local_bundle_adjustment}](images/figures/local_bundle_adjustment.png)

이 옵션을 켜면 로컬 및 글로벌 번들 조정을 작동하는 로직을 변경한다. 
첫째, 복원에 새로 추가된 지점의 수와 관계없이 100장의 영상이 추가될 때마다 글로벌 번들 조정이 수행된다. 
둘째, 로컬 번들조정을 수행할 때 프로그램은 3이 아닌 1단계 연결성 안에 있는 영상만을 인식한다. 
다시 말하면, 이 옵션은 글로벌 번들 조정의 수행 횟수는 늘리되 로컬 번들 조정 횟수는 줄인다.

![글로벌 BA 실행 추정치\label{fig:global_ba_estimate}](images/figures/global_ba_estimate.png)

![로컬 BA 실행 추정치\label{fig:local_ba_estimate}](images/figures/local_ba_estimate.png)

작은 데이터 세트(\< 1000개의 이미지)의 경우 큰 차이가 없다. 
이미지 수가 증가함에 따라, 더 많은 로컬 번들 조정 계산 비용이 더 많은 글로벌 번들 조정 계산 비용을 초과하기 시작한다. 
큰 데이터 세트의 경우 이 옵션을 설정하면 총 실행 시간이 단축될 수 있다. 
글로벌 번들 조정이 더 자주 이루어지기 때문에 reconstruction의 정확성도 높일 수 있다.

## use-opensfm-dense

이 프로그램에는 dense cloud point 생성을 위한 두 가지 옵션이 있다.

1.  MVE (default)

2.  OpenSfM

기본적으로 MVE(Multi-View Environment)가 사용된다.
이걸 바꿈으로써 옵션 OpenSfM의 depthmap reconstruction 알고리즘이 대신 사용된다.

## verbose

이 옵션은 더 많은 내용의 메시지를 전달하도록 한다. 
이 옵션을 활성화하면 처리 출력에 가능한 문제를 진단하는 데 사용할 수 있는 더 많은 메시지를 포함하도록 한다.

## version

이 옵션은 ODM버전 번호를 출력하고 종료하도록 한다.

[^qgis]: QGIS: <https://qgis.org>
[^points2grid]: Points2Grid: A Local Gridding Method for DEM Generation from Lidar Point Cloud Data <https://opentopography.org/otsoftware/points2grid>
[^filter]: smrf: A Simple Morphological Filter for Ground Identification of LIDAR Data. <http://tpingel.org/code/smrf/smrf.html>
[^reconstruction]: Screened Poisson Reconstruction: watertight surfaces from oriented point sets. <http://www.cs.jhu.edu/~misha/MyPapers/ToG13.pdf>
[^scenes]: Accurate Multiple View 3D Reconstruction Using Patch-Based Stereo for Large-Scale Scenes. <http://www.nlpr.ia.ac.cn/2013papers/gjkw/gk11.pdf>
[^values]: LAS 1.4 Specification: <https://www.asprs.org/wp-content/uploads/2010/12/LAS_1_4_r13.pdf>
[^filter]: smrf: A Simple Morphological Filter for Ground Identification of LIDAR Data. <http://tpingel.org/code/smrf/smrf.html>
[^cloudcompare]: CloudCompare: <http://www.cloudcompare.org>
[^methods]: CloudCompare CANUPO Plugin: <http://www.cloudcompare.org/doc/wiki/index.php?title=CANUPO_(plugin)>
[^format]: Entwine Point Tile: <https://entwine.io/entwine-point-tile.html>
[^pdal]: PDAL: <https://pdal.io>
[^reconstructions]: Let There Be Color! Large-Scale Texturing of 3D Reconstructions: <https://www.gcc.tu-darmstadt.de/media/gcc/papers/Waechter-2014-LTB.pdf>
