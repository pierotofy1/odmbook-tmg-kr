# Processing Large Datasets
\label{processing_large_datasets}

ODM can use a lot of memory while processing. These memory requirements
increase almost proportionally with the number of images and their
resolution. Twice the images? Need twice the RAM! Of course at some
point you might encounter a very large dataset made of dozens of
thousands of images, but don’t have 800GB of RAM just lying around.

ODM은 처리하는 동안 많은 메모리를 사용할 수 있다. 메모리 요구 사항은 이미지 수와 해상도에 거의 비례하여 증가한다. 이미지의 수가 2배 라면? RAM이 2배 필요하다! 물론 어느 순간 수십만 개의 이미지로 이루어진 매우 큰 데이터 셋을 마주할 수 있지만, 800GB의 RAM이 그냥 놓여있는 것은 아니다.

This is were a nice feature called **split-merge** comes into play. This
feature splits a large dataset into smaller, manageable parts called
submodels that have some overlap between them. Each submodel is
processed independently, either one at a time on a single machine (local
split-merge) or even in parallel on multiple machines (distributed
split-merge)! Once all submodels have been processed, the results are
merged back together into a single consistent model. With this approach
people can process much larger datasets using much less powerful
computers. Enabling split-merge changes the ODM execution pipeline:

이것은 **split-merge**라고 불리는 멋진 기능이다. 이 기능은 큰 데이터 셋을 submodel이라고 하는 더 작고 관리하기 쉬운 부분으로 나눈다. Submodel 끼리는 일부 중복된다. 각 submodel은 단일 시스템 (local split-merge)에서 한 번에 하나씩 또는 여러 시스템 (distributed split-merge)에서 동시에 독립적으로 처리된다! 모든 submodel이 처리되면 결과는 다시 하나의 일관된 모델로 병합된다. 이러한 접근법으로 사람들은 훨씬 적은 컴퓨팅 파워로 훨씬 더 큰 데이터 셋을 처리할 수 있다. Split-merge을 사용하도록 설정하면 ODM 실행 파이프라인이 변경된다.


![Split-merge pipeline. Step 2 and 4 of the split section can be
performed in parallel on separate machines when using distributed
split-merge\label{fig:split_merge_pipeline}](images/figures/split_merge_pipeline.png)

Split-merge 파이프라인. split 섹션의 2, 4단계는 distributed split-merge을 이용할 때 각각의 시스템에서 병렬로 수행될 수 있다.


## Split-Merge Options

### split

Split-merge can be used either from the command line (ODM) or from
WebODM and is turned on/off by setting the **split** option. Split-merge
will be turned on anytime the following condition is true:

Split-merge는 명령 줄 (ODM) 또는 WebODM에서 사용할 수 있으며 **split** 옵션을 설정해 켜거나 끈다. 다음 조건에 해당 할 때마다 split-merge가 설정된다.

    Split Option < Number of Images

This option specifies the **average** number
of images that should be included in each submodel. Note that this does
not guarantee that your submodels will have this exact amount of images.
In fact, some submodels might end up having twice as many images as
others. It just means that the following expression holds true:

이 옵션은 각 submodel에 포함돼야 하는 **평균(average)** 이미지 수를 지정한다. 이 옵션이 submodel이 정확히 해당 양의 이미지를 가진다고 보장하지는 않는다. 실제로 일부 submodel은 다른 submodel보다 2배 더 많은 이미지를 가질 수 있다. 단지 다음 표현이 참임을 의미한다.

    (Sum of submodel images) / (number of submodels) =~ Split Value
    
In plain words: The sum of all submodel images divided by the number of submodels is approximately equal to the split value.

모든 submodel 이미지의 합을 submodel 수로 나눈 값은 split 값과 거의 같다.

### split-overlap

In order to align and merge results, each submodel must be reconstructed
with a certain amount of overlap and redundancy with other submodels.

결과를 정렬(align)하고 병합(merge)하기 위해 각 submodel을 다른 submodel과 일정량의 중복을 가지고 재구성해야한다.

![Overlap area between two submodels\label{fig:overlap_areas}](images/figures/overlap_areas.png)

두 submodel간의 중복 영역

The amount of overlap in meters is specified with this option. Datasets
captured at higher altitudes should use a larger value, while those
captured at lower altitudes can use lower values. More overlap will
significantly slow down computation because of the redundancy of
processed data, but can help achieve better model alignment during the
merge step. If the resulting DEMs and point clouds from different
submodels show big gaps in elevation, try increasing the overlap.

이 옵션으로 미터 단위의 중복 양을 지정한다. 더 높은 고도에서 촬영된 데이터셋은 더 큰 값을, 더 낮은 고도에서 촬영된 데이터 셋은 더 낮은 값을 사용할 수 있다. 중복도가 높을수록 처리된 데이터의 중복성으로 계산 속도가 크게 느려지지만 병합 단계에서 더 나은 모델 정렬을 얻을 수 있다. 다른 submodel의 DEM과 포인트 클라우드의 고도 차가 큰 경우에는 중복도를 중복도를 높인다.

### sm-cluster

This option enables distributed split-merge by specifying a URL to a
ClusterODM instance. The process of setting up ClusterODM is described
later in the *Distributed Split-Merge* section of this chapter.

이 옵션은 ClusterODM 인스턴스에 URL을 지정하여 distributed split-merge을 활성화한다. ClusterODM 설정 프로세스는 이 장의 *Distributed Split-Merge* 에서 나중에 설명한다.

### merge

After splitting and computing each individual submodel, results need to
be merged back together. By default all outputs (point clouds, DEMs and
orthophotos) are merged. A user can use this option to specify that only
a particular type of output should be merged. Valid options are:

각 개별 submodel을 분할하고 계산한 후, 결과를 다시 병합해야한다. 기본적으로 모든 결과물(포인트 클라우드, DEM, 정사영상)이 병합된다. 사용자는 이 옵션을 사용하여 특정 유형의 결과물만 병합하도록 지정할 수 있다. 유효한 옵션은 다음과 같다.

  - all

  - pointcloud

  - orthophoto

  - dem

## Local Split-Merge

It’s really easy to use local split-merge. Simply pass the **split**
option and **split-overlap** options:

Local split-merge을 사용하는 것은 정말 쉽다. 간단히 **split** 옵션과 **split-overlap** 옵션을 입력한다.

    $ docker run -ti --rm -v //d/odmbook/project:/datasets/code opendronemap/odm --project-path /datasets --split 100 --split-overlap 75

In the example above, submodels will be stored in
**D:\textbackslash odmbook\textbackslash project\textbackslash submodels\textbackslash** while the merged results will be
stored in the usual folders.

위의 예에서 submodel은 **D:\textbackslash odmbook\textbackslash project\textbackslash submodels\textbackslash**에 저장되고, 병합된 결과는 일반적인 폴더에 저장된다.

Each submodel folder (**submodels\textbackslash submodel\_xxxx**) is itself a valid
OpenDroneMap project. So if a submodel fails to process, you can re-run
the individual submodel to isolate potential issues by running:

각 submodel 폴더(**submodels\textbackslash submodel\_xxxx**)는 그 자체로 유효한 OpenDroneMap 프로젝트다. 따라서 submodel을 처리하지 못하면, 개별 submodel을 다시 실행하여 잠재적 문제를 분리할 수 있다.

    $ docker run -ti --rm -v //d/odmbook/project:/datasets/code opendronemap/odm --project-path /datasets/code/submodels --orthophoto-cutline --dem-euclidean-map submodel_xxxx

The **orthophoto-cutline** and **dem-euclidean-map** options are always
required for the purpose of merging DEMs and orthophotos. If an error
occurs while processing one of the submodels, the process will stop.
After fixing the problem, you can resume by re-running the initial
command:

**orthophoto-cutline**과 **dem-euclidean-map** 옵션은 DEM과 정사영상을 병합하기 위해 항상 필요하다. Submodel 중 하나를 처리하는 동안 오류가 발생하면 프로세스가 중지된다. 문제를 해결한 후 초기 명령을 다시 실행하여 재개 할 수 있다.

    $ docker run -ti --rm -v //d/odmbook/project:/datasets/code opendronemap/odm --project-path /datasets --split 100 --split-overlap 75 

Submodels that correctly processed the first time will
**not** be re-processed, unless the
**rerun-from split** option is passed. Execution will resume from the
failed submodel.

**rerun-from split** 옵션을 입력하지 않으면 처음에 올바르게 처리된 submodel은 다시 처리되지 **않는다**. 실패한 submodel에서 부터 다시 실행된다.

If a task fails at the merge step, after all submodels have finished
processing, you can check for issues and resume the merge step by
typing:

병합 단계에서 작업이 실패하면, 모든 submodel의 처리가 완료된 후 다음 명령어를 입력하여 문제를 확인하고 병합 단계를 다시 시작할 수 있다.

    $ docker run -ti --rm -v //d/odmbook/project:/datasets/code opendronemap/odm --project-path /datasets --split 100 --split-overlap 75 --rerun merge --merge all

## Distributed Split-Merge

Distributed split-merge works just like local split-merge, but can be
much faster, as submodels are processed independently in parallel by
many machines. All that is required is to setup an instance of
ClusterODM and link some NodeODM nodes to it.

Distributed split-merge은 local split-merge과 동일하게 작동하지만, submodel이 여러 시스템에 의해 독립적으로 병렬 처리되므로 훨씬 빠르다. ClusterODM의 인스턴스를 설정하고 일부 NodeODM 노드를 인스턴스에 연결하는 과정이 필요하다.

### ClusterODM

ClusterODM is a program to connect together NodeODM API compatible
nodes. It allows for tasks to be distributed across multiple nodes while
taking into consideration factors such as maximum number of images,
queue size and slot availability. It can also automatically spin up/down
nodes based on demand using cloud computing providers (at the time of
writing only Digital Ocean and Amazon Web Services are supported, but
more are on the roadmap).

ClusterODM은 NodeODM API 호환 노드를 함께 연결하는 프로그램이니다. 최대 이미지 수, 대기열 크기 및 슬롯 가용성과 같은 요소를 고려하면서 여러 노드에 작업을 분산시킬 수 있다. 또한 클라우드 컴퓨팅 제공 업체를 사용해 수요에 따라 노드를 자동으로 스핀 업/다운 할 수 있다(작성 당시에는 Digital Ocean과 Amazon Web Services만 지원되지만 더 많은 업체가 고려되고 있다).

A ClusterODM looks like a normal NodeODM node from the outside and can
operate with any tool that also works with NodeODM. To start ClusterODM,
simply type:

ClusterODM은 외부에서 일반적인 NodeODM 노드처럼 보이고 NodeODM과 함께 작동하는 모든 도구로 작동 할 수 있다. ClusterODM을 시작하려면 다음을 입력하면 된다.

    $ git clone https://github.com/OpenDroneMap/ClusterODM
    $ cd ClusterODM
    $ docker-compose up

If you open your browser to <http://localhost:10000> you should be
greeted with:

<http://localhost:10000>로 브라우저를 열면 다음과 같은 메시지가 표시된다.

![ClusterODM web admin page\label{fig:clusterodm_admin_page}](images/figures/clusterodm_admin_page.png)

ClusterDOM 웹 관리자 페이지

ClusterODM has setup a default NodeODM node on the same machine. Let’s
add some more nodes. If you have another computer with docker installed,
you can run:

ClusterODM은 동일한 시스템에 기본 NodeODM 노드를 설정한다. 노드를 더 추가해보자. 도커가 설치된 다른 컴퓨터가 있는 경우, 다음을 실행할 수 있습니다.

    $ docker run -d -p 3000:3000 opendronemap/nodeodm

which will launch a new instance of NodeODM on port 3000. Now it’s time
to connect our new NodeODM node to ClusterODM. For that we’ll need to
use an archaic but functional tool called **telnet.** On Linux and macOS
this tool is usually installed by default (if it isn’t, Google how to
install it). On Windows you usually need to enable it. From an elevated
shell (right-click **Git Bash** and select **Run As Administrator**)
type:

포트 3000에서 NodeODM의 새 인스턴스를 시작한다. 이제 새 NodeODM 노드를 ClusterODM에 연결할 차례다. 이를 위해 **텔넷(telnet)**이라는 오래됐지만 기능적인 도구를 사용해야한다. Linux와 macOS에서 텔넷은 보통 기본적으로 설치돼 있다(그렇지 않으면 설치 방법을 검색해보면 된다). Windows에서는 일반적으로 활성화해야 한다(**Git Bash**를 우클릭하고 **관리자 권한으로 실행**을 클릭).

    $ pkgmgr /iu:"TelnetClient" 

Then restart the shell. Once **telnet** is available, type:

다음에 쉘을 재시작한다. **텔넷**이 사용가능해지면 다음을 입력한다.

    $ telnet localhost 8080
    Connected to ...
    Escape character is '^]'.
    ...
    #

Typing **HELP** will show you all available commands. To add a NodeODM
node use the **NODE ADD** command:

**HELP**를 입력하면 사용가능한 명령어를 확인할 수 있다. NodeODM 노드를 추가하기 위해 **NODE ADD** 명령어를 사용한다.

    # NODE ADD ipofmachine 3000
    # NODE LIST
    1) nodeodm-1:3000 [online] [0/2] <version 1.5.1>
    2) ipofmachine:3000 [online] [0/2] <version 1.5.1>

You’ll need to change **ipofmachine** with the IP address or hostname of
the computer running NodeODM.

**ipofmachine**을 NodeODM을 실행하는 컴퓨터의 IP 주소나 호스트 이름으로 변경해야 한다.

To verify that things are working you can now open
<http://localhost:4000>. If things are working you should see the
NodeODM web interface. This means ClusterODM is properly forwarding
requests to the NodeODM nodes.

작동하는지 확인하기 위해 <http://localhost:4000>를 열어본다. 작동한다면 NodeODM 웹 인터페이스가 표시되어야 한다. 이는 ClusterODM이 NodeODM 노드로 요청을 올바르게 전달하고 있음을 의미한다.

You can connect as many nodes as you want to ClusterODM. If you have a
variety of computers, some more powerful than others, you can start
NodeODM instances with the **max\_images** option:

ClusterODM에 원하는만큼 노드를 연결할 수 있다. 다양한 컴퓨터가 있고 다른 컴퓨터보다 강력한 컴퓨터가 있는 경우, **max\ _images** 옵션을 사용하여 NodeODM 인스턴스를 시작할 수 있다.

    $ docker run -d -p 3000:3000 opendronemap/nodeodm --max_images 300

This way NodeODM will be instructed to process datasets only up to 300
images. ClusterODM will take this factor into consideration when
processing new tasks and will forward the task to the first machine
capable of handling it.

이러한 방식으로 NodeODM이 데이터 셋을 최대 300장의 이미지까지 처리하도록 할 수 있다. ClusterODM은 새 작업을 처리 할 때 이 요소를 고려하여, 해당 작업을 처리 할 수 있는 첫 번째 시스템으로 이를 전달한다.

A useful command is **NODE LOCK** which will prevent a certain node from
being used by ClusterODM:

특정 노드가 ClusterODM에 의해 사용되지 못하도록 하는 **NODE LOCK**이라는 유용한 명령어가 있다.

    # NODE LOCK 1
    # NODE LIST
    1) nodeodm-1:3000 [online] [0/2] <version 1.5.1> [L]
    2) ipofmachine:3000 [online] [0/2] <version 1.5.1>

In the setup above, all tasks will be forwarded to machine 2. Nodes that
are locked can be unlocked with **NODE UNLOCK**.

위 설정에서 모든 작업이 시스템 2로 전달된다. 잠긴 노드는 **NODE UNLOCK**으로 잠금을 해제 할 수 있다.

ClusterODM can also use cloud computing providers such as Digital Ocean
to spin up/down computing instances on demand. Because this feature is
relatively new, check the ClusterODM README[^readme] for the latest
instructions on how to configure it.

ClusterODM은 Digital Ocean과 같은 클라우드 컴퓨팅 제공 업체를 사용하여 필요할 때 컴퓨팅 인스턴스를 스핀 업/다운 할 수 있다. 이 기능은 비교적 새로운 기능이므로 ClusterODM README[^readme]에서 설정 방법에 대한 최신 지침을 확인하면 된다.

### Distributed Run

Now that ClusterODM is setup, running distributed split-merge is the
same as running local split-merge, except that we pass an additional
**sm-cluster** option that specifies the URL to a ClusterODM instance.

ClusterODM이 설정되었으므로 distributed split-merge 실행은 URL을 ClusterODM 인스턴스에 지정하는 추가 **sm-cluster** 옵션을 전달한다는 점을 제외하고 local split-merge 실행과 동일하다.

    $ docker run -ti --rm -v //d/odmbook/project:/datasets/code opendronemap/odm --project-path /datasets --split 100 --split-overlap 75 --sm-cluster http://ipofclusterodm:4000

There’s a small gotcha, if you are running this command on the same
machine as ClusterODM: **ipofclusterodm** cannot be **localhost**,
because **localhost** refers to the **opendronemap/odm** container and
not your machine. To find out the correct IP address, run:

ClusterODM과 동일한 시스템에서 이 명령을 실행하면 작은 문제가 있다. **localhost**는 시스템이 아닌 **opendronemap/odm** 컨테이너를 참조하므로 **ipofclusterodm**은 **localhost**가 될 수 없다. 올바른 IP 주소를 찾으려면 다음을 실행하고,

    $ docker ps
    
    CONTAINER ID        IMAGE                             COMMAND                  CREATED             STATUS              PORTS                     NAMES
    5a86a35fe643        opendronemap/clusterodm              "/usr/bin/nodejs /va..."   4 days ago          Up 2 hours          0.0.0.0:4000->3000/tcp   node-odm-1
    
    $ docker inspect -f "{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}" 5a86
    
    172.23.0.5

and use that IP instead. In the example above, we can then use
**--sm-cluster http://172.23.0.5:4000**.

이 IP를 대신 사용한다. 위의 예에서 **--sm-cluster http://172.23.0.5:4000** 를 사용할 수 있다.

## Using Image Groups and GCPs

You can control how a dataset should be split by placing a
**image\_groups.txt** file in your project folder. For example, if your
images are in **D:\textbackslash odmbook\textbackslash project\textbackslash images**, you can create a
**D:\textbackslash odmbook\textbackslash project\textbackslash image\_groups.txt** file with the following
content:

프로젝트 폴더에 **image\_groups.txt** 파일을 위치시켜 데이터 셋을 분할하는 방법을 조절할 수 있다. 예를 들어, 이미지가 **D:\textbackslash odmbook\textbackslash project\textbackslash images**에 있는 경우 **D:\textbackslash odmbook\textbackslash project\textbackslash image\ _groups.txt** 파일을 다음 내용으로 만들 수 있다.

    1.JPG A
    2.JPG A
    3.JPG B
    4.JPG B
    5.JPG C
    [...]

where the items on the left are image names and items on the right
represent submodel groups. If you run out of letters simply use *AA*,
*BB*, etc. The file is case-sensitive so uppercase and lowercase letters
are treated differently.

여기서 왼쪽의 항목은 이미지 이름이고 오른쪽의 항목은 submodel 그룹을 나타낸다. 문자가 부족한 경우 *AA*, *BB* 등을 사용하면 된다. 파일은 대소문자를 구분하므로 대문자와 소문자가 다르게 취급된다.

If this file is detected during split-merge, the **split** value will be
ignored and the dataset will be split according to the rules specified
in the **image\_groups.txt** file. You will still need to pass **split**
to enable the split-merge workflow.

Split-merge 중에 이 파일이 발견되면, **split** 값이 무시되고 **image\_groups.txt** 파일에 지정된 규칙에 따라 데이터 셋이 분할된다. Split-merge 작업 흐름을 활성화하려면 여전히 **split**은 전달해야 한다.

At the time of writing, image groups are currently not supported in
WebODM.

작성 당시 이미지 그룹은 현재 WebODM에서 지원되지 않는다.

Image groups are important when using GCPs. GCPs can be used with
split-merge, but care should be exercised to make sure there are at
least 3 GCPs that fall within each submodel. If less than 3 GCPs are
present in a submodel, the submodel will be aligned with the GPS
information from EXIF data and by looking at the position of other
submodels, but won’t be as accurate.

이미지 그룹은 GCP를 사용할 때 중요하다. GCP는 split-merge와 함께 사용할 수 있지만 각 submodel에 속하는 GCP가 3개 이상 존재하는지 확인하기 위해 주의를 기울여야 한다. 하나의 submodel에 3개 미만의 GCP가 있는 경우에는, submodel은 EXIF 데이터의 GPS 정보와 다른 submodel과의 관계를 통해 정렬되지만 정확하지는 않다.

You can use GCPs with split-merge just like you would use GCPs for a
normal run.

일반 실행에 GCP를 사용하는 것처럼 GCP를 split-merge과 함께 사용할 수 있다.

## Limitations

With split-merge you get point clouds, DEMs and orthophotos, but not 3D
textured meshes. You can still access the individual 3D textured meshes
from each submodel, but no *global* 3D textured mesh is generated.

Split-merge를 사용하면 포인트 클라우드, DEM, 정사영상을 얻을 수 있지만 3차원 텍스쳐 메쉬(3D textured meshes)는 얻을 수 없다. 각 submodel에서 개별 3차원 텍스쳐 메쉬에 계속 접근할 수 있지만 *전체(global)* 3차원 텍스처 메쉬는 생성되지 않는다.

All of the problems listed in the *Camera Calibration* chapter are
magnified when using split-merge. It’s imperative to follow the best
practices for data acquisition and if possible, to use a well calibrated
camera model.

Split-merge을 사용하면 *카메라 캘리브레이션*장에 나열된 모든 문제가 확대된다. 데이터 취득의 모범 사례(best practices)를 따라야 하고, 가능하면 보정이 잘된 카메라 모델을 사용해야한다.

While the longest parts of processing can be parallelized in distributed
split-merge, a dataset still needs to be split, aligned and merged on a
single machine. The merge step in particular, for really large datasets,
could still make the machine run out of memory. Choose
**orthophoto-resolution**, **dem-resolution** and
**depthmap-resolution** parameters conservatively. Use the most powerful
machine you have available to start a distributed split-merge task.

Distributed split-merge에서 가장 긴 처리 부분을 병렬화 할 수 있지만 단일 시스템에서 데이터 셋을 분할, 정렬, 병합해야한다. 특히 대용량 데이터 집합의 경우, 병합 단계에서 시스템 메모리가 부족해질 수 있다. **orthophoto-resolution**, **dem-resolution**, **depthmap-resolution** 매개 변수를 안전하게 선택해야 한다. Distributed split-merge을 시작하기 위해 사용할 수 있는 가장 강력한 시스템을 사용하는 것이 좋다.

[^readme]: ClusterODM: <https://github.com/OpenDroneMap/ClusterODM>