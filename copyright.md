# Copyright

First published by MasseranoLabs LLC 2019

Copyright © 2019 by Piero Toffanin

All rights reserved. No part of this publication may be reproduced, stored
or transmitted in any form or by any means, electronic, mechanical,
photocopying, recording, scanning, or otherwise without written
permission from the publisher. It is illegal to copy this book, post it to a
website, or distribute it by any other means without permission.

Piero Toffanin asserts the moral right to be identified as the author of this
work.

Piero Toffanin has no responsibility for the persistence or accuracy of
URLs for external or third-party Internet Websites referred to in this
publication and does not guarantee that any content on such Websites is, or
will remain, accurate or appropriate.

Designations used by companies to distinguish their products are often
claimed as trademarks. All brand names and product names used in this
book and on its cover are trade names, service marks, trademarks and
registered trademarks of their respective owners. The publishers and the
book are not associated with any product or vendor mentioned in this book.
None of the companies referenced within the book have endorsed the book.

Trademarks: OpenDroneMap and the OpenDroneMap logo are
trademarks or registered trademarks of Cleveland Metroparks and/or its
affiliates and may not be used without written permission. MasseranoLabs
is not associated with Cleveland Metroparks. Cleveland Metroparks has
not endorsed this book.

First edition

Cover art by Piero Toffanin

Proofreading by Danielle Y. Toffanin
