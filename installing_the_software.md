# 소프트웨어 설치하기
\label{installing_the_software}

최근까지 오픈드론맵은 ODM 프로젝트라는 단일 명령 어플리케이션을 지칭하기 위한 용어로 사용됐다. 그러나 최근 웹 인터페이스, API 외 툴(Tool)들의 발달에 따라 오픈드론맵은 항공 데이터를 
처리, 분석, 시각화하기 위한 다양한 어플리케이션 '집합체'로 거듭나게 되었다. 오픈드론 맵의 구성요소는 다음과 같다.

  - **ODM** ODM은 커맨드라인으로 작동시킬 수 있는 프로세스 엔진이다. ODM은 영상을 입력으로 받으며, 포인트클라우드, 3차원 모델, 정사영상과 같은 다양한 결과물들을 생성한다.

  - **NodeODM** NodeODM은 ODM에 탑재되어 있는 API다. 사용자와 어플리케이션은 NodeODM을 이용하여 컴퓨터 네트워크를 통해 ODM의 다양한 기능을 이용할 수 있다.
    
  - **WebODM** WebODM은 사용자 친화적 인터페이스다. 지도 및 3차원 뷰어, 로그인, 플러그인 시스템 등 최근 드론 맵핑 플랫폼에서 요구되는 다양한 기능들을 포함하고 있다.
  
  - **CloneODM** CloneODM은 NodeODM API를 이용하여 ODM과 소통할 수 있는 커맨드라인이다.

  - **PyODM** PyODM은 NodeODM API를 통해 작업을 생성하기 위한 파이썬 SDK다. 위와 관련된 내용은 *파이썬으로 자동화 처리하기*챕터에서 자세히 다루도록 하겠다.

  - **ClusterODM** ClusterODM은 여러개의 NodeODM 인스턴스를 연결하기 위한 "load balancer"다. 위와 관련한 내용은 *대량 데이터 처리* 챕터에서 다루도록 하겠다.

필자는 실질적이고 점진적인 학습 접근 방식을 지향한다. 따라서 독자여러분들이 쉽게 접근하실 수 있도록, 먼저 WebODM의 설치와 사용법을 설명하고 이를 바탕으로 ODM와 NodeODM을 설치하고자 한다. 먼저
WebODM을 설치하기 위해선, 간단한 명령어를 사용하는 방법부터 익힐 필요가 있다. 명령어와 관련된 심화내용은 파트 III에서 다룰이다. 이 단계에 이르게 되면, 독자분들께서는 본 프로그램의 핵심 개념에 익숙해질
것이며, 학습 속도 또한 빨라질 것이다. 만일 본 프로그램들의 설치에 익숙하신 독자들이라면, 해당 챕터는 생략해도 좋다.

ODM, NodeODM, WebODM을 Window, MacOS, Linux와 같은 주요 플랫폼에서 실행하기 위해서는 Docker가 필요하다. Docker는 프로그램을 실행하기 위한 시스템과 해당 프로그램 및 그 하위 프로그램들의 패키지화된 사본인 *컨테이너*
를 실행하기 위한 도구이며, 이러한 컨테이너들은 가상 환경에서 실행된다. Linux 상에서 이러한 가상환경은 운영체제에서 실행할 수 있기 때문에 매우 효율적이다. MacOS와 Window의 경우 가상화 머신(Virtual Machine, VMs)을 이용하여
컨테이너를 실행해야 하는데, 이때 약간의 오버헤드가 있을 수 있다. 일단 설치가 완료되면, WebODM만을 실행하기 위한 수단으로만 사용하므로, 사용자들은 Docker에 대해서 신경쓸 필요가 없다. Docker 사용에 익숙한 사용자라면 
*Docker Essentials* 챕터를 참고하기 바란다.

Docker는 Linux 이외의 플랫폼에서 설치 및 구성에 다소 어려움이 있기 때문에 Docker를 사용하는 것에 대해 회의적인 의견들을 받는 경우가 자주 있다. ODM은 복잡한 소프트웨어다. 많은 하위 프로그램들이 필요하며 이 중 몇 가지는
Window에서 실행하는 것 자체가 불가능한 경우도 있다. 따라서 Docker를 사용하지 않으면, ODM을 Window와 MacOS에서 실행하는 것은 불가능하다. 현재 Linux용 Window(WLS) 활용 및 MacOS를 위한 하위 프로그램들의 기본 포트에 초점을
맞춰 다양한 노력들이 이뤄지고 있으며, 이를 통해 좀 더 쉬운 설치가 가능하도록 할 예정이다. 그러나 당분간은 Docker를 이용하는 것이 유일한 수단이다.

Ubunut Linux 16.04에서는 모든 오픈드론맵 소프트웨어를 설치 및 실행이 가능하다. 해당 Linux 버전에서 도커를 실행할 경우 성능에 거의 문제가 없고, 해당 플랫폼에 ODM을 설치하는 것 또한 간편하다. 그러나 필자의 경우 해당 버전을
추천하지는 않는다. Unbuntu Linux 16.04 보다는 Linux 상에서 컨테이너화(containerization)의 이점이 약간의 퍼포먼스 성능저하를 훨씬 상회하기 때문이다. Docker를 사용하면, 간편한 소프트웨어 업데이트 또한 가능하다.

## 하드웨어 사양

오픈드론맵을 실행하기 위한 최소한의 사양은 다음과 같다.

  - 2010년 이후 버전 또는 64비트 CPU

  - 20 GB 디스크 여유 공간
  
  - 4 GB 램

100-200장 정도의 영상의 경우 위에서 언급한 사양으로는 처리할 수 없다. ODM 설치를 위한 권장 사양은 다음과 같다.

  - 최신 세대 CPU
  
  - 100 GB 디스크 여유 공간
  
  - 16 GB 램

권장사항 보다 많은 코어를 탑재한 CPU일수록 더 효율적인 처리가 가능하다. 다만, 현재 아쉽게도 GPU를 이용한 처리는 지원하지 않는다. 더 많은 영상을 처리하기 위해선 처리하고자 하는 만큼의 영상에 선형비례하여 더 많은 디스크 여유 
공간과 RAM이 필요하다.

## Window에 설치하기

오픈드론맵을 실행하기 위해서는 최소한 Window 7이상의 버전이 필요하다. 이전 버전은 지원하지 않는다.

### Step 1. 가상화 지원여부 확인

윈도우에서 Docker를 실행하기 위해 VMs을 실행해야 하는데, VMs를 실행가이 위해서는 가상화 기능이 필요하다. 따라서, 먼저 가상화를 지원 여부를 먼저 확인한다. 일부 가상화 기능을 지원하지 않는 경우가 있다. Window 8 또는 
그 이상 버전에서는 **작업관리자** (CTRL+SHIFT+ESC)의 **성능** 탭에서 확인할 수 있다.

![가상화 기능을 지원해야 한다.\label{fig:virtualization_windows_check}](images/figures/virtualization_windows_check.png)

윈도우 7 버전에서는 Microsoft® Hardware-Assisted Virtualization Detection Tool[^virtualization_detection_tool]을 설치하여 가상화를 지원하는지 확인할 수 있다.

만일 가상화를 사용할 수 없다면, 가상화 기능을 활성화하기 위한 일련의 과정이 필요하다. 그러나 해당 과정의 경우 컴퓨터 모델에 따라 조금씩 상이하며, 이를 위해 "\<사용자 컴퓨터 모델\>에서 vtx 활성화하는 방법" 등으로 검색 엔진에서 
이와 관련된 내용을 검색해보는 것을 추천한다. 대부분의 경우 컴퓨터 재부팅시 F2 또는 F12를 누르고 부팅메뉴에 들어가 가상화를 실행할 수 있도록 설정함으로서 활성화할 수 있다. (often called *VT-X*)

컴퓨터 제조사별 부팅 메뉴에 접근하기 위한 단축키는 일반적으로 다음과 같다.

| **Vendor**   | **Key**      |
|--------------|--------------|
| Acer         | Esc, F9, F12 |
| ASUS         | Esc, F8      |
| Compaq       | Esc, F9      |
| Dell         | F12          |
| EMachines    | F12          |
| HP           | F9           |
| Intel        | F10          |
| Lenovo       | F8, F10, F12 |
| NEC          | F5           |
| Packard Bell | F8           |
| Samsung      | Esc, F12     |
| Sony         | F11, F12     |
| Toshiba      | F12          |

가상화를 활성화한 후, step2를 진행한다.

### Step 2. ODM 설치를 위한 하위 프로그램(Requirements) 설치하기

첫 째, 다음과 같은 프로그램을 설치한다.

  - Git: <https://git-scm.com/downloads>

  - Python (최소 3.x 버전 이상):
    [https://www.python.org/downloads/windows/](https://www.python.org/downloads/windows)

파이썬 3을 설치하는 과정에서 반드시 **Add Python 3.x to PATH** 항목을 체크해야 한다.

![반드시 환경변수에 파이썬 경로를 추가해야 한다.\label{fig:python_path}](images/figures/python_path.png)

**Window 10 Home, Window 8 (어떤 버전이든)이거나 Winodw 7(어떤 버전이든)** 인 경우, 다음의 링크에서 설치한다.

  - Docker Toolbox:
    <https://github.com/docker/toolbox/releases/download/v18.09.3/DockerToolbox-18.09.3.exe>

**Winodw 10 Professional 또는 그 이상 버전** 인 경우, 아래의 링크에서 설치해야 한다.

  - Docker Desktop:
    <https://download.docker.com/win/stable/Docker%20for%20Windows%20Installer.exe>

두 버전의 Docker를 **모두 설치하지 않도록** 주의한다. 두 버전은 서로 다르기 때문에 둘 다 설치할 경우 문제가 발생할 수 있다.

After installing docker, launch it from the Desktop icon that is created
from the installation (**Docker Quickstart** in the case of Docker
Toolbox, **Docker Desktop** otherwise). This is important, do not skip
this step. If there are errors, follow the prompts on screen to fix
them.
Docker를 설치한 후, 생성된 데스크탑 아이콘을 통해 Docker를 실행할 수 있다(Docker 툴박스의 경우 **Docker Quickstart**, 다른 경우 **Docker Desktop**). 이 단계를 상략하지 않는 겻이 중요하다. 만일 오류가 발생한다면, 프롬프트에 표시된 
내용에 따라 오류를 해결해야 한다.

### Step 3. 메모리와 CPU 할당량 체크

윈도우 기반 Docker는 VMs을 통해 실행된다(VMs를 일종의 컴퓨터 에뮬레이터라고 생각하자). 버츄어머신은 일정양의 메모리를 할당받으며, WebODM은 할당된 메모리의 크기만큼만 사용할 수 있다.

만일 Docker 툴박스를 설치했다면 다음과 같은 과정을 통해 실행한다.(만일 Docker 데스크탑을 설치했다면 하단을 참고한다.)

1. **VirtualBox Manager**을 연다.

2. **Default**을 우클린 한 후, **Close (ACPI Shutdown)** 을 눌러 VMs을 중지한다.

3. **Default**를 우클릭한 후 **Settings...** 을 클릭한다.

4. **System**의 **Base Memory**로 이동한 후 사용 가능한 메모리의 60~70%를 할당한다. 옵션으로 **Processor** 탭에서 사용가능한 프로세서의 50%를 추가할 수 있다.

![버츄어박스 셋팅\label{fig:virtualbox_settings}](images/figures/virtualbox_settings.png)

이후 **OK** 버튼을 누르고, **default** 버튼을 우클릭한 후 **Start** 버튼을 클릭한다.

만일 Docker Desktop을 설치한 경우는 다음과 같다.

1. System tray에서 *whilte whale* 아이콘을 우클릭한다.

2. 메뉴에서, **Settings...** 클릭한다.

3. 페널에서 **Advanced**를 클릭한 후, 사용가능한 메모리의 60~70%를 할당하고, CPU는 절반을 할당한다.

4. **Apply**를 누른다.

![Docker Tray](images/figures/docker_tray.jpg)

![Docker Windows 설정](images/figures/docker_windows_settings.jpg)

### Step 4. WebODM 다운로드

Git과 함께 설치된 **Git Gui**를 실행한다. 이후 다음과 같은 과정을 수행한다.

  - **Source Location** 란에는 다음과 같이 입력한다.
    https://github.com/OpenDroneMap/WebODM
  
  - **Target Directory**란에는 browse를 클릭하여 클론을 저장할 위치에 있는 폴더를 클릭한다.(필요시 폴더를 생성한다.)
  
  - **Clone** 을 클릭한다.
  
![Git Gui\label{fig:git_gui}](images/figures/git_gui.png)

다운로드에 성공한 경우, 다음과 같은 창이 뜬다.

![Git Gui after successful download (clone)\label{fig:git_gui_after_clone}](images/figures/git_gui_after_clone.png)

**Repository** 로 이동한 후, **Create Desktop Icon** 을 클릭한다. 새롭게 생성된 데스크탑 아이콘을 클릭하면 해당 창을 다시 확인할 수 있다.

### Step 4. WebODM 실행하기

Git GUI에서 **Repository** 메뉴로 이동한 후, **Git Bash**를 클릭한다. 다음과 같은 명령어를 통해 WebODM을 실행한다.

    $ ./webodm.sh start

명령어를 입력하면 WebODM을 비롯하여 NodeODM, ODM이 추가적으로 설치될 것이다. 다운로드 후 화면에 다음과 같은 메세지가 표시될 것이다.

![WebODM을 처음 실행한 후 출력되는 화면\label{fig:console_output_after_webodm_start}](images/figures/console_output_after_webodm_start.jpg)

  - If you are using **Docker Desktop**, open a web browser to
    <http://localhost:8000>

  - If you are using **Docker Toolbox**, find the IP address to connect
    to by typing:

<!-- end list -->

    $ docker-machine ip
    192.168.1.100

이후 <http://192.168.1.100:8000>로 연결한다(필요하다면 IP 주소를 적절한 주소로 변경한다).

## MacOS에서 설치하기

Sierra 10.12 또는 그 이상의 최신 Mac(2010년 이후)에서는 Docker를 이용하여 오픈드론맵을 실행할 수 있다. 다만, 마찬가지로 하드웨어 가상화 기능이 지원되어야 한다.

### Step 1. 가상화 기능 지원여부 확인

**Terminal** 앱을 연다.

    $ sysctl kern.hv_support
    kern.hv_support: 1

만일 **1**을 결과로 반환받았다면, 해당 Mac의 경우 가상화를 지원하는 것이다. 이 경우 다음 단계로 넘어간다.

만일 **0**을 반환한다면, 해당 Mac은 오픈드론맵을 지원하지 않는 버전이다. :(

### Step 2. ODM 설치를 위한 하위 프로그램 설치하기

추가로 설치해야할 프로그램은 두 가지로 다음과 같다.

1.  Docker: <https://download.docker.com/mac/stable/Docker.dmg>

2.  Git:
    [https://sourceforge.net/projects/git-osx-installer/files/](https://sourceforge.net/projects/git-osx-installer/files)

도커를 설치한 후에 하얀색 고래처럼 보이는 아이콘을 작업표시줄에서 찾는다.

![MacOS에서 Docker Tray](images/figures/docker_macos.png)

**Terminal**에 음과 같은 명령어를 입력하여 적절하게 Docker가 실행되는지 확인한다.

    $ docker run hello-world
    [...]
    Hello from Docker!

Git이 잘 설치되었는지 확인하기 위해서는 다음과 같은 명령어를 입력하면 된다.

    $ git --version
    git version 2.20.1 (Apple Git-117)

*bash: git: command not found* 를 반환받았다면, **Terminal** 앱을 재시작한 후, 설치과정에서 오류가 있지는 않았는지 재확인한다.

### Step 3. 메모리와 CPU 할당량 검사하기

MacOS에서 Docker는 VMs를 통해 실행된다. VMs은 특정량의 할당된 메모리를 필요로하며, WebODM은 할당된 크기의 메모리만 사용할 수 있다.


1. 작업표시줄에서 하얀색 고래를 우클릭한 후, **Preference...** 를 클릭한다.

2. **Advanced** 탭을 클릭한다.

3. CPU 슬라이더를 이용하여 사용가능한 CPU의 절반을 할당하고, 동일방법으로 사용가능한 메모리의 60~70%를 할당한다.
   
4. **Apply & Restart** 를 클릭한다.

![도커 고급 설정\label{fig:docker_advanced_settings_macos}](images/figures/docker_advanced_settings_macos.png)

### Step 4. WebODM 다운로드 받고 설치하기

**Terminal**에서 다음과 같은 명령어를 입력한다.

    $ git clone https://github.com/OpenDroneMap/WebODM
    $ cd WebODM
    $ ./webodm.sh start

이후 웹 브라우저에서 http://localhost:8000.를 연다.

## Linux에서 설치하기

OpenDroneMap can run on any Linux distribution that supports docker.
According to docker’s documentation website[^docker_documentation_website] the officially
supported distributions are CentOS, Debian, Ubuntu and Fedora, with
static binaries available for others (I use Arch Linux quite
successfully). If you have to pick a distribution solely for running
OpenDroneMap, Ubuntu is the recommended way to go.
오픈드론맵은 Docker가 지원되는 모든 Linux 배포판에서 실행할 수 있다. Docker의 Documentation에 따르면, 공식 지원 배포판은 CentOS, Debian, Ubuntu, Fedora이며, (해석) 오픈드론맵용으로만 사용할 계획이라면, Ubuntu를 추천한다.

### Step 1. ODM 설치를 위한 하위 프로그램 설치하기

4개의 프로그램을 필요로하며, 이는 다음과 같다.

1.  Docker

2.  Git

3.  Python (2 or 3)

4.  Pip

모든 Linux 버전의 설치방법을 다루는 것은 지면상 한계가 있으므로, 공식적으로 Docker가 지원하는 배포판에 한정하여 설명을 진행하고자 한다. 터미널 프롬프트를 열거나 명령어를 입력하는 정도에서만 차이가 있다.

#### Ubuntu / Debian에서 설치하기

명령어를 다음과 같이 입력한다.

    $ sudo apt update
    $ curl -fsSL https://get.docker.com -o get-docker.sh
    $ sh get-docker.sh
    $ sudo apt install -y git python python-pip

#### CentOS / RHEL에서 설치하기

명령어를 다음과 같이 입력한다.

    $ curl -fsSL https://get.docker.com -o get-docker.sh
    $ sh get-docker.sh
    $ sudo yum -y install git python python-pip

#### Fedora에서 설치하기

명령어를 다음과 같이 입력한다.

    $ curl -fsSL https://get.docker.com -o get-docker.sh
    $ sh get-docker.sh
    $ sudo dnf install git python python-pip

#### Arch에서 설치하기

명령어를 다음과 같이 입력한다.

    $ sudo pacman -Sy docker git python python-pip

### Step 2. 추가 하위 프로그램 검사하기

위에서 언급한 프로그램들을 설치하는 것 외에도, **docker-compose**가 추가로 필요하다. Docker와 함께 설치되는 경우도 있으나, 그렇지 않은 경우도 있으므로 아래와 같은 명령어를 입력하여 체크한다.

    $ docker-compose --version
    docker-compose version 1.22.0, build f46880f

다음과 같은 응답이 나왔다면,

    docker-compose: command not found

**pip**을 이용하여 다운로드 받으면 된다.

    $ sudo pip install docker-compose

### Step 3. WebODM 다운로드 받고 설치하기

터미널에서 다음과 같이 입력한다.

    $ git clone https://github.com/OpenDroneMap/WebODM
    $ cd WebODM
    $ ./webodm.sh start

이후 http://localhost:8000.를 웹 브라우져에서 연다.

## 기본 명령어와 문제해결법

The cool thing about using docker is that 99% of the tasks you’ll ever
need to perform while using WebODM can be done via the ./webodm.sh
script. You have already encountered one of them:
Docker의 장점은 독자분들께서 WebODM을 이용하여 수행하게 될 99%의 업무가 ./webodm.sh 스크립트만으로 수행될 수 있다는 점이다. 그중 하나는 다음과 같으며, 이를 통해 WebODM의 시작과 기본처리노드를 설정할 수 있다.

    $ ./webodm.sh start

WebODM을 종료하고 싶다면, **CTRL+C**를 누르거나 다음과 같은 명령어를 사용하면 된다.

    $ ./webodm.sh stop

다른 변수를 이용한 다른 몇 가지 명령어들이 있다. 변수들은 ./webodm.sh 명령어와 함께 입력되며 일반적으로 접두사가 두 가지다. 예를들어, **--port**는 WebODM이 다른 네트워크 포트를 사용하도록 지시하는 변수다.

포트 8000이 아닌 80에서 WebODM 실행한다.
    $ ./webodm.sh restart --port 80 

유용한 다른 명령어는 아래의 리스트와 같다.

WebODM을 다시 시작한다.
    $ ./webodm.sh restart
    
관리자의 비밀번호를 리셋한다.
    $ ./webodm.sh resetadminpassword newpass
    
가장 최근버전으로 업데이트 한다.
    $ ./webodm.sh update
    
도커 내 기본 경로가 아닌 특정 경로로 처리결과를 저장한다.
    $ ./webodm.sh restart --media-dir /path/to/webodm_results
    
모든 옵션을 본다.
    $ ./webodm.sh --help


백업이나 문제해결과 같은 일반적인 유지관리 작업의 경우, WebODM의 README 페이지에서 최신 지침서를 제공하므로 읽어보길 권한다[^webodm_readme]. Community forum의 경우 설치과정에서 문제가 발생하거나, ./webodm.sh 스크립트
사용과 관련하여 궁금증이 생기게 되면 활용하는 것을 추천한다[^community_forum].

## Hello, WebODM!

**./webodm.sh start**를 통해 WebODM을 브라우저에서 실행하게 되면, 환영 메세지와 함께 계정 생성 요청을 받는다. 다양한 메뉴와 인터페이스에 친숙해질 수 있도록 한다.

![WebODM 대쉬보드bel{fig:webodm_dashboard}](images/figures/webodm_dashboard.png)

**Processing Nodes** 메뉴 하단에 독자분들이 사용할 수 있도록 *node-odm-1*이 사전에 생성되어 있다.이는 NodeODM 노드이며 WebODM에 의하여 자동으로 생성된 것이다. 이 노드는 동일 기기에서 WebODM과 동일 하게 작동한다.
*The Command Line*에서 동일 기기 뿐 아니라 다른 기기에서 새로운 노드를 생성하는 방법 을 확인할 수 있다.

이제 데이터를 처리해본다.

[^docker_on_linux]: IBM Research Report: An Updated Performance Comparison of Virtual Machines and Linux Containers: <https://domino.research.ibm.com/library/cyberdig.nsf/papers/0929052195DD819C85257D2300681E7B/$File/rc25482.pdf>
[^virtualization_detection_tool]: Microsoft® Hardware-Assisted Virtualization Detection Tool: <http://www.microsoft.com/en-us/download/details.aspx?id=592>
[^docker_documentation_website]: Docker Documentation: <https://docs.docker.com/install/>
[^webodm_readme]: WebODM README: <https://github.com/OpenDroneMap/WebODM>
[^community_forum]: OpenDroneMap Community Forum: <https://community.opendronemap.org>